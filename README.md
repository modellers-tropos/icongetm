# Coupled model ICONGETM

ICONGETM has been developed for coupled regional atmosphere-ocean simulations. The model is based on the non-hydrostatic NWP core of ICON and the regional model GETM.

## Developer: Tobias Peter Bauer and Knut Klingbeil

### Leibniz Institute for Tropospheric Research (TROPOS) / Leibniz Institute for Baltic Sea Research (IOW)

#### tobias.bauer@tropos.de / knut.klingbeil@io-warnemuende.de

##### How to get external libraries:

###### ICON:

Due to the license restrictions of ICON, the user has to download the ICON code. There are two possibilities:
1) After presenting a valid license from the DWD, which can be obtained from https://code.mpimet.mpg.de/projects/icon-license, the used code for the simulation from Bauer et al. (2020) (https://doi.org/10.5194/gmd-2020-269) can be downloaded from Zenodo:
- https://doi.org/10.5281/zenodo.4432739 and must then be copied to external/icon/code/
2) Alternatively, if a connection to the TROPOS network is established, the code can be downloaded by
- make -C external pull-icon

###### GETM:

All other external libraries (GOTM & GETM) can easily be downloaded by: 
- make -C external pull-all

##### How to install:

The model code can be installed using the provided make commands in the following order:

1) make -C external
2) make -C src

The Rules.make guarentees that all required libraries are appropriately linked. The following libraries are required:
|Libraries |Model component |
|----------|----------------|
|COMPILER  |ICON + GETM     |
|NETCDF    |ICON + GETM     |
|MPI       |ICON + GETM     |
|ESMF      |ICON + GETM     |
|SZIP      |ICON            |
|HDF5      |ICON            |
|libXML2   |ICON            |
|libXSLT   |ICON            |

Additionally, some variables have to be exported:

|Variables        |Usage                                       |
|-----------------|--------------------------------------------|
|LOCUSDIR         |path to ICONGETM code                       |
|ESMFMKFILE       |path to esmf.mk                             |
|FORTRAN_COMPILER |compiler specification for GETM             |
|ESMF_COMPILER    |compiler specification for ESMF and ICONGETM|

Special compiler flags can be set optional with:

- LOCUS_FFLAGS
- EXTRA_FFLAGS
- FABM_FFLAGS
- GOTM_FFLAGS
- GETM_FFLAGS

##### How to setup a simulation:

Both models ICON + GETM can be configured with the same settings as for uncoupled simulations. Only GETM requires changes to

met_method=3          (switches coupling mode on)

with optional settings for flux data exchange

calc_met=False 	      (assumes meteorological fluxes are provided)
fwf_method=2          (expects evaporation rate is provided)

The simulation must be provided with a configuration file ICONGETM.rc containing the following variables with default values in paranthesis 

nProc_ICON: 864 			# number of CPUs for ICON (1)\
nProc_GETM: 384				# number of CPUs for GETM (1)\
vars_GETM_Import: air_pressure_at_sea_level precipitation surface_downward_eastward_stress surface_downward_northward_stress surface_downwelling_shortwave_flux surface_downward_heat_flux evaporation		# list of variables which are imported to GETM (no default - optional)\
vars_ICON_Import: t_seasfc		# list of variables which are imported to ICON (no default - optional)\
startTime: 2012 07 01 00 00 00 		# year month day hour minute seconds (no default)\
endTime: 2012 07 22 00 00 00		# year month day hour minute seconds (no default)\
timestep: 3 0 				# [[[[[year] month] day] hour] min] seconds (no default)\
verbosity: low				# possible values: off, low, high, max (off - optional)\

##### Code changes in ICON + GETM:

The code changes in ICON are always marked with

'changed by Bauer'

and the corresponding pre-processor flag is -DESMF_coupling. If this flag is not provided during the compilation, then the original ICON code without the ESMF interface is compiled.

The required code modification in GETM are included in the official source code used by the IOW.
