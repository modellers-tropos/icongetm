ifndef ESMFMKFILE
$(error ESMFMKFILE not defined!)
endif
include $(ESMFMKFILE)

export FC=$(ESMF_F90COMPILER)
export FFLAGS=-I$(LOCUSDIR)/include $(ESMF_F90COMPILEPATHS) $(ESMF_F90COMPILEOPTS) $(LOCUS_FFLAGS)
export LOCUS_LDFLAGS=-L$(LOCUSDIR)/lib $(ESMF_F90LINKPATHS) $(ESMF_F90LINKRPATHS)

ESMF_COMPILER = $(strip $(shell grep "\# ESMF_COMPILER:" $(ESMFMKFILE) | cut -d':' -f2-))
ESMF_COMM = $(strip $(shell grep "\# ESMF_COMM:" $(ESMFMKFILE) | cut -d':' -f2-))
ESMF_OPENMP = $(strip $(shell grep "\# ESMF_OPENMP:" $(ESMFMKFILE) | cut -d':' -f2-))

ifeq ($(ESMF_COMPILER),intel)
  export FFLAGS+=-module $(LOCUSDIR)/include
  export CC=icc
else
  export FFLAGS+=-J$(LOCUSDIR)/include
endif

### ICON
export external_ICONDIR=$(LOCUSDIR)/external/icon/code
ifndef LOCUS_ICONDIR
ifneq ($(wildcard $(external_ICONDIR)/configure),)
    export LOCUS_ICONDIR=$(external_ICONDIR)
endif
endif
ifdef LOCUS_ICONDIR
  export ICONDIR=$(LOCUS_ICONDIR)
endif
ifndef ICONDIR
  $(warning ICON not available)
endif

#ifdef GRIBAPILIBPATH
#GRIBAPIROOT?=`echo $(GRIBAPILIBPATH) | rev | cut -d '/' -f 2- | rev`
#endif
#GRIBAPIROOT?=`which grib_info | rev | cut -d '/' -f 3- | rev`
#GRIBAPILIBPATH?=$(GRIBAPIROOT)/lib

##ICONs configure might have pick anything...
#GRIBAPIROOT = $(strip $(shell grep "GRIBAPIROOT" $(ICONDIR)/Makefile | head -n1 | cut -d'=' -f2 2> /dev/null))
##GRIBAPI_LIB = $(strip $(shell grep "GRIBAPI_LIB" $(ICONDIR)/Makefile | head -n1 | cut -d'=' -f2- 2> /dev/null))
#GRIBAPI_LIB := $(eval $(shell grep "GRIBAPI_LIB" $(ICONDIR)/Makefile | head -n1))$(GRIBAPI_LIB)

ifneq ($(wildcard $(ICONDIR)/config/set-up.info),)
export ICON_BUILDDIR=$(ICONDIR)/$(shell fgrep "use_builddir=" $(ICONDIR)/config/set-up.info | cut -d "'" -f 2)
export FFLAGS += -I$(ICON_BUILDDIR)/module/
#export ICON_LDFLAGS = $(ICON_BUILDDIR)/src/version.o -L$(ICON_BUILDDIR)/lib/ -L$(ICON_BUILDDIR)/src/ -L$(GRIBAPILIBPATH) $(shell xml2-config --libs) -licon -lmtime -lself -lsupport -lyac -lgrib_api -llapack -lblas
export ICON_LDFLAGS = $(ICON_BUILDDIR)/src/version.o -L$(ICON_BUILDDIR)/lib/ -L$(ICON_BUILDDIR)/src/ $(shell xml2-config --libs) -licon -lmtime -lself -lsupport -lyac -llapack -lblas $(GRIBAPI_LIB)
endif

### FABM
export FABM=false
export external_FABMDIR=$(LOCUSDIR)/external/fabm/code
FABM_PREFIX=
ifndef LOCUS_FABMDIR
  ifneq ($(wildcard $(external_FABMDIR)/src/fabm.F90),)
    export LOCUS_FABMDIR=$(external_FABMDIR)
  endif
endif
ifdef LOCUS_FABMDIR
  export FABMDIR=$(LOCUS_FABMDIR)
endif
ifdef FABMDIR
  export FABM_BINARY_DIR=$(LOCUSDIR)/external/fabm/build
  export FABM_PREFIX=$(LOCUSDIR)/external/fabm/install
endif
ifndef FABM_PREFIX
  $(warning FABM not available)
endif

### GOTM
export external_GOTMDIR=$(LOCUSDIR)/external/gotm/code
GOTM_PREFIX=
ifndef LOCUS_GOTMDIR
  ifneq ($(wildcard $(external_GOTMDIR)/src/gotm/gotm.F90),)
    export LOCUS_GOTMDIR=$(external_GOTMDIR)
  endif
endif
ifdef LOCUS_GOTMDIR
  export GOTMDIR=$(LOCUS_GOTMDIR)
endif
ifdef GOTMDIR
  export GOTM_BINARY_DIR=$(LOCUSDIR)/external/gotm/build
  export GOTM_PREFIX=$(LOCUSDIR)/external/gotm/install
endif
ifndef GOTM_PREFIX
  $(warning GOTM not available)
endif

### GETM
export external_GETMDIR=$(LOCUSDIR)/external/getm/code
ifndef LOCUS_GETMDIR
  ifneq ($(wildcard $(external_GETMDIR)/src/getm/main.F90),)
    export LOCUS_GETMDIR=$(external_GETMDIR)
  endif
endif
ifdef LOCUS_GETMDIR
  export GETMDIR=$(LOCUS_GETMDIR)
endif
ifdef GETMDIR
  #include $(GETMDIR)/src/coupling/Rules.make_ESMF
else
  $(warning GETM not available)
endif
export GETM_LIBRARY_PATH=$(GETMDIR)/lib/$(FORTRAN_COMPILER)
GETM_LINKDIRS = -L$(GETM_LIBRARY_PATH) -L$(GOTM_PREFIX)/lib
GETM_LIBS := -lgetm_esmf_prod -lgetm_prod  -loutput_prod -lmeteo_prod
ifneq ($(GETM_NO_3D),true)
  GETM_LIBS += -l3d_prod
endif
GETM_LIBS += -l2d_prod -lwaves_prod -lles_prod -lpool_prod -ldomain_prod -linput_prod -lncdfio_prod -lfutils_prod
ifeq ($(FABM),true)
  GETM_LINKDIRS += -L$(FABM_PREFIX)/lib
  GETM_LIBS += -lgotm_fabm -lfabm
endif
GETM_LIBS += -loutput_manager -lturbulence $(GOTM_PREFIX)/lib/libutil.a
ifneq ($(wildcard $(GOTM_PREFIX)/lib/libfield_manager.*), )
  GETM_LIBS += -lfield_manager -lyaml
endif
export STATIC+=-D_NO_GRIDALIGN_
export GETM_CPPFLAGS = $(STATIC)
ifeq ($(GETM_PARALLEL),true) # Compile for parallel execution
  export GETM_CPPFLAGS += -DGETM_PARALLEL
endif
export GETM_CPPFLAGS += -I$(GETMDIR)/include -I$(GETMDIR)/modules/$(FORTRAN_COMPILER)
export GETM_CPPFLAGS += -I$(GOTM_PREFIX)/include -I$(FABM_PREFIX)/include
export GETM_LDFLAGS = $(GETM_LINKDIRS) $(GETM_LIBS)

export LOCUS_LDFLAGS += -llocus $(ICON_LDFLAGS) $(GETM_LDFLAGS) $(ESMF_F90ESMFLINKLIBS)

%.o: %.F90
	$(FC) $(CPPFLAGS) $(FFLAGS) -c $< -o $@

%.o: %.f90
	$(FC) $(CPPFLAGS) $(FFLAGS) -c $< -o $@

