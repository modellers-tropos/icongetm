MODULE mo_esmf_icon_mesh

    USE ESMF

    USE mo_mpi,                 ONLY: ESMF_ICON_COMM
    
    USE mo_kind,                ONLY: wp
    USE mo_model_domain,        ONLY: p_patch
    USE mo_ext_data_state,      ONLY: ext_data
    USE mo_grid_config,         ONLY: n_dom
    USE mo_physical_constants,  ONLY: earth_radius

    USE mo_math_types,          ONLY: t_geographical_coordinates
    USE mo_math_constants,      ONLY: pi , rad2deg , deg2rad

    USE mo_math_utilities,      ONLY: ccw

    USE mo_parallel_config,     ONLY: nproma
    USE mo_exception,           ONLY: message

    USE mo_esmf_var,            ONLY: config_ESMF
    USE mo_esmf_utilities,      ONLY: checkESMF
    USE mo_esmf_math_utilities, ONLY: point_in_polygon

IMPLICIT NONE

    PUBLIC :: construct_ICON_Mesh , destruct_ICON_Mesh

CONTAINS


SUBROUTINE construct_ICON_Mesh(ICON_Mesh)

    TYPE(ESMF_Mesh), INTENT(OUT) :: ICON_Mesh

    ! dimension of cells (e.g. for triangle 2, cube 3)
    INTEGER, PARAMETER :: parametricDim = 2
    INTEGER, PARAMETER :: triangular_cell = 3
    ! dimension of coords of elements, spatialDim >= parametricDim
    ! 2: (x,y) or (phi,r), 3: (x,y,z), (lon,lat,r)
    INTEGER, PARAMETER :: spatialDim = 3 ! here: (lon,lat,r)

    INTEGER :: nverts , ncells
    INTEGER :: i , ii , j , k , ix , i_dom
    INTEGER :: localrc

    INTEGER, DIMENSION(:,:), ALLOCATABLE :: vertsID , vertsOwner , vertsMask
    ! Mask: 0: land no model connection, 1: sea no model connection
    !      10: land model connection    11: sea model connection
    INTEGER, DIMENSION(:,:), ALLOCATABLE :: cellsID , cellsType , cellsConn , cellsMask
    INTEGER, DIMENSION(triangular_cell) :: vert_idx , vert_blk

    REAL(wp), DIMENSION(:,:), ALLOCATABLE :: vertsCoord , cellsCoord
    REAL(wp), DIMENSION(:,:), ALLOCATABLE :: cellsArea

    TYPE(t_geographical_coordinates) :: p_Rectangle(4) , p_Triangle(3) , vert
  
    TYPE(t_geographical_coordinates) :: p(3)

    CHARACTER(len=*), PARAMETER :: routine = 'mo_esmf_ICON_Mesh:construct_ICON_ESMF_Mesh'


    ! Creation of ESMF_Mesh for atmospheric grid.
    ICON_Mesh = ESMF_MeshCreate(parametricDim = parametricDim , spatialDim = spatialDim , &
        & coordSys = ESMF_COORDSYS_SPH_DEG , rc = localrc)
    CALL checkESMF(localrc , TRIM(routine) , "ESMF_MeshCreate")

    DO i_dom = n_dom , n_dom

        IF (config_ESMF%lowned) THEN

            ncells = p_patch(i_dom)%cells%owned%size - p_patch(i_dom)%cells%owned%no_of_holes

        ELSE

            ncells = p_patch(i_dom)%cells%ALL%size

        END IF
    
        ALLOCATE(cellsID(ncells,1))
        ALLOCATE(cellsMask(ncells,1))
        ALLOCATE(cellsType(ncells,1))
        ALLOCATE(cellsArea(ncells,1))
        ALLOCATE(cellsCoord(spatialDim * ncells,1))
        ALLOCATE(cellsConn(triangular_cell * ncells,1))

        cellsType(:,1) = ESMF_MESHELEMTYPE_TRI ! in this case always triangle
        cellsMask(:,1) = 0

        ii = 0

        DO ix = 1 , p_patch(i_dom)%cells%ALL%size

            i = MOD(ix-1 , nproma) + 1
            j = (ix-1) / nproma + 1

            IF (config_ESMF%lowned .AND. p_patch(i_dom)%cells%decomp_info%decomp_domain(i,j) .NE. 0) THEN

                ii = ii + 1
                CYCLE

            END IF

            cellsID(ix-ii,1) = p_patch(i_dom)%cells%decomp_info%glb_index(ix)
            
            IF (.NOT. ext_data(i_dom)%atm%llsm_atm_c(i,j)) cellsMask(ix-ii,1) = 1
! 0: Land
! 1: Water
            cellsArea(ix-ii,1) = p_patch(i_dom)%cells%area(i,j)! / (earth_radius**2)

            vert_idx(:) = p_patch(i_dom)%cells%vertex_idx(i,j,:)
            vert_blk(:) = p_patch(i_dom)%cells%vertex_blk(i,j,:)

            p(1)%lon = p_patch(i_dom)%verts%vertex(vert_idx(1),vert_blk(1))%lon
            p(1)%lat = p_patch(i_dom)%verts%vertex(vert_idx(1),vert_blk(1))%lat
            p(2)%lon = p_patch(i_dom)%verts%vertex(vert_idx(2),vert_blk(2))%lon
            p(2)%lat = p_patch(i_dom)%verts%vertex(vert_idx(2),vert_blk(2))%lat
            p(3)%lon = p_patch(i_dom)%verts%vertex(vert_idx(3),vert_blk(3))%lon
            p(3)%lat = p_patch(i_dom)%verts%vertex(vert_idx(3),vert_blk(3))%lat
  
            k = triangular_cell*(ix-ii-1)+1
      
            ! cell connections need to be in counter clockwise order
            IF (ccw(p(1),p(2),p(3)) == 1) THEN

                cellsConn(k  ,1) = vert_idx(1)+(vert_blk(1)-1)*nproma
                cellsConn(k+1,1) = vert_idx(2)+(vert_blk(2)-1)*nproma
                cellsConn(k+2,1) = vert_idx(3)+(vert_blk(3)-1)*nproma

            ELSE
        
                cellsConn(k  ,1) = vert_idx(1)+(vert_blk(1)-1)*nproma
                cellsConn(k+1,1) = vert_idx(3)+(vert_blk(3)-1)*nproma
                cellsConn(k+2,1) = vert_idx(2)+(vert_blk(2)-1)*nproma

            END IF

            k = spatialDim*(ix-ii-1)+1

            cellsCoord(k  ,1) = rad2deg * p_patch(i_dom)%cells%center(i,j)%lon
            cellsCoord(k+1,1) = rad2deg * p_patch(i_dom)%cells%center(i,j)%lat
            cellsCoord(k+2,1) = 1.0_wp !earth_radius

        END DO

        nverts = 1

        DO i = 2 , triangular_cell * ncells

            IF (COUNT(cellsConn(i,1) == cellsConn(1:i-1,1)) == 0) THEN

                nverts = nverts + 1
            
            END IF

        END DO

        ALLOCATE(vertsID(nverts , 1))
        ALLOCATE(vertsCoord(spatialDim * nverts , 1))
        ALLOCATE(vertsOwner(nverts , 1))
!        ALLOCATE(vertsMask(nverts , 1))
!
!        vertsMask(:,1) = 0
!
!        p_Rectangle(1)%lon = 14.474970072463769 * deg2rad
!        p_Rectangle(2)%lon = 18.025359927536229 * deg2rad
!        p_Rectangle(3)%lon = 18.025359927536229 * deg2rad
!        p_Rectangle(4)%lon = 14.474970072463769 * deg2rad    
!    
!        p_Rectangle(1)%lat = 53.928376319444439 * deg2rad
!        p_Rectangle(2)%lat = 53.928376319444439 * deg2rad
!        p_Rectangle(3)%lat = 55.825638680555549 * deg2rad
!        p_Rectangle(4)%lat = 55.825638680555549 * deg2rad

        ii = 0
    
        DO ix = 1 , MAXVAL(cellsConn)
        
            IF (.NOT. ANY(cellsConn == ix)) THEN

                ii = ii + 1
                CYCLE

            END IF

            i = MOD(ix-1 , nproma) + 1
            j = (ix-1) / nproma + 1

            vertsID(ix-ii , 1)    = p_patch(i_dom)%verts%decomp_info%glb_index(ix)
            vertsOwner(ix-ii , 1) = p_patch(i_dom)%verts%decomp_info%owner_local(ix)

            k = spatialDim*(ix-ii-1)+1

            vertsCoord(k  , 1) = rad2deg * p_patch(i_dom)%verts%vertex(i,j)%lon
            vertsCoord(k+1, 1) = rad2deg * p_patch(i_dom)%verts%vertex(i,j)%lat
            vertsCoord(k+2, 1) = 1.0_wp !earth_radius

!            vert%lon = vertsCoord(k  ,1) * deg2rad
!            vert%lat = vertsCoord(k+1,1) * deg2rad
!        
!            vertsMask(ix-ii,1) = point_in_polygon(p_Rectangle , vert)

        END DO

        ! adding vertices to mesh
        CALL ESMF_MeshAddNodes(ICON_Mesh , nodeIds = vertsID(:,1) , nodeCoords = vertsCoord(:,1) , &
            & nodeOwners = vertsOwner(:,1) , & 
!                             & nodeMask = vertsMask(:,1) , &
            & rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_MeshAddNodes")

        IF (MAXVAL(cellsConn) > nverts) THEN

            ii = 0

            DO i = 1 , MAXVAL(cellsConn)

                IF (ANY(cellsConn == i - ii)) THEN

                    CYCLE

                ELSE

                    WHERE (cellsConn > i - ii)

                        cellsConn = cellsConn - 1

                    END WHERE

                    ii = ii + 1

                END IF

            END DO

        END IF

!        DO i = 1 , ncells
!
!            k = triangular_cell*(i-1)+1
!
!            p_Triangle(1)%lon = deg2rad * vertsCoord(spatialDim * (cellsConn(k  ,1)-1)+1,1)
!            p_Triangle(2)%lon = deg2rad * vertsCoord(spatialDim * (cellsConn(k+1,1)-1)+1,1)
!            p_Triangle(3)%lon = deg2rad * vertsCoord(spatialDim * (cellsConn(k+2,1)-1)+1,1)
!
!            p_Triangle(1)%lat = deg2rad * vertsCoord(spatialDim * (cellsConn(k  ,1)-1)+1+1,1)
!            p_Triangle(2)%lat = deg2rad * vertsCoord(spatialDim * (cellsConn(k+1,1)-1)+1+1,1)
!            p_Triangle(3)%lat = deg2rad * vertsCoord(spatialDim * (cellsConn(k+2,1)-1)+1+1,1)
!
!            IF (ANY(vertsMask(cellsConn(k:k+2,1),1) == 1)) THEN
!
!                cellsMask(i,1) = 10 + cellsMask(i,1)
!
!            ELSE
!
!                DO j = 1 , 4
!
!                   tf = point_in_ICON_triangle(p_Triangle , p_Rectangle(j))
!    
!                    IF (tf == 1) THEN
!
!                        cellsMask(i,1) = 10 + cellsMask(i,1)
!                        EXIT
!
!                    END IF
!
!                END DO
!
!            END IF
!
!        END DO
    
        DEALLOCATE(vertsID , vertsOwner , vertsCoord)! , vertsMask)

        ! adding cells to mesh
        ! vertex indices are from 1 to nverts and NOT dependent from global ID's
        CALL ESMF_MeshAddElements(ICON_Mesh , elementIds = cellsID(:,1) , &
                                & elementTypes = cellsType(:,1) , elementConn = cellsConn(:,1) , &
                                & elementMask = cellsMask(:,1) , &
                                & elementArea = cellsArea(:,1) , &
                                & elementCoords = cellsCoord(:,1) , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_MeshAddElements")
    
       DEALLOCATE(cellsConn , cellsType , cellsID , cellsCoord , cellsArea , cellsMask)

    END DO

END SUBROUTINE construct_ICON_Mesh


SUBROUTINE destruct_ICON_Mesh(ICON_Mesh)

    TYPE(ESMF_Mesh), INTENT(INOUT) :: ICON_Mesh

    INTEGER :: localrc

    CHARACTER(LEN=*), PARAMETER :: routine = 'mo_esmf_mesh:destruct_ICON_Mesh'


    CALL ESMF_MeshDestroy(ICON_Mesh , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_MeshDestroy")

    CALL message(TRIM(routine) , 'destruct ESMF_Mesh finished')

END SUBROUTINE destruct_ICON_Mesh


END module mo_esmf_icon_mesh
