MODULE mo_esmf_icon_comp

    USE ESMF
    USE NUOPC
    USE NUOPC_Model,                  ICON_Comp_SetServices             => SetServices , &
                                    & ICON_Comp_Label_SetClock          => label_SetClock , &
                                    & ICON_Comp_Label_Advance           => label_Advance , &
                                    & ICON_Comp_Label_Finalize          => label_Finalize
    
    USE mo_esmf_utilities,      ONLY: checkESMF
    USE mo_esmf_var,            ONLY: config_ESMF

    USE mo_esmf_icon_data,      ONLY: get_var_list_with_var
    USE mo_esmf_icon_mesh,      ONLY: construct_ICON_Mesh , destruct_ICON_Mesh

    ! from here ICON specific modules
    USE mo_driver_icon,         ONLY: icon_init , icon_run , icon_final

    USE mo_mpi,                 ONLY: ESMF_ICON_COMM
    USE mo_sync,                ONLY: SYNC_C , sync_patch_array

    USE mo_io_units,            ONLY: nerr
    USE mo_kind,                ONLY: wp

    USE mo_atm_phy_nwp_config,  ONLY: atm_phy_nwp_config

    USE mo_run_config,          ONLY: nsteps , dtime , iforcing
    USE mo_parallel_config,     ONLY: nproma

    USE mo_exception,           ONLY: message

    USE mo_nonhydro_state,      ONLY: p_nh_state , p_nh_state_lists
    USE mo_nwp_phy_state,       ONLY: prm_nwp_diag_list
    USE mo_nwp_lnd_state,       ONLY: p_lnd_state

    USE mo_var_list,            ONLY: get_var , get_var_list , get_var_list_element_info
    USE mo_linked_list,         ONLY: t_var_list
    USE mo_var_metadata_types,  ONLY: t_var_metadata

    USE mo_time_config,         ONLY: time_config

    USE mo_model_domain,        ONLY: p_patch
    USE mo_grid_config,         ONLY: n_dom

    USE mo_dynamics_config,     ONLY: nnow_rcf , nnew_rcf
    USE mo_lnd_nwp_config,      ONLY: isub_water , sstice_mode

    USE mo_util_mtime,          ONLY: assumePrevMidnight
    USE mo_nh_stepping,         ONLY: mtime_old

    USE mo_impl_constants,      ONLY: inwp , SSTICE_ANA

    USE mo_ext_data_state,      ONLY: ext_data

    USE mo_td_ext_data,         ONLY: set_sst_and_seaice

    USE mo_nwp_sfc_utils,       ONLY: update_sst_and_seaice

    IMPLICIT NONE

    PUBLIC :: SetServices

    PRIVATE :: Advertize , Realize

    INTEGER, ALLOCATABLE, DIMENSION(:)  :: noHalo

    INTEGER :: timeCount

CONTAINS


SUBROUTINE SetServices(ICON_Comp , localrc)

    TYPE(ESMF_GridComp)             :: ICON_Comp

    INTEGER, INTENT(OUT)            :: localrc

    CHARACTER(LEN=29), PARAMETER    :: routine = "mo_esmf_icon_comp:SetServices"


    CALL ESMF_LogWrite("ICON: set services" , ESMF_LOGMSG_INFO , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_LogWrite ICON SetServices")

    CALL NUOPC_CompDerive(ICON_Comp , ICON_Comp_SetServices , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompDerive")    


!    CALL ESMF_GridCompSetEntryPoint(ICON_Comp , ESMF_METHOD_INITIALIZE , userRoutine = ICON_Init_Phase_0 , phase = 0 , rc = localrc)
!    CALL checkESMF(localrc , routine , "ESMF_GridCompSetEntryPoint - ESMF_METHOD_INITIALIZE - Phase 0")

    CALL NUOPC_CompSetEntryPoint(ICON_Comp , ESMF_METHOD_INITIALIZE , phaseLabelList = (/ "IPDv00p1" /) , &
        & userRoutine = ICON_Init_Phase_1 , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompSetEntryPoint - ESMF_METHOD_INITIALIZE - Phase 1")

    CALL NUOPC_CompSetEntryPoint(ICON_Comp , ESMF_METHOD_INITIALIZE , phaseLabelList = (/ "IPDv00p2" /) , &
        & userRoutine = ICON_Init_Phase_2 , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompSetEntryPoint - ESMF_METHOD_INITIALIZE - Phase 2")

    
    ! phase-independent specializing methods

    CALL NUOPC_CompSpecialize(ICON_Comp , specLabel = ICON_Comp_Label_SetClock , specRoutine = ICON_Comp_SetClock , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompSpecialize ICON clock")

    CALL NUOPC_CompSpecialize(ICON_Comp , specLabel = ICON_Comp_Label_Advance , specRoutine = ICON_Comp_Advance , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompSpecialize ICON advance")

    CALL NUOPC_CompSpecialize(ICON_Comp , specLabel = ICON_Comp_Label_Finalize , specRoutine = ICON_Comp_Finalize , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompSpecialize ICON finalize")

END SUBROUTINE SetServices


!SUBROUTINE ICON_Init_Phase_0(ICON_Comp , ImportState , ExportState , ICONGETM_Clock , localrc)
!
!    TYPE(ESMF_GridComp)             :: ICON_Comp
!    TYPE(ESMF_State)                :: ImportState , ExportState
!    TYPE(ESMF_Clock)                :: ICONGETM_Clock
!
!    INTEGER, INTENT(OUT)            :: localrc
!
!    CHARACTER(LEN=35), PARAMETER    :: routine = "mo_esmf_icon_comp:ICON_Init_Phase_0"
!
!
!    ! Switch to IPDv05 by filtering all other phaseMap entries
!    CALL NUOPC_CompFilterPhaseMap(ICON_Comp , ESMF_METHOD_INITIALIZE , acceptStringList = (/ "IPDv05p" /) , rc = localrc)
!    CALL checkESMF(localrc , routine , "NUOPC_CompFilterPhaseMap IPDv05p")
!
!END SUBROUTINE ICON_Init_Phase_0


SUBROUTINE ICON_Init_Phase_1(ICON_Comp , ImportState , ExportState , ICON_Clock , localrc)

    TYPE(ESMF_GridComp)             :: ICON_Comp
    TYPE(ESMF_State)                :: ImportState , ExportState
    TYPE(ESMF_Clock)                :: ICON_Clock

    INTEGER, INTENT(OUT)            :: localrc
    
    TYPE(ESMF_VM)                   :: ICON_VM
    TYPE(ESMF_Mesh)                 :: ICON_Mesh

    INTEGER                         :: i , p_error , COMM_ESMF_ICON

    TYPE(NUOPC_FreeFormat)          :: freeFormat    

    CHARACTER(LEN=35), PARAMETER    :: routine = "mo_esmf_icon_comp:ICON_Init_Phase_1"


    CALL ESMF_LogWrite("ICON: initialize: phase 1" , ESMF_LOGMSG_INFO , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_LogWrite ICON init phase 1")

    CALL ESMF_GridCompGet(ICON_Comp , vm = ICON_VM , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_GridCompGet ICON VM")

    CALL ESMF_VMGet(ICON_VM , mpiCommunicator = COMM_ESMF_ICON , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_VMGet ICON communicator")

    CALL MPI_COMM_DUP(COMM_ESMF_ICON , ESMF_ICON_COMM , p_error)
    CALL checkESMF(p_error , routine , "MPI_COMM_DUP ICON")


    ! init ICON
    CALL icon_init()


    ! get ESMF_Mesh for ICON  
    CALL construct_ICON_Mesh(ICON_Mesh)

    CALL ESMF_GridCompSet(ICON_Comp , mesh = ICON_Mesh , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_GridCompSet ICON mesh")


    ! pressure

        ! advertise export: pres_msl = mean sea level pressure = air_pressure_at_sea_level
        CALL Advertize(ExportState , "pres_msl")

    ! wind

        ! advertise export: u_10m = zonal wind in 10m = wind_x_velocity_at_10m
        CALL Advertize(ExportState , "u_10m")

        ! advertise export: v_10m = meridional wind in 10m = wind_y_velocity_at_10m
        CALL Advertize(ExportState , "v_10m")
    
    ! temperature

        ! advertise import: t_seasfc = sea surface temperature = sea_surface_temperature
        CALL Advertize(ImportState , "t_seasfc")
! required for corrected data initialization -> should be removed as soon as  possible
        CALL Advertize(ExportState , "t_seasfc")

        ! advertise export: t_s = weighted temperature of ground surface 
        CALL Advertize(ExportState , "t_s")

        ! advertise export: t_2m = temperature in 2m = air_temperature_at_2m
        CALL Advertize(ExportState , "t_2m")
    
        ! advertise export: td_2m = dew-point in 2m = dew_point_temperature
        CALL Advertize(ExportState , "td_2m")

    ! momentum

        ! advertise export: umfl_s = u-momentum flux at the surface = surface_downward_eastward_stress
        CALL Advertize(ExportState , "umfl_s")

        ! advertise export: vmfl_s = v-momentum flux at the surface = surface_downward_northward_stress
        CALL Advertize(ExportState , "vmfl_s")

    ! precipitation

        ! advertise export: rain_gsp_rate = gridscale rain rate
        CALL Advertize(ExportState , "rain_gsp_rate")

        ! advertise export: snow_gsp_rate = gridscale snow rate
        CALL Advertize(ExportState , "snow_gsp_rate")

        ! For graupel scheme
        SELECT CASE (atm_phy_nwp_config(n_dom)%inwp_gscp)

            CASE (2,4,5,6)

                ! advertise export: graupel_gsp_rate = gridscale graupel rate
                CALL Advertize(ExportState , "graupel_gsp_rate")

        END SELECT

        !For two moment microphysics
        SELECT CASE (atm_phy_nwp_config(n_dom)%inwp_gscp)

            CASE (4,5,6)

                ! advertise export: ice_gsp_rate = gridscale ice rate
                CALL Advertize(ExportState , "ice_gsp_rate")

                ! advertise export: hail_gsp_rate = gridscale hail rate
                CALL Advertize(ExportState , "hail_gsp_rate")
    
        END SELECT

        ! advertise export: rain_con_rate = convective rain rate
        CALL Advertize(ExportState , "rain_con_rate")

        ! advertise export: snow_con_rate = convective snow rate
        CALL Advertize(ExportState , "snow_con_rate")

    ! radiation

        ! advertise export: sob_s = shortwave net flux at surface (net without diffusive part) = surface_downwelling_shortwave_flux
        CALL Advertize(ExportState , "sob_s")

        ! advertise export: thb_s = longwave net flux at surface
        CALL Advertize(ExportState , "thb_s")

        ! advertise export: sodifd_s = shortwave diffuse downward flux at surface
        CALL Advertize(ExportState , "sodifd_s")

    ! heat

        ! advertise export: shfl_s = surface sensible heat flux (downward)
        CALL Advertize(ExportState , "shfl_s")

        ! advertise export: lhfl_s = surface latent heat flux (downward)
        CALL Advertize(ExportState , "lhfl_s")

    ! humidity

        ! advertise export: qv_s = specific humidity at the surface 
        CALL Advertize(ExportState , "qv_s")

        ! advertise export: rh_2m = relative humidity in 2m
        CALL Advertize(ExportState , "rh_2m")

    ! moisture / cloud cover
    
        ! advertise export: qhfl_s = surface moisture flux = surface_downward_water_flux
        CALL Advertize(ExportState , "qhfl_s")

        ! advertise export: clct = total cloud cover = total_cloud_cover
        CALL Advertize(ExportState , "clct")


    CALL message(routine , "returning")

END SUBROUTINE ICON_Init_Phase_1


SUBROUTINE Advertize(State , ShortName)

    TYPE(ESMF_State), INTENT(INOUT)         :: State

    CHARACTER(LEN=*), INTENT(IN)            :: ShortName

    TYPE(t_var_list)                        :: list
    TYPE(t_var_metadata)                    :: var_info 

    INTEGER                                 :: localrc

    CHARACTER(LEN=128)                      :: StandardNames(2)

    CHARACTER(LEN=27), PARAMETER            :: routine = "mo_esmf_icon_comp:Advertize"


    ! add ICON variable to NUOPC_FieldDictionary

    list = p_nh_state_lists(n_dom)%diag_list
    CALL get_var_list_element_info(list , TRIM(ShortName) , var_info)

    IF (TRIM(ShortName) /= TRIM(var_info%name)) THEN

        list = prm_nwp_diag_list(n_dom)
        CALL get_var_list_element_info(list , TRIM(ShortName) , var_info)

        IF (TRIM(ShortName) /= TRIM(var_info%name)) THEN

            list = p_lnd_state(n_dom)%lnd_diag_nwp_list
            CALL get_var_list_element_info(list , TRIM(ShortName) , var_info)

            IF (TRIM(ShortName) /= TRIM(var_info%name)) THEN

                CALL checkESMF(-1 , routine , "Variable "//TRIM(ShortName)//" not found in ICON!!!")

            END IF

        END IF

    END IF


    CALL NUOPC_Advertise(State , StandardName = TRIM(var_info%name) , &
        & TransferOfferGeomObject = "will provide" , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_Advertise "//TRIM(var_info%name))

END SUBROUTINE Advertize


SUBROUTINE ICON_Init_Phase_2(ICON_Comp , ImportState , ExportState , ICON_Clock , localrc)

    TYPE(ESMF_GridComp)                 :: ICON_Comp
    TYPE(ESMF_State)                    :: ImportState , ExportState
    TYPE(ESMF_Clock)                    :: ICON_Clock

    INTEGER, INTENT(OUT)                :: localrc

    TYPE(ESMF_Mesh)                     :: ICON_Mesh

    INTEGER                             :: i , j , k , l

    CHARACTER(LEN=35), PARAMETER        :: routine = "mo_esmf_icon_comp:ICON_Init_Phase_2"


    CALL ESMF_LogWrite("ICON: initialize: phase 2" , ESMF_LOGMSG_INFO , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_LogWrite ICON init phase 2")

    CALL ESMF_GridCompGet(ICON_Comp , mesh = ICON_Mesh , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_GridCompGet ICON mesh")

    ! Initialize data

    ! cell masking for ICON, i.e. weather cell is HALO or not

    IF (.NOT. config_ESMF%lowned) THEN

        ALLOCATE(noHalo(p_patch(n_dom)%cells%ALL%size))

        noHalo = (/ (i , i=1 , p_patch(n_dom)%cells%ALL%size , 1) /)

    ELSE

        ALLOCATE(noHalo(p_patch(n_dom)%cells%owned%size - p_patch(n_dom)%cells%owned%no_of_holes))

        l = 1

        DO i = 1 , p_patch(n_dom)%cells%ALL%size

            j = MOD(i-1 , nproma) + 1
            k = (i-1) / nproma + 1

            IF (p_patch(n_dom)%cells%decomp_info%halo_level(j,k) == 0) THEN

                noHalo(l) = i
                l = l + 1

            END IF

        END DO

    END IF


    ! pressure

        ! realize export: pres_msl = mean sea level pressure = air_pressure_at_sea_level
        CALL Realize(ExportState , ICON_Mesh , "pres_msl")

    ! wind

        ! realize export: u_10m = zonal wind in 10m = wind_x_velocity_at_10m
        CALL Realize(ExportState , ICON_Mesh , "u_10m")

        ! realize export: v_10m = meridional wind in 10m = wind_y_velocity_at_10m
        CALL Realize(ExportState , ICON_Mesh , "v_10m")
    
    ! temperature

        ! realize import: t_seasfc = sea surface temperature = sea_surface_temperature
        CALL Realize(ImportState , ICON_Mesh , "t_seasfc")
! required for corrected data initialization -> should be removed as soon as possible
        CALL Realize(ExportState , ICON_Mesh , "t_seasfc")

        ! realize export: t_2m = temperature in 2m = air_temperature_at_2m
        CALL Realize(ExportState , ICON_Mesh , "t_2m")
    
        ! realize export: td_2m = dew-point in 2m = dew_point_temperature
        CALL Realize(ExportState , ICON_Mesh , "td_2m")

        ! realize import: t_s = weighted temperature of ground surface
        CALL Realize(ExportState , ICON_Mesh , "t_s")

    ! momentum

        ! realize export: umfl_s = u-momentum flux at the surface = surface_downward_eastward_stress
        CALL Realize(ExportState , ICON_Mesh , "umfl_s")

        ! realize export: vmfl_s = v-momentum flux at the surface = surface_downward_northward_stress
        CALL Realize(ExportState , ICON_Mesh , "vmfl_s")

    ! precipitation

        ! realize export: rain_gsp_rate = gridscale rain rate
        CALL Realize(ExportState , ICON_Mesh , "rain_gsp_rate")

        ! realize export: snow_gsp_rate = gridscale snow rate
        CALL Realize(ExportState , ICON_Mesh , "snow_gsp_rate")

        ! For graupel scheme
        SELECT CASE (atm_phy_nwp_config(n_dom)%inwp_gscp)

            CASE (2,4,5,6)

                ! realize export: graupel_gsp_rate = gridscale graupel rate
                CALL Realize(ExportState , ICON_Mesh , "graupel_gsp_rate")

        END SELECT

        !For two moment microphysics
        SELECT CASE (atm_phy_nwp_config(n_dom)%inwp_gscp)

            CASE (4,5,6)

                ! realize export: ice_gsp_rate = gridscale ice rate
                CALL Realize(ExportState , ICON_Mesh , "ice_gsp_rate")

                ! realize export: hail_gsp_rate = gridscale hail rate
                CALL Realize(ExportState , ICON_Mesh , "hail_gsp_rate")
    
        END SELECT

        ! realize export: rain_con_rate = convective rain rate
        CALL Realize(ExportState , ICON_Mesh , "rain_con_rate")

        ! realize export: snow_con_rate = convective snow rate
        CALL Realize(ExportState , ICON_Mesh , "snow_con_rate")

    ! radiation

        ! realize export: sob_s = shortwave net flux at surface (net without diffusive part) = surface_downwelling_shortwave_flux
        CALL Realize(ExportState , ICON_Mesh , "sob_s")

        ! realize export: thb_s = longwave net flux at surface
        CALL Realize(ExportState , ICON_Mesh , "thb_s")

        ! realize export: sodifd_s = shortwave diffuse downward flux at surface
        CALL Realize(ExportState , ICON_Mesh , "sodifd_s")

    ! heat

        ! realize export: shfl_s = surface sensible heat flux (downward)
        CALL Realize(ExportState , ICON_Mesh , "shfl_s")

        ! realize export: lhfl_s = surface latent heat flux (downward)
        CALL Realize(ExportState , ICON_Mesh , "lhfl_s")

    ! humidity

        ! realize export: qv_s = specific humidity at the surface 
        CALL Realize(ExportState , ICON_Mesh , "qv_s")

        ! realize export: rh_2m = relative humidity in 2m
        CALL Realize(ExportState , ICON_Mesh , "rh_2m")

    ! moisture / cloud cover
    
        ! realize export: qhfl_s = surface moisture flux = surface_downward_water_flux
        CALL Realize(ExportState , ICON_Mesh , "qhfl_s")

        ! realize export: clct = total cloud cover = total_cloud_cover
        CALL Realize(ExportState , ICON_Mesh , "clct")


    CALL message(routine , "returning")

END SUBROUTINE ICON_Init_Phase_2


SUBROUTINE Realize(State , ICON_Mesh , FieldName)

    TYPE(ESMF_State), INTENT(INOUT) :: State
    TYPE(ESMF_Mesh), INTENT(IN)     :: ICON_Mesh

    CHARACTER(LEN=*), INTENT(IN)    :: FieldName

    TYPE(t_var_list)                :: list
    TYPE(t_var_metadata)            :: var_info 

    INTEGER                         :: localrc

    LOGICAL                         :: tf

    TYPE(ESMF_Field)                :: FieldFromState
    TYPE(ESMF_MeshLoc)              :: meshLoc_CVE

    REAL(wp), POINTER               :: FArrayPtr1D(:)
    REAL(wp), POINTER, CONTIGUOUS   :: temp(:) , FArrayPtr2D(:,:)

    CHARACTER(LEN=ESMF_MAXSTR)      :: transferAction

    CHARACTER(LEN=25), PARAMETER    :: routine = "mo_esmf_icon_comp:Realize"


    CALL ESMF_StateGet(State , TRIM(FieldName) , field = FieldFromState , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_StateGet field "//TRIM(FieldName))

    tf = NUOPC_IsConnected(FieldFromState , localrc)
    CALL checkESMF(localrc , routine , "NUOPC_GetAttribute Connected "//TRIM(FieldName))

    IF (tf) THEN

        CALL NUOPC_GetAttribute(FieldFromState , name = "TransferActionGeomObject" , value = transferAction , rc = localrc)
        CALL checkESMF(localrc , routine , "NUOPC_GetAttribute TransferActionGeomObject "//TRIM(FieldName))

!WRITE(0,*) routine , ": " , TRIM(FieldName) , ": TransferActionGeomObject: " , TRIM(transferAction)
            
        list = p_nh_state_lists(n_dom)%diag_list
        CALL get_var_list_element_info(list , TRIM(FieldName) , var_info)

        IF (TRIM(FieldName) /= TRIM(var_info%name)) THEN

            list = prm_nwp_diag_list(n_dom)
            CALL get_var_list_element_info(list , TRIM(FieldName) , var_info)

            IF (TRIM(FieldName) /= TRIM(var_info%name)) THEN

                list = p_lnd_state(n_dom)%lnd_diag_nwp_list
                CALL get_var_list_element_info(list , TRIM(FieldName) , var_info)

                IF (TRIM(FieldName) /= TRIM(var_info%name)) THEN

                    CALL checkESMF(-1 , routine , "Variable "//TRIM(FieldName)//" not found in ICON!!!")

                END IF

            END IF

        END IF

        SELECT CASE(var_info%hgrid)

            CASE (1)

                meshLoc_CVE = ESMF_MESHLOC_ELEMENT

            CASE (2)

                !meshLoc_CVE = ESMF_MESHLOC_NODE
                CALL checkESMF(-1 , routine , "variable location on vertex not supported")

            CASE (3)

                CALL checkESMF(-1 , routine , "variable location on edge not supported")

            CASE default

                CALL checkESMF(-1 , routine , "variable location not recognised")

        END SELECT


        ! realize ESMF_Field for ICON
        CALL ESMF_FieldEmptySet(FieldFromState , mesh = ICON_Mesh , meshloc = meshLoc_CVE , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldEmptySet ICON mesh "//TRIM(var_info%name))

        CALL ESMF_FieldEmptyComplete(FieldFromState , ESMF_TYPEKIND_R8 , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldCreate "//TRIM(var_info%name))


        CALL NUOPC_Realize(State , FieldFromState , rc = localrc)
        CALL checkESMF(localrc , routine , "NUOPC_Realize "//TRIM(FieldName))


        ! get pointer to array of ESMF_MeshLoc of ESMF_Field
        CALL ESMF_FieldGet(FieldFromState , farrayPtr = FArrayPtr1D , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldGet "//TRIM(var_info%name)//" pointer")

        CALL get_var(list , TRIM(var_info%name) , FArrayPtr2D)

        IF (.NOT. ASSOCIATED(FArrayPtr2D)) THEN

            CALL checkESMF(-1 , routine , "Find "//TRIM(var_info%name)//" in list: not associated")

        END IF


        temp(1:SIZE(FArrayPtr2D)) => FArrayPtr2D
        FArrayPtr1D(1:SIZE(FArrayPtr1D)) = temp(noHalo) 

        CALL message(routine , TRIM(FieldName)//": connected: yes")

    ELSE

        CALL NUOPC_Realize(State , ICON_Mesh , fieldName = TRIM(FieldName) , &
            & selection = "realize_connected_remove_others" , rc = localrc)
        CALL checkESMF(localrc , routine , "NUOPC_Realize (remove) "//TRIM(FieldName))

        CALL message(routine , TRIM(FieldName)//": connected: no")

    END IF    

END SUBROUTINE Realize


SUBROUTINE ICON_Comp_SetClock(ICON_Comp , localrc)
    
    TYPE(ESMF_GridComp)             :: ICON_Comp

    INTEGER, INTENT(OUT)            :: localrc

    TYPE(ESMF_Clock)                :: ICON_Clock
    TYPE(ESMF_TimeInterval)         :: ICON_TimeStep

    CHARACTER(LEN=36), PARAMETER    :: routine = "mo_esmf_icon_comp:ICON_Comp_SetClock"


    CALL NUOPC_ModelGet(ICON_Comp , modelClock = ICON_Clock , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_ModelGet ICON clock")

    CALL ESMF_TimeIntervalSet(ICON_TimeStep , s = INT(dtime) , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_TimeIntervalSet ICON time step")

    CALL NUOPC_CompSetClock(ICON_Comp , ICON_Clock , ICON_TimeStep , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompSetClock ICON time step")

    timeCount = 0

END SUBROUTINE ICON_Comp_SetClock


SUBROUTINE ICON_Comp_Advance(ICON_Comp , localrc)
    
    TYPE(ESMF_GridComp)                                     :: ICON_Comp

    INTEGER, INTENT(OUT)                                    :: localrc

    TYPE(ESMF_State)                                        :: ImportState , ExportState

    TYPE(ESMF_Clock)                                        :: ICON_Clock
    TYPE(ESMF_Time)                                         :: runtime , startTime , stopTime
    TYPE(ESMF_TimeInterval)                                 :: timeStep
    
    INTEGER                                                 :: i , nCount

    CHARACTER(LEN=128), ALLOCATABLE, DIMENSION(:)           :: itemNames
    
    TYPE(ESMF_StateItem_Flag), ALLOCATABLE, DIMENSION(:)    :: itemTypes

    CHARACTER(LEN=35), PARAMETER                            :: routine = "mo_esmf_icon_comp:ICON_Comp_Advance"


    CALL NUOPC_ModelGet(ICON_Comp , modelClock = ICON_Clock , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_ModelGet ICON clock")

    CALL ESMF_ClockGet(ICON_Clock , timestep = timeStep , currTime = runTime , &
        & startTime = startTime , stopTime = stopTime , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_ClockGet")

    IF (timeCount == 0) THEN
        
        CALL ESMF_LogWrite("ICON: advance" , ESMF_LOGMSG_INFO , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_LogWrite ICON advance")

        ! update import: copy field data to ICON data
        CALL NUOPC_ModelGet(ICON_Comp ,importState = ImportState , rc = localrc)
        CALL checkESMF(localrc , routine , "NUOPC_ModelGet ICON import state")

        CALL ESMF_StateGet(ImportState , itemCount = nCount , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet ICON import nCount")

        IF (nCount > 0) THEN

            ALLOCATE(itemNames(nCount))
            ALLOCATE(itemTypes(nCount))

            CALL ESMF_StateGet(ImportState , itemNameList = itemNames , itemTypeList = itemTypes , rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_StateGet ICON import names and types")

            DO i = 1 , nCount

                IF (itemTypes(i) == ESMF_STATEITEM_FIELD) CALL updateImport(ImportState , TRIM(itemNames(i)))

            END DO

            DEALLOCATE(itemNames , itemTypes)

        END IF

    END IF


    ! advance icon time loop (one time step)

    CALL icon_run()

    timeCount = timeCount + 1


    IF (runtime + timeStep == stopTime) THEN
        
        CALL NUOPC_ModelGet(ICON_Comp , importState = ImportState , exportState = ExportState , rc = localrc)
        CALL checkESMF(localrc , routine , "NUOPC_ModelGet ICON import state")


        ! prepare import: copy ICON data to field data

        CALL ESMF_StateGet(ImportState , itemCount = nCount , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet ICON import nCount")

        IF (nCount > 0) THEN

            ALLOCATE(itemNames(nCount))
            ALLOCATE(itemTypes(nCount))

            CALL ESMF_StateGet(ImportState , itemNameList = itemNames , itemTypeList = itemTypes , rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_StateGet ICON import names and types")

            DO i = 1 , nCount

                IF (itemTypes(i) == ESMF_STATEITEM_FIELD) CALL updateESMF_Fields(ImportState , TRIM(itemNames(i)))

            END DO

            DEALLOCATE(itemNames , itemTypes)

        END IF

        ! update field data for export

        CALL ESMF_StateGet(ExportState , itemCount = nCount , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet ICON export nCount")

        IF (nCount > 0) THEN

            ALLOCATE(itemNames(nCount))
            ALLOCATE(itemTypes(nCount))

            CALL ESMF_StateGet(ExportState , itemNameList = itemNames , itemTypeList = itemTypes , rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_StateGet ICON export names and types")

            DO i = 1 , nCount

                IF (itemTypes(i) == ESMF_STATEITEM_FIELD) CALL updateESMF_Fields(ExportState , TRIM(itemNames(i)))

            END DO

            DEALLOCATE(itemNames , itemTypes)

        END IF

        timeCount = 0

        CALL message(routine , "data exchange ...")  
        
    END IF

END SUBROUTINE ICON_Comp_Advance


SUBROUTINE updateImport(ImportState , FieldName)

    TYPE(ESMF_State), INTENT(INOUT) :: ImportState

    CHARACTER(LEN=*), INTENT(IN)    :: FieldName

    INTEGER                         :: localrc

    INTEGER                         :: n_now , n_new 

    TYPE(ESMF_Field)                :: ImportField

    REAL(wp), POINTER               :: FArrayPtr1D(:)
    REAL(wp), POINTER, CONTIGUOUS   :: temp(:) , FArrayPtr2D(:,:)

    TYPE(t_var_list)                :: list

    CHARACTER(LEN=1024)             :: timeString

    CHARACTER(LEN=30), PARAMETER    :: routine = "mo_esmf_icon_comp:updateImport"


    ! get ESMF_Field from ESMF_State

    CALL ESMF_StateGet(ImportState , TRIM(FieldName) , field = ImportField , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_StateGet "//TRIM(FieldName))


    ! get pointer to array of ESMF_Field

    CALL ESMF_FieldGet(ImportField , farrayPtr = FArrayPtr1D , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_FieldGet "//TRIM(FieldName)//" pointer")


    ! set ICON-variables from GETM ...

    NULLIFY(FArrayPtr2D)

    list = p_lnd_state(n_dom)%lnd_diag_nwp_list
    CALL get_var(list , TRIM(FieldName) , FArrayPtr2D)
        
    IF (.NOT. ASSOCIATED(FArrayPtr2D)) THEN

        list = prm_nwp_diag_list(n_dom)
        CALL get_var(list , TRIM(FieldName) , FArrayPtr2D)

        IF (.NOT. ASSOCIATED(FArrayPtr2D)) THEN

            CALL checkESMF(-1 , routine , "Variable "//TRIM(FieldName)//" not found in ICON!!!")

        END IF

    END IF

    temp(1:SIZE(FArrayPtr2D)) => FArrayPtr2D
    temp(noHalo) = FArrayPtr1D(1:SIZE(FArrayPtr1D))

    CALL sync_patch_array(SYNC_C , p_patch(n_dom) , FArrayPtr2D , TRIM(FieldName))

    IF (TRIM(FieldName) == "t_seasfc") THEN

        ! this is only required for SSTICE_ANA case since it is not
        ! automatically performed within next time loop body call 
        ! compare with ICON code src/lnd_phy_nwp/mo_td_ext_data.f90:138
        IF (iforcing == inwp .AND. SSTICE_ANA == sstice_mode)  THEN

            CALL set_sst_and_seaice (.FALSE. , assumePrevMidnight(time_config%tc_current_date) , &
                                   & mtime_old , sstice_mode , p_patch(n_dom) , ext_data(n_dom) , p_lnd_state(n_dom))
      
            CALL update_sst_and_seaice(p_patch(n_dom) , ext_data(n_dom) , p_lnd_state(n_dom) , p_nh_state(n_dom) , &
                                     & sstice_mode , assumePrevMidnight(time_config%tc_exp_startdate) , &
                                     & assumePrevMidnight(time_config%tc_current_date))
      
        END IF

    END IF

END SUBROUTINE updateImport


SUBROUTINE updateESMF_Fields(state , FieldName)

    TYPE(ESMF_State), INTENT(INOUT) :: State

    CHARACTER(LEN=*), INTENT(IN)    :: FieldName

    INTEGER                         :: localrc

    TYPE(ESMF_Field)                :: FieldFromState

    REAL(wp), POINTER               :: FArrayPtr1D(:)
    REAL(wp), POINTER, CONTIGUOUS   :: temp(:)
    REAL(wp), POINTER, CONTIGUOUS   :: FArrayPtr2D(:,:)

    TYPE(t_var_list)                :: list

    CHARACTER(LEN=35), PARAMETER    :: routine = "mo_esmf_icon_comp:updateESMF_Fields"


    ! get ESMF_Field from ESMF_State

    CALL ESMF_StateGet(State , TRIM(FieldName) , field = FieldFromState , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_StateGet ICON "//TRIM(FieldName))
    
    
    ! get pointer to array of ESMF_Field

    CALL ESMF_FieldGet(FieldFromState , farrayPtr = FArrayPtr1D , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_FieldGet ICON "//TRIM(FieldName)//" pointer")

    NULLIFY(FArrayPtr2D)

    list = p_nh_state_lists(n_dom)%diag_list
    CALL get_var(list , TRIM(FieldName) , FArrayPtr2D)

    IF (.NOT. ASSOCIATED(FArrayPtr2D)) THEN

        list = prm_nwp_diag_list(n_dom)
        CALL get_var(list , TRIM(FieldName) , FArrayPtr2D)

        IF (.NOT. ASSOCIATED(FArrayPtr2D)) THEN

            list = p_lnd_state(n_dom)%lnd_diag_nwp_list
            CALL get_var(list , TRIM(FieldName) , FArrayPtr2D)

            IF (.NOT. ASSOCIATED(FArrayPtr2D)) THEN

                CALL checkESMF(-1 , routine , "Variable "//TRIM(FieldName)//" not found in ICON!!!")

            END IF

        END IF

    END IF

    temp(1:SIZE(FArrayPtr2D)) => FArrayPtr2D
    FArrayPtr1D(1:SIZE(FArrayPtr1D)) = temp(noHalo) 

END SUBROUTINE updateESMF_Fields


SUBROUTINE ICON_Comp_Finalize(ICON_Comp , localrc)

    TYPE(ESMF_GridComp)             :: ICON_Comp

    INTEGER, INTENT(OUT)            :: localrc

    TYPE(ESMF_State)                :: ImportState
    TYPE(ESMF_Mesh)                 :: ICON_Mesh
    
    INTEGER                                                 :: i , nCount

    CHARACTER(LEN=128), ALLOCATABLE, DIMENSION(:)           :: itemNames

    TYPE(ESMF_StateItem_Flag), ALLOCATABLE, DIMENSION(:)    :: itemTypes

    CHARACTER(LEN=36), PARAMETER    :: routine = "mo_esmf_icon_comp:ICON_Comp_Finalize"


    CALL ESMF_LogWrite("ICON: finalize" , ESMF_LOGMSG_INFO , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_LogWrite ICON finalize")

    ! update import: copy field data to ICON data
    CALL NUOPC_ModelGet(ICON_Comp , importState = ImportState , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_ModelGet ICON import state")

    CALL ESMF_StateGet(ImportState , itemCount = nCount , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_StateGet ICON import nCount")

    IF (nCount > 0) THEN

        ALLOCATE(itemNames(nCount))
        ALLOCATE(itemTypes(nCount))

        CALL ESMF_StateGet(ImportState , itemNameList = itemNames , itemTypeList = itemTypes , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet ICON import names and types")

        DO i = 1 , nCount

            IF (itemTypes(i) == ESMF_STATEITEM_FIELD) CALL updateImport(ImportState , TRIM(itemNames(i)))

        END DO

        DEALLOCATE(itemNames , itemTypes)

    END IF


    CALL icon_final()
 
    CALL ESMF_GridCompGet(ICON_Comp , mesh = ICON_Mesh , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_GridCompGet ICON mesh")


    ! destroy ESMF_Mesh
    CALL destruct_ICON_Mesh(ICON_Mesh) 
    
    CALL message(routine , "Gridded Comp Final returning")

END SUBROUTINE ICON_Comp_Finalize


END MODULE mo_esmf_icon_comp

