MODULE mo_esmf_icon_data

    USE mo_linked_list,         ONLY: t_var_list , t_list_element
    USE mo_util_hash,           ONLY: util_hashword
    USE mo_var_list_element,    ONLY: t_var_list_element
    USE mo_var_list,            ONLY: nvar_lists , var_lists

    IMPLICIT NONE

    PUBLIC :: get_var_list_with_var

CONTAINS

SUBROUTINE get_var_list_with_var(this_list, var_name)

    TYPE(t_var_list), POINTER :: this_list ! pointer

    CHARACTER(len=*), INTENT(IN) :: var_name  ! name of variable

    TYPE(t_list_element), POINTER :: this_list_element

    INTEGER :: i , key


    NULLIFY(this_list)

    key = util_hashword(var_name , LEN_TRIM(var_name) , 0)

    DO i = 1 , nvar_lists

        this_list => var_lists(i)
        this_list_element => this_list%p%first_list_element

        DO WHILE (ASSOCIATED(this_list_element))

            IF (key == this_list_element%field%info%key) RETURN

            this_list_element => this_list_element%next_list_element
        
        ENDDO

    END DO
        
    NULLIFY(this_list)

END SUBROUTINE get_var_list_with_var


END MODULE mo_esmf_icon_data
