MODULE mo_esmf_math_utilities

    USE mo_kind,            ONLY: wp
    USE mo_math_constants,  ONLY: dbl_eps
    USE mo_math_types,      ONLY: t_cartesian_coordinates , t_geographical_coordinates
    USE mo_math_utilities,  ONLY: gc2cc , project_point_to_plane

    PUBLIC :: point_in_ICON_triangle , point_in_polygon

CONTAINS

    
INTEGER FUNCTION point_in_ICON_triangle(p_Triangle , vert) 

    TYPE(t_geographical_coordinates), INTENT(IN) :: p_Triangle(3) , vert 

    TYPE(t_cartesian_coordinates) :: p_Test(5)
    TYPE(t_geographical_coordinates) :: p(3) , verts
    
    p_Test(1) = gc2cc(p_Triangle(1))
    p_Test(2) = gc2cc(p_Triangle(2))
    p_Test(3) = gc2cc(p_Triangle(3))
    p_Test(4) = gc2cc(vert)

    p_Test(5) = project_point_to_plane(p_Test(4) , p_Test(1) , p_Test(2) , p_Test(3))

    p%lon = p_Test(1:3)%x(1)
    p%lat = p_Test(1:3)%x(2)
    verts%lon = p_Test(5)%x(1)
    verts%lat = p_Test(5)%x(2)

    point_in_ICON_triangle = point_in_polygon(p , verts)

END FUNCTION point_in_ICON_triangle


INTEGER FUNCTION point_in_polygon(p , vert) 

    TYPE(t_geographical_coordinates), INTENT(IN) , DIMENSION(:) :: p
    TYPE(t_geographical_coordinates), INTENT(IN) :: vert

    INTEGER :: nP , i , tf 

    nP = SIZE(p)
    tf = -1
    
    DO i = 1 , nP

        tf = tf * CrossProdTest(vert , p(i) , p(MOD(i,nP)+1))

    END DO

    point_in_polygon = MAX(tf , 0)

END FUNCTION point_in_polygon


INTEGER FUNCTION CrossProdTest(vert , p_i , p_j)

    TYPE(t_geographical_coordinates), INTENT(IN) :: vert , p_i , p_j

    INTEGER :: tf

    TYPE(t_geographical_coordinates) :: A , B , C , tmp

    REAL(wp) :: d


    A = vert
    B = p_i
    C = p_j

    IF (ABS(A%lat - B%lat) < dbl_eps .AND. ABS(B%lat - C%lat) < dbl_eps .AND. &
      & ABS(A%lat - C%lat) < dbl_eps) THEN

        IF ((B%lon <= A%lon .AND. A%lon <= C%lon) .OR. (C%lon <= A%lon .AND. A%lon <= B%lon)) THEN

            tf = 0

        ELSE

            tf = 1

        END IF

    ELSE

        IF (B%lat > C%lat) THEN

            tmp = B
            B = C
            C = tmp

        END IF

        IF (ABS(A%lat - B%lat) < dbl_eps .AND. ABS(A%lon - B%lon) < dbl_eps) THEN

            tf = 0

        ELSEIF (A%lat <= B%lat .OR. A%lat > C%lat) THEN

            tf = 1

        ELSE

            d = (B%lon - A%lon) * (C%lat - A%lat) - (B%lat - A%lat) * (C%lon - A%lon)

            IF (d > 0.0_wp) THEN

                tf = -1

            ELSEIF (d < 0.0_wp) THEN

                tf = 1

            ELSE

                tf = 0

            END IF

        END IF

    END IF

    CrossProdTest = tf

END FUNCTION CrossProdTest


END MODULE mo_esmf_math_utilities
