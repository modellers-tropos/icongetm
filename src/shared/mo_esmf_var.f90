MODULE mo_esmf_var

    IMPLICIT NONE

    PRIVATE :: t_config_ESMF

        TYPE t_config_ESMF

            LOGICAL :: loverwrite , lowned

            CHARACTER(LEN=128), DIMENSION(:), ALLOCATABLE :: vars_ICON_Import , vars_ICON_Export
            CHARACTER(LEN=128), DIMENSION(:), ALLOCATABLE :: vars_GETM_Import , vars_GETM_Export
            CHARACTER(LEN=1024) :: outputArray

            INTEGER, DIMENSION(:), ALLOCATABLE :: startTime , timestep , endTime

            INTEGER :: nProc_ICON , nProc_GETM

            CHARACTER(LEN=4) :: verbosity

        END TYPE t_config_ESMF

!    PUBLIC :: ESMF_COMM_WORLD , ESMF_GETM_COMM
    PUBLIC :: config_ESMF
    PUBLIC :: nVar_ICON2GETM , nVar_GETM2ICON
    PUBLIC :: nVar_2GETM , nVar_2ICON

!        INTEGER :: ESMF_COMM_WORLD , ESMF_GETM_COMM
        INTEGER, DIMENSION(2) :: nVar_ICON2GETM , nVar_GETM2ICON
        INTEGER :: nVar_2ICON , nVar_2GETM

        TYPE(t_config_ESMF) :: config_ESMF

END MODULE mo_esmf_var

