MODULE mo_esmf_coupler_comp

    USE ESMF
    USE NUOPC
    USE NUOPC_Connector,    ONLY: Coupl_SetServices        => SetServices , &
                                & Label_ComputeRouteHandle => label_ComputeRouteHandle , &
                                & Label_ExecuteRouteHandle => label_ExecuteRouteHandle , &
                                & Label_ReleaseRouteHandle => label_ReleaseRouteHandle , &
                                & NUOPC_ConnectorGet , NUOPC_ConnectorSet

    USE mo_esmf_utilities,      ONLY: checkESMF
    USE mo_esmf_var,            ONLY: config_ESMF

    IMPLICIT NONE

    PUBLIC :: SetServices

    INTEGER :: my_rank , nProc
    INTEGER :: nField_Import , nField_Export

    REAL(8), ALLOCATABLE, DIMENSION(:) :: SSTFracICON

CONTAINS


SUBROUTINE SetServices(COUPLER_Comp , localrc)

    TYPE(ESMF_CplComp) :: COUPLER_Comp

    INTEGER, INTENT(OUT) :: localrc

    CHARACTER(LEN=32), PARAMETER :: routine = "mo_esmf_coupler_comp:SetServices"

    
    CALL NUOPC_CompDerive(COUPLER_Comp , Coupl_SetServices , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompDerive")
    
    CALL NUOPC_CompSpecialize(COUPLER_Comp , specLabel = Label_ComputeRouteHandle , &
        & specRoutine = ComputeRouteHandle , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompSpecialize Coupler compute route handle")

    CALL NUOPC_CompSpecialize(COUPLER_Comp , specLabel = Label_ExecuteRouteHandle , &
        & specRoutine = ExecuteRouteHandle , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompSpecialize Coupler execute route handle")

    CALL NUOPC_CompSpecialize(COUPLER_Comp , specLabel = Label_ReleaseRouteHandle , &
        & specRoutine = ReleaseRouteHandle , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompSpecialize Coupler release route handle")

END SUBROUTINE SetServices


SUBROUTINE ComputeRouteHandle(COUPLER_Comp , localrc)

    TYPE(ESMF_CplComp)                  :: COUPLER_Comp

    INTEGER, INTENT(OUT)                :: localrc

    TYPE(ESMF_State)                    :: InternalState
    TYPE(ESMF_FieldBundle)              :: srcFieldBundle , dstFieldBundle , ICONGETM_XGrid_FieldBundle
    TYPE(ESMF_Field), ALLOCATABLE       :: srcFields(:) , dstFields(:) , dstFracFields(:) , ICONGETM_XGrid_Fields(:)
    TYPE(ESMF_RouteHandle), ALLOCATABLE :: src2xgrid_RouteHandle(:) , xgrid2dst_RouteHandle(:)
    TYPE(ESMF_Grid)                     :: GETM_Grid
    TYPE(ESMF_Mesh)                     :: ICON_Mesh
    TYPE(ESMF_XGrid)                    :: ICONGETM_XGrid
    
    INTEGER                             :: i , nSrcFields , nDstFields

    REAL(8), POINTER                    :: FArrayPtr_XGrid_Field(:)
    
    CHARACTER(LEN=128)                  :: srcFieldName , dstFieldName , COUPLER_Name

REAL(8), POINTER :: FArrayPtr1D(:) , FArrayPtr1DFrac(:)
REAL(8), ALLOCATABLE, DIMENSION(:) :: temp
INTEGER :: localDeCount

    CHARACTER(LEN=39), PARAMETER        :: routine = "mo_esmf_coupler_comp:ComputeRouteHandle"

    CALL NUOPC_ConnectorGet(COUPLER_Comp , state = InternalState , srcFields = srcFieldBundle , &
        & dstFields = dstFieldBundle , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_ConnectorGet internal state")

    CALL ESMF_CplCompGet(COUPLER_Comp , name = COUPLER_Name , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_CplCompGet name")

    IF (COUPLER_Name == "ICON-TO-GETM") THEN

        CALL ESMF_FieldBundleGet(srcFieldBundle , mesh = ICON_Mesh , fieldCount = nSrcFields , itemorderflag = ESMF_ITEMORDER_ADDORDER , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldBundleGet ICON mesh")

        CALL ESMF_FieldBundleGet(dstFieldBundle , grid = GETM_Grid , fieldCount = nDstFields , itemorderflag = ESMF_ITEMORDER_ADDORDER , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldBundleGet GETM grid")

    ELSEIF (COUPLER_Name == "GETM-TO-ICON") THEN
        
        CALL ESMF_FieldBundleGet(srcFieldBundle , grid = GETM_Grid , fieldCount = nSrcFields , itemorderflag = ESMF_ITEMORDER_ADDORDER , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldBundleGet GETM grid")

        CALL ESMF_FieldBundleGet(dstFieldBundle , mesh = ICON_Mesh , fieldCount = nDstFields , itemorderflag = ESMF_ITEMORDER_ADDORDER , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldBundleGet ICON mesh")

    ELSE

        CALL checkESMF(-1 , routine , "COUPLER ("//TRIM(COUPLER_Name)//"): not recognized")

    ENDIF

    IF (nSrcFields /= nDstFields) THEN

        CALL checkESMF(-1 , routine , "number of source and destination fields are not identical")

    END IF

    ALLOCATE(srcFields(nSrcFields))
    ALLOCATE(dstFields(nDstFields))
    ALLOCATE(ICONGETM_XGrid_Fields(nSrcFields))
    ALLOCATE(src2xgrid_RouteHandle(nSrcFields))
    ALLOCATE(xgrid2dst_RouteHandle(nDstFields))
    ALLOCATE(dstFracFields(nDstFields))

    CALL ESMF_FieldBundleGet(srcFieldBundle , fieldList = srcFields , itemorderflag = ESMF_ITEMORDER_ADDORDER , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_FieldBundleGet source fields")

    CALL ESMF_FieldBundleGet(dstFieldBundle , fieldList = dstFields , itemorderflag = ESMF_ITEMORDER_ADDORDER , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_FieldBundleGet destination fields")

    ICONGETM_XGrid_FieldBundle = ESMF_FieldBundleCreate(name = TRIM(COUPLER_Name)//"_XGrid_FieldBundle" , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_FieldBundleCreate "//TRIM(COUPLER_Name)//" xgrid field bundle")

    DO i = 1 , nSrcFields

        CALL ESMF_FieldGet(srcFields(i) , name = srcFieldName , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldGet "//TRIM(srcFieldName))

        CALL ESMF_FieldGet(dstFields(i) , name = dstFieldName , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldGet "//TRIM(dstFieldName))

        IF (COUPLER_Name == "ICON-TO-GETM") THEN

            CALL ESMF_FieldGet(srcFields(i) , mesh = ICON_Mesh , rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_FieldGet ICON mesh "//TRIM(srcFieldName))

            CALL ESMF_FieldGet(dstFields(i) , grid = GETM_Grid , rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_FieldGet GETM grid "//TRIM(dstFieldName))

            ICONGETM_XGrid = ESMF_XGridCreate(sideAMesh = (/ ICON_Mesh /) , sideBGrid = (/ GETM_Grid /) , &
!                & storeOverlay = .TRUE. , &
                & rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_XGridCreate "//TRIM(COUPLER_Name))

        ELSEIF (COUPLER_Name == "GETM-TO-ICON") THEN

            CALL ESMF_FieldGet(srcFields(i) , grid = GETM_Grid , rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_FieldGet GETM grid "//TRIM(srcFieldName))

            CALL ESMF_FieldGet(dstFields(i) , mesh = ICON_Mesh , rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_FieldGet ICON mesh "//TRIM(dstFieldName))

            ICONGETM_XGrid = ESMF_XGridCreate(sideAGrid = (/ GETM_Grid /) , sideBMesh = (/ ICON_Mesh /) , & 
!                & storeOverlay = .TRUE. , &
                & sideAMaskValues = (/ 0 /) , &
                & sideBMaskValues = (/ 0 /) , &
                & rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_XGridCreate "//TRIM(COUPLER_Name))

        END IF
        
        ICONGETM_XGrid_Fields(i) = ESMF_FieldCreate(ICONGETM_XGrid , ESMF_TYPEKIND_R8 , &
            & name = TRIM(srcFieldName)//"_2_"//TRIM(dstFieldName) , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldCreate "//TRIM(srcFieldName)//"_2_"//TRIM(dstFieldName))

        CALL ESMF_FieldGet(ICONGETM_XGrid_Fields(i) , farrayPtr = FArrayPtr_XGrid_Field , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldGet "//TRIM(srcFieldName)//"_2_"//TRIM(dstFieldName)//" pointer")

        FArrayPtr_XGrid_Field(:) = 0.0_8

        ! prepare ESMF_FieldRegrid - create RouteHandle
        CALL ESMF_FieldRegridStore(ICONGETM_Xgrid , srcFields(i) , ICONGETM_XGrid_Fields(i) , &
!            & regridmethod = ESMF_REGRIDMETHOD_CONSERVE_2ND , &
            & routeHandle = src2xgrid_RouteHandle(i) , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldRegridStore "//TRIM(srcFieldName)//"2XGrid_RouteHandle")

        CALL ESMF_RouteHandleSet(src2xgrid_RouteHandle(i) , name = TRIM(srcFieldName)//"2XGrid_RouteHandle" , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_RouteHandleSet "//TRIM(srcFieldName)//"2XGrid route handle")        

! seems to be necessary, but not understandable -> ask ESMF suuport
IF (COUPLER_Name == "ICON-TO-GETM") THEN

        CALL ESMF_FieldRegridStore(ICONGETM_Xgrid , ICONGETM_XGrid_Fields(i) , dstFields(i) , &
!            & regridmethod = ESMF_REGRIDMETHOD_CONSERVE_2ND , &
            & routeHandle = xgrid2dst_RouteHandle(i) , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldRegridStore XGrid2"//TRIM(dstFieldName)//"_RouteHandle")    

ELSEIF (COUPLER_Name == "GETM-TO-ICON") THEN

    CALL ESMF_FieldGet(dstFields(i) , localDeCount = localDeCount , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_FieldGet localDeCount "//TRIM(dstFieldName))

    IF (localDeCount > 0) THEN

        CALL ESMF_FieldGet(dstFields(i) , fArrayPtr = FArrayPtr1D , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldGet pointer "//TRIM(dstFieldName))
            
        ALLOCATE(temp(SIZE(FArrayPtr1D)))
                
        temp = FArrayPtr1D

    END IF
       
    dstFracFields(i) = ESMF_FieldCreate(ICON_Mesh , ESMF_TYPEKIND_R8 , meshloc = ESMF_MESHLOC_ELEMENT , &
        & name = "Frac_"//TRIM(dstFieldName) , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_FieldCreate destination fraction "//TRIM(dstFieldName)) 

        CALL ESMF_FieldRegridStore(ICONGETM_Xgrid , ICONGETM_XGrid_Fields(i) , dstFields(i) , &
!            & regridmethod = ESMF_REGRIDMETHOD_CONSERVE_2ND , &
            & dstFracField = dstFracFields(i) , &
            & routeHandle = xgrid2dst_RouteHandle(i) , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldRegridStore XGrid2"//TRIM(dstFieldName)//"_RouteHandle")

    IF (localDeCount > 0) THEN
    
        CALL ESMF_FieldGet(dstFracFields(i) , fArrayPtr = FArrayPtr1DFrac , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldGet ICON mesh fraction "//TRIM(dstFieldName))

        ALLOCATE(SSTFracICON(SIZE(FArrayPtr1DFrac)))

        SSTFracICON = FArrayPtr1DFrac

        FArrayPtr1D = temp
            
        DEALLOCATE(temp)

    END IF

    CALL ESMF_FieldDestroy(dstFracFields(i) , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_FieldDestroy ICON mesh fraction "//TRIM(dstFieldName))

END IF

        CALL ESMF_RouteHandleSet(xgrid2dst_RouteHandle(i) , name = "XGrid2"//TRIM(dstFieldName)//"_RouteHandle" , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_RouteHandleSet XGrid2"//TRIM(dstFieldName)//" route handle")

    END DO

    CALL ESMF_FieldBundleAdd(ICONGETM_XGrid_FieldBundle , ICONGETM_XGrid_Fields , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_FieldBundleAdd ICONGETM xgrid fields")

    CALL ESMF_StateAdd(InternalState , (/ ICONGETM_XGrid_FieldBundle /) , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_StateAdd ICONGETM xgrid field bundle")

    CALL ESMF_StateAdd(InternalState , src2xgrid_RouteHandle , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_StateAdd ICONGETM xgrid field bundle")
    
    CALL ESMF_StateAdd(InternalState , xgrid2dst_RouteHandle , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_StateAdd ICONGETM xgrid field bundle")

    DEALLOCATE(srcFields , dstFields , ICONGETM_XGrid_Fields , src2xgrid_RouteHandle , xgrid2dst_RouteHandle , dstFracFields)

END SUBROUTINE ComputeRouteHandle


SUBROUTINE ExecuteRouteHandle(COUPLER_Comp , localrc)

    TYPE(ESMF_CplComp)                  :: COUPLER_Comp

    INTEGER, INTENT(OUT)                :: localrc

    TYPE(ESMF_State)                    :: InternalState
    TYPE(ESMF_FieldBundle)              :: srcFieldBundle , dstFieldBundle , ICONGETM_XGrid_FieldBundle
    TYPE(ESMF_Field), ALLOCATABLE       :: srcFields(:) , dstFields(:) , ICONGETM_XGrid_Fields(:)
    TYPE(ESMF_RouteHandle), ALLOCATABLE :: src2xgrid_RouteHandle(:) , xgrid2dst_RouteHandle(:)

    INTEGER                             :: i , nFields

    CHARACTER(LEN=128)                  :: srcFieldName , dstFieldName , COUPLER_Name

REAL(8), ALLOCATABLE, DIMENSION(:) :: temp
REAL(ESMF_KIND_R8), POINTER :: FArrayPtr1D(:)
INTEGER :: localDeCount

    CHARACTER(LEN=39), PARAMETER        :: routine = "mo_esmf_coupler_comp:ExecuteRouteHandle"


    CALL NUOPC_ConnectorGet(COUPLER_Comp , state = InternalState , srcFields = srcFieldBundle , &
        & dstFields = dstFieldBundle , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_ConnectorGet internal state")

    CALL ESMF_CplCompGet(COUPLER_Comp , name = COUPLER_Name , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_CplCompGet name")

    CALL ESMF_StateGet(InternalState , TRIM(COUPLER_Name)//"_XGrid_FieldBundle" , ICONGETM_XGrid_FieldBundle , rc = localrc) 
    CALL checkESMF(localrc , routine , "ESMF_StateGet "//TRIM(COUPLER_Name)//" xgrid field bundle")

    CALL ESMF_FieldBundleGet(ICONGETM_XGrid_FieldBundle , fieldCount = nFields , itemorderflag = ESMF_ITEMORDER_ADDORDER , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_FieldBundleGet number of ICONGETM xgrid fields")

    ALLOCATE(srcFields(nFields))
    ALLOCATE(dstFields(nFields))
    ALLOCATE(ICONGETM_XGrid_Fields(nFields))
    ALLOCATE(src2xgrid_RouteHandle(nFields))
    ALLOCATE(xgrid2dst_RouteHandle(nFields))

    CALL ESMF_FieldBundleGet(srcFieldBundle , fieldList = srcFields , itemorderflag = ESMF_ITEMORDER_ADDORDER , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_FieldBundleGet source fields")

    CALL ESMF_FieldBundleGet(dstFieldBundle , fieldList = dstFields , itemorderflag = ESMF_ITEMORDER_ADDORDER , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_FieldBundleGet destination fields")

    CALL ESMF_FieldBundleGet(ICONGETM_XGrid_FieldBundle , fieldList = ICONGETM_XGrid_Fields , itemorderflag = ESMF_ITEMORDER_ADDORDER , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_FieldBundleGet ICONGETM xgrid fields")

    DO i = 1 , nFields

        CALL ESMF_FieldGet(srcFields(i) , name = srcFieldName , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldGet "//TRIM(srcFieldName))

        CALL ESMF_StateGet(InternalState , TRIM(srcFieldName)//"2XGrid_RouteHandle" , src2xgrid_RouteHandle(i) , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet "//TRIM(srcFieldName)//"2XGrid route handle")

        CALL ESMF_FieldRegrid(srcFields(i) , ICONGETM_XGrid_Fields(i) , routeHandle = src2xgrid_RouteHandle(i) , &
            & rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldRegrid "//TRIM(srcFieldName)//"2XGrid")

        CALL ESMF_FieldGet(dstFields(i) , name = dstFieldName , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldGet "//TRIM(dstFieldName))

IF (TRIM(COUPLER_Name) == "GETM-TO-ICON") THEN

    CALL ESMF_FieldGet(dstFields(i) , localDeCount = localDeCount , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_FieldGet "//TRIM(dstFieldName))

    IF (localDeCount > 0) THEN

        CALL ESMF_FieldGet(dstFields(i) , fArrayPtr = FarrayPtr1D , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldGet "//TRIM(dstFieldName)//" pointer")

        ALLOCATE(temp(SIZE(FarrayPtr1D)))

        temp = FarrayPtr1D * (1.0_8 - SSTFracICON)

    END IF

END IF

        CALL ESMF_StateGet(InternalState , "XGrid2"//TRIM(dstFieldName)//"_RouteHandle" , xgrid2dst_RouteHandle(i) , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet XGrid2"//TRIM(dstFieldName)//" route handle")

        CALL ESMF_FieldRegrid(ICONGETM_XGrid_Fields(i) , dstFields(i) , routeHandle = xgrid2dst_RouteHandle(i) , &
!            & zeroregion = ESMF_REGION_EMPTY , &
!            & zeroregion = ESMF_REGION_SELECT , &
            & rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldRegrid XGrid2"//TRIM(dstFieldName))

IF (TRIM(COUPLER_Name) == "GETM-TO-ICON") THEN 

    IF (localDeCount > 0) THEN

        CALL ESMF_FieldGet(dstFields(i) , fArrayPtr = FarrayPtr1D , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldGet "//TRIM(dstFieldName)//" pointer")

        FarrayPtr1D = temp + FarrayPtr1D

        DEALLOCATE(temp)

    END IF

END IF

    END DO
 
    DEALLOCATE(srcFields , dstFields , ICONGETM_XGrid_Fields , src2xgrid_RouteHandle , xgrid2dst_RouteHandle)

END SUBROUTINE ExecuteRouteHandle


SUBROUTINE ReleaseRouteHandle(COUPLER_Comp , localrc)

    TYPE(ESMF_CplComp)                  :: COUPLER_Comp

    INTEGER, INTENT(OUT)                :: localrc

    TYPE(ESMF_State)                    :: InternalState
    TYPE(ESMF_FieldBundle)              :: srcFieldBundle , dstFieldBundle , ICONGETM_XGrid_FieldBundle
    TYPE(ESMF_Field), ALLOCATABLE       :: srcFields(:) , dstFields(:) , ICONGETM_XGrid_Fields(:)
    TYPE(ESMF_XGrid)                    :: ICONGETM_XGrid
    TYPE(ESMF_RouteHandle), ALLOCATABLE :: src2xgrid_RouteHandle(:) , xgrid2dst_RouteHandle(:)

    INTEGER                             :: i , nFields

    CHARACTER(LEN=128)                  :: srcFieldName , dstFieldName , COUPLER_Name

    CHARACTER(LEN=39), PARAMETER :: routine = "mo_esmf_coupler_comp:ReleaseRouteHandle"


    CALL NUOPC_ConnectorGet(COUPLER_Comp , state = InternalState , srcFields = srcFieldBundle , &
        & dstFields = dstFieldBundle , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_ConnectorGet internal state")

    CALL ESMF_CplCompGet(COUPLER_Comp , name = COUPLER_Name , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_CplCompGet name")

    CALL ESMF_StateGet(InternalState , TRIM(COUPLER_Name)//"_XGrid_FieldBundle" , ICONGETM_XGrid_FieldBundle , rc = localrc) 
    CALL checkESMF(localrc , routine , "ESMF_StateGet "//TRIM(COUPLER_Name)//" xgrid field bundle")
   
    CALL ESMF_FieldBundleGet(ICONGETM_XGrid_FieldBundle , fieldCount = nFields , itemorderflag = ESMF_ITEMORDER_ADDORDER , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_FieldBundleGet number of ICONGETM xgrid fields")

    ALLOCATE(srcFields(nFields))
    ALLOCATE(dstFields(nFields))    
    ALLOCATE(ICONGETM_XGrid_Fields(nFields))
    ALLOCATE(src2xgrid_RouteHandle(nFields))
    ALLOCATE(xgrid2dst_RouteHandle(nFields))

    CALL ESMF_FieldBundleGet(srcFieldBundle , fieldList = srcFields , itemorderflag = ESMF_ITEMORDER_ADDORDER , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_FieldBundleGet source fields")

    CALL ESMF_FieldBundleGet(dstFieldBundle , fieldList = dstFields , itemorderflag = ESMF_ITEMORDER_ADDORDER , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_FieldBundleGet destination fields")

    CALL ESMF_FieldBundleGet(ICONGETM_XGrid_FieldBundle , &!xgrid = ICONGETM_XGrid , &
        & fieldList = ICONGETM_XGrid_Fields , itemorderflag = ESMF_ITEMORDER_ADDORDER , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_FieldBundleGet ICONGETM xgrid fields")

    DO i = 1 , nFields

        CALL ESMF_FieldGet(ICONGETM_XGrid_Fields(i) , xgrid = ICONGETM_XGrid , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldGet xgrid "//TRIM(srcFieldName)//"_2_"//TRIM(dstFieldName))

        CALL ESMF_XGridDestroy(ICONGETM_XGrid , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_XGridDestroy ICONGETM xgrid "//TRIM(srcFieldName)//"_2_"//TRIM(dstFieldName))

        CALL ESMF_FieldDestroy(ICONGETM_XGrid_Fields(i) , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldDestroy "//TRIM(srcFieldName)//"_2_"//TRIM(dstFieldName))


        CALL ESMF_FieldGet(srcFields(i) , name = srcFieldName , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldGet "//TRIM(srcFieldName))

        CALL ESMF_StateGet(InternalState , TRIM(srcFieldName)//"2XGrid_RouteHandle" , src2xgrid_RouteHandle(i) , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet "//TRIM(srcFieldName)//"2XGrid route handle")

        CALL ESMF_FieldRegridRelease(src2xgrid_RouteHandle(i) , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldRegridRelease "//TRIM(srcFieldName)//"2XGrid route handle")
        

        CALL ESMF_FieldGet(dstFields(i) , name = dstFieldName , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldGet "//TRIM(dstFieldName))

        CALL ESMF_StateGet(InternalState , "XGrid2"//TRIM(dstFieldName)//"_RouteHandle" , xgrid2dst_RouteHandle(i) , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet XGrid2"//TRIM(dstFieldName)//" route handle")

        CALL ESMF_FieldRegridRelease(xgrid2dst_RouteHandle(i) , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldRegridRelease XGrid2"//TRIM(dstFieldName)//" route handle")

    END DO

    CALL ESMF_FieldBundleDestroy(ICONGETM_XGrid_FieldBundle , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_FieldBundleDestroy ICONGETM xgrid field bundle")

!    CALL ESMF_XGridDestroy(ICONGETM_XGrid , rc = localrc)
!    CALL checkESMF(localrc , routine , "ESMF_XGridDestroy ICONGETM xgrid")

    DEALLOCATE(ICONGETM_XGrid_Fields , src2xgrid_RouteHandle , xgrid2dst_RouteHandle)

END SUBROUTINE ReleaseRouteHandle


END MODULE mo_esmf_coupler_comp
