MODULE mo_esmf_cpl_comp

    USE ESMF
    USE NUOPC
    USE NUOPC_Mediator,           Cpl_Comp_SetServices          => SetServices , &
                                & Cpl_Comp_Label_DataInitialize => label_DataInitialize , &
                                & Cpl_Comp_Label_Advance        => label_Advance , &
                                & Cpl_Comp_Label_Finalize       => label_Finalize


    USE mo_esmf_utilities,      ONLY: checkESMF , FINDLOC

    USE mo_esmf_var,            ONLY: config_ESMF , nVar_2ICON , nVar_2GETM

    USE mo_physical_constants,  ONLY: rhoWater => rhoh2o , Rd => rd , Rd_Rv => rdv

    IMPLICIT NONE

    PRIVATE :: nVar_ICON_Import , nVar_ICON_Export , nVar_GETM_Import , nVar_GETM_Export

    PRIVATE :: defineConnections2ICON , defineConnections2GETM
    PRIVATE :: setConnections2ICON , setConnections2GETM
    PRIVATE :: Realize , Realize_withMesh , Realize_withGrid
    PRIVATE :: StoreRouteHandle
    PRIVATE :: FinalizeNestedStates , FinalizeFieldsInState

    TYPE Cpl_InternalStateStruct
        SEQUENCE
        TYPE(ESMF_Field), ALLOCATABLE, DIMENSION(:) :: ICON2GETM_Field
        TYPE(ESMF_Field), ALLOCATABLE, DIMENSION(:) :: GETM2ICON_Field
        INTEGER, ALLOCATABLE, DIMENSION(:,:)        :: VarConnection
        INTEGER                                     :: nVar_ICON2 = 0 , nVar_GETM2 = 0
        REAL(8), ALLOCATABLE, DIMENSION(:)          :: SSTFracICON
    END TYPE Cpl_InternalStateStruct

    TYPE Cpl_InternalState
        SEQUENCE
        TYPE(Cpl_InternalStateStruct) , POINTER :: wrapper
    END TYPE

    ! must be changed if possible more variables are advertized
    INTEGER, PARAMETER :: nVar_ICON_Import = 1
    INTEGER, PARAMETER :: nVar_ICON_Export = 25 ! 24 + nVar_ICON_Import
    INTEGER, PARAMETER :: nVar_GETM_Import = 14
    INTEGER, PARAMETER :: nVar_GETM_Export = 1

    INTERFACE Realize
        MODULE PROCEDURE Realize_withMesh
        MODULE PROCEDURE Realize_withGrid
    END INTERFACE Realize    

CONTAINS


SUBROUTINE SetServices(Cpl_Comp , localrc)

    TYPE(ESMF_GridComp)     :: Cpl_Comp

    INTEGER, INTENT(OUT)    :: localrc

    CHARACTER(LEN=28), PARAMETER    :: routine = "mo_esmf_cpl_comp:SetServices"


    CALL ESMF_LogWrite("Coupler: set services" , ESMF_LOGMSG_INFO , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_LogWrite Coupler set services")

    CALL NUOPC_CompDerive(Cpl_Comp , Cpl_Comp_SetServices , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompDerive")


    CALL ESMF_GridCompSetEntryPoint(Cpl_Comp , ESMF_METHOD_INITIALIZE , userRoutine = Cpl_Comp_Init_Phase_0 , phase = 0 , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_GridCompSetEntryPoint - ESMF_METHOD_INITIALIZE - Phase 0")

    CALL NUOPC_CompSetEntryPoint(Cpl_Comp , ESMF_METHOD_INITIALIZE , phaseLabelList = (/ "IPDv03p1" /) , &
        & userRoutine = Cpl_Comp_Init_Phase_1 , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompSetEntryPoint - ESMF_METHOD_INITIALIZE - Phase 1")

    CALL NUOPC_CompSetEntryPoint(Cpl_Comp , ESMF_METHOD_INITIALIZE , phaseLabelList = (/ "IPDv03p5" /) , &
        & userRoutine = Cpl_Comp_Init_Phase_5 , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompSetEntryPoint - ESMF_METHOD_INITIALIZE - Phase 5")

    CALL NUOPC_CompSetEntryPoint(Cpl_Comp , ESMF_METHOD_INITIALIZE , phaseLabelList = (/ "IPDv03p6" /) , &
        & userRoutine = Cpl_Comp_Init_Phase_6 , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompSetEntryPoint - ESMF_METHOD_INITIALIZE - Phase 6")


    CALL NUOPC_CompSpecialize(Cpl_Comp , specLabel = Cpl_Comp_Label_DataInitialize , specRoutine = Cpl_Comp_DataInit , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompSpecialize Cpl initialize data")

    CALL NUOPC_CompSpecialize(Cpl_Comp , specLabel = Cpl_Comp_Label_Advance , specRoutine = Cpl_Comp_Run , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompSpecialize Cpl run")

    CALL NUOPC_CompSpecialize(Cpl_Comp , specLabel = Cpl_Comp_Label_Finalize , specRoutine = Cpl_Comp_Finalize , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompSpecialize Cpl finalize")

END SUBROUTINE SetServices


SUBROUTINE Cpl_Comp_Init_Phase_0(Cpl_Comp , ImportState , ExportState , ICONGETM_Clock , localrc)

    TYPE(ESMF_GridComp)             :: Cpl_Comp
    TYPE(ESMF_State)                :: ImportState , ExportState
    TYPE(ESMF_Clock)                :: ICONGETM_Clock

    INTEGER, INTENT(OUT)            :: localrc

    CHARACTER(LEN=38), PARAMETER    :: routine = "mo_esmf_cpl_comp:Cpl_Comp_Init_Phase_0"


    ! Switch to IPDv03 by filtering all other phaseMap entries
    CALL NUOPC_CompFilterPhaseMap(Cpl_Comp , ESMF_METHOD_INITIALIZE , acceptStringList = (/ "IPDv03p" /) , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompFilterPhaseMap IPDv03p")

END SUBROUTINE Cpl_Comp_Init_Phase_0


SUBROUTINE Cpl_Comp_Init_Phase_1(Cpl_Comp , ImportState , ExportState , Cpl_Clock , localrc)

    TYPE(ESMF_GridComp)                     :: Cpl_Comp
    TYPE(ESMF_State)                        :: ImportState , ExportState
    TYPE(ESMF_Clock)                        :: Cpl_Clock

    INTEGER, INTENT(OUT)                    :: localrc

    TYPE(ESMF_State)                        :: fromICON , toGETM , fromGETM , toICON
   
    INTEGER                                 :: i , j
    INTEGER                                 :: nVar_ICON2 , nVar_GETM2

    CHARACTER(LEN=128)                      :: Var_ICON2(28) , Var_GETM2(1)

    TYPE(Cpl_InternalState)                 :: InternalStateWrapper
    TYPE(Cpl_InternalStateStruct) , POINTER :: InternalStateData

    CHARACTER(LEN=38), PARAMETER    :: routine = "mo_esmf_cpl_comp:Cpl_Comp_Init_Phase_1"
    
    
    CALL ESMF_LogWrite("Coupler: initialize: phase 1" , ESMF_LOGMSG_INFO , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_LogWrite Coupler initialize phase 1")

    ALLOCATE(InternalStateData)

    ALLOCATE(InternalStateData%VarConnection(nVar_ICON_Import+nVar_ICON_Export+nVar_GETM_Import+nVar_GETM_Export , &
                                           & nVar_ICON_Import+nVar_ICON_Export+nVar_GETM_Import+nVar_GETM_Export))

    ! 2GETM

    IF (nVar_2GETM > 0) THEN
    
        CALL defineConnections2GETM(Var_ICON2 , nVar_ICON2)

        InternalStateData%nVar_ICON2 = nVar_ICON2
    
        ! advertise import fields from ICON
    
        CALL NUOPC_AddNamespace(ImportState , namespace = "ICON" , nestedState = fromICON , rc = localrc)
        CALL checkESMF(localrc , routine , "NUOPC_AddNamespace ImportState ICON")

        DO i = 1 , nVar_ICON2

            CALL NUOPC_Advertise(fromICON , StandardName = TRIM(Var_ICON2(i)) , &
                & TransferOfferGeomObject = "cannot provide" , rc = localrc)
            CALL checkESMF(localrc , routine , "NUOPC_Advertise "//TRIM(Var_ICON2(i)))

        END DO

        ! advertise export fields to GETM

        CALL NUOPC_AddNamespace(ExportState , namespace = "GETM" , nestedState =   toGETM , rc = localrc)
        CALL checkESMF(localrc , routine , "NUOPC_AddNamespace ExportState GETM")

        DO j = 1 , nVar_2GETM
    
            CALL NUOPC_Advertise(  toGETM , StandardName = TRIM(config_ESMF%vars_GETM_Import(j)) , &
                & TransferOfferGeomObject = "cannot provide" , rc = localrc)
            CALL checkESMF(localrc , routine , "NUOPC_Advertise "//TRIM(config_ESMF%vars_GETM_Import(j)))

        END DO

    END IF

    ! 2ICON

    IF (nVar_2ICON > 0) THEN

        CALL defineConnections2ICON(Var_GETM2 , nVar_GETM2)

        InternalStateData%nVar_GETM2 = nVar_GETM2

        ! advertise import fields from GETM

        CALL NUOPC_AddNamespace(ImportState , namespace = "GETM" , nestedState = fromGETM , rc = localrc)
        CALL checkESMF(localrc , routine , "NUOPC_AddNamespace ImportState GETM")

        DO j = 1 , nVar_GETM2

            CALL NUOPC_Advertise(fromGETM , StandardName = TRIM(Var_GETM2(j)) , &
                & TransferOfferGeomObject = "cannot provide" , rc = localrc)
            CALL checkESMF(localrc , routine , "NUOPC_Advertise "//TRIM(Var_GETM2(j)))

        END DO

        ! advertise export fields to ICON

        CALL NUOPC_AddNamespace(ExportState , namespace = "ICON" , nestedState =   toICON , rc = localrc)
        CALL checkESMF(localrc , routine , "NUOPC_AddNamespace ExportState ICON")

        DO i = 1 , nVar_2ICON

            CALL NUOPC_Advertise(  toICON , StandardName = TRIM(config_ESMF%vars_ICON_Import(i)) , &
                & TransferOfferGeomObject = "cannot provide" , rc = localrc)
            CALL checkESMF(localrc , routine , "NUOPC_Advertise "//TRIM(config_ESMF%vars_ICON_Import(i)))

        END DO

    END IF


    ! Special situation with sea surface temperature -> should be removed soon 

    i = FINDLOC(config_ESMF%vars_ICON_Import , "t_seasfc")

    IF (i > 0) THEN

        IF (nVar_2GETM <= 0) THEN

            CALL NUOPC_AddNamespace(ImportState , namespace = "ICON" , nestedState = fromICON , rc = localrc)
            CALL checkESMF(localrc , routine , "NUOPC_AddNamespace ImportState ICON")

        END IF

        CALL NUOPC_Advertise(fromICON , StandardName = "t_seasfc" , &
            & TransferOfferGeomObject = "cannot provide" , rc = localrc)
        CALL checkESMF(localrc , routine , "NUOPC_Advertise from ICON t_seasfc")

    END IF


    InternalStateWrapper%wrapper => InternalStateData
    
    CALL ESMF_GridCompSetInternalState(Cpl_Comp , InternalStateWrapper , localrc)
    CALL checkESMF(localrc , routine , "ESMF_GridCompSetInternalState")

END SUBROUTINE Cpl_Comp_Init_Phase_1


SUBROUTINE Cpl_Comp_Init_Phase_5(Cpl_Comp , ImportState , ExportState , Cpl_Clock , localrc)

    TYPE(ESMF_GridComp)                             :: Cpl_Comp
    TYPE(ESMF_State)                                :: ImportState , ExportState
    TYPE(ESMF_Clock)                                :: Cpl_Clock

    INTEGER, INTENT(OUT)                            :: localrc

    TYPE(ESMF_State)                                :: fromICON , toGETM , fromGETM , toICON
    TYPE(ESMF_Grid), ALLOCATABLE, DIMENSION(:)      :: GETM_Grid
    TYPE(ESMF_Mesh), ALLOCATABLE, DIMENSION(:)      :: ICON_Mesh
    TYPE(ESMF_XGrid)                                :: ICON2GETM_XGrid , GETM2ICON_XGrid
    TYPE(ESMF_Field), ALLOCATABLE, DIMENSION(:)     :: FieldFromState , FieldToState
    TYPE(ESMF_Field), ALLOCATABLE, DIMENSION(:)     :: ICON2GETM_XGrid_Field , GETM2ICON_XGrid_Field
    
    INTEGER                                         :: i , j , nVar , nVar_ICON2 , nVar_GETM2 , ix(1)
    INTEGER, ALLOCATABLE, DIMENSION(:,:)            :: VarConnection

    REAL(ESMF_KIND_R8), POINTER                     :: FArrayPtr_1D(:)

    CHARACTER(LEN=128), ALLOCATABLE, DIMENSION(:)   :: FieldNamesFromICON , FieldNamesFromGETM

INTEGER :: localDeCount

    TYPE(Cpl_InternalState)                         :: InternalStateWrapper, InternalStateWrapperNew
    TYPE(Cpl_InternalStateStruct) , POINTER         :: InternalStateData

    CHARACTER(LEN=38), PARAMETER                    :: routine = "mo_esmf_cpl_comp:Cpl_Comp_Init_Phase_5"


    CALL ESMF_LogWrite("Coupler: initialize: phase 5" , ESMF_LOGMSG_INFO , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_LogWrite Coupler initialize phase 5")

    CALL ESMF_GridCompGetInternalState(Cpl_Comp , InternalStateWrapper , localrc)
    CALL checkESMF(localrc , routine , "ESMF_GridCompGetInternalState")

    InternalStateData => InternalStateWrapper%wrapper 

    nVar_ICON2 = InternalStateData%nVar_ICON2
    nVar_GETM2 = InternalStateData%nVar_GETM2


    ALLOCATE(VarConnection(nVar_ICON_Import+nVar_ICON_Export+nVar_GETM_Import+nVar_GETM_Export , &
                         & nVar_ICON_Import+nVar_ICON_Export+nVar_GETM_Import+nVar_GETM_Export))

    VarConnection = 0


    i = FINDLOC(config_ESMF%vars_ICON_Import , "t_seasfc")
    IF (i > 0) i = 1

    IF ((nVar_ICON2 > 0) .OR. (i > 0)) THEN

        CALL checkConnectionStatus(ImportState , "ICON" , "import" , fromICON , nVar_ICON2)
        InternalStateData%nVar_ICON2 = nVar_ICON2

    END IF

    IF ( nVar_GETM2 > 0) CALL checkConnectionStatus(ImportState , "GETM" , "import" , fromGETM)

    IF ( nVar_2GETM > 0) CALL checkConnectionStatus(ExportState , "GETM" , "export" , toGETM)
    IF ( nVar_2ICON > 0) CALL checkConnectionStatus(ExportState , "ICON" , "export" , toICON)


    IF (nVar_ICON2 + i > 0) THEN

        ALLOCATE(FieldNamesFromICON(nVar_ICON2 + i))

        CALL ESMF_StateGet(fromICON , itemNameList = FieldNamesFromICON , &
            & itemorderflag = ESMF_ITEMORDER_ADDORDER , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet field names from ICON")

    END IF

    IF (nVar_GETM2 > 0) THEN

        ALLOCATE(FieldNamesFromGETM(nVar_GETM2))

        CALL ESMF_StateGet(fromGETM , itemNameList = FieldNamesFromGETM , &
            & itemorderflag = ESMF_ITEMORDER_ADDORDER , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet field names from GETM")

    END IF


    ! 2GETM

    IF (nVar_2GETM > 0) CALL setConnections2GETM(VarConnection , nVar_2GETM , FieldNamesFromICON , FieldNamesFromGETM)


    ! 2ICON

    IF (nVar_2ICON > 0) CALL setConnections2ICON(VarConnection , nVar_2ICON , FieldNamesFromGETM)


    ! ICON2GETM

    IF (nVar_2GETM > 0) THEN

        ALLOCATE(ICON2GETM_XGrid_Field(nVar_2GETM))
    
        ALLOCATE(FieldFromState(nVar_ICON2))
        ALLOCATE(FieldToState(nVar_2GETM))

        ALLOCATE(ICON_Mesh(nVar_ICON2))
        ALLOCATE(GETM_Grid(nVar_2GETM))

        DO i = 1 , nVar_ICON2

            CALL ESMF_StateGet(fromICON , TRIM(FieldNamesFromICON(i)) , field = FieldFromState(i) , rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_StateGet from ICON "//TRIM(FieldNamesFromICON(i)))

            CALL Realize(fromICON , FieldFromState(i) , TRIM(FieldNamesFromICON(i)) , ICON_Mesh(i))

        END DO

        DO i = 1 , nVar_2GETM

            CALL ESMF_StateGet(  toGETM , TRIM(config_ESMF%vars_GETM_Import(i)) , field = FieldToState(i) , rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_StateGet   to GETM "//TRIM(config_ESMF%vars_GETM_Import(i)))

            CALL Realize(  toGETM , FieldToState(i) , TRIM(config_ESMF%vars_GETM_Import(i)) , GETM_Grid(i))

        END DO

        nVar = 0

        DO j = 1 , nVar_2GETM

            i = FINDLOC(VarConnection(nVar_ICON_Import + 1 : nVar_ICON_Import + nVar_ICON_Export , &
                                    & nVar_ICON_Import + nVar_ICON_Export + j) , 1)

            IF (VarConnection(nVar_ICON_Import + i , nVar_ICON_Import + nVar_ICON_Export + j) > 0) THEN

                ! create xgrid
                ICON2GETM_XGrid = ESMF_XGridCreate(sideAMesh = (/ ICON_Mesh(i) /) , sideBGrid = (/ GETM_Grid(j) /) , &
!                    & sideAMaskValues = (/ 0 /) , sideBMaskValues = (/ 0 , 2 , 3 /) , &
!                    & sideAMaskValues = (/ 0 /) , &
                    & sideBMaskValues = (/ 0 /) , &
                    & name = "ICON2GETM_XGrid" , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_XGridCreate ICON2GETM")
        
                nVar = nVar + 1

                ! create field for xgrid
                ICON2GETM_XGrid_Field(nVar) = ESMF_FieldCreate(ICON2GETM_XGrid , ESMF_TYPEKIND_R8 , &
                    & name = TRIM(FieldNamesFromICON(i))//"_2_"//TRIM(config_ESMF%vars_GETM_Import(j)) , rc = localrc)
                CALL checkESMF(localrc , routine , &
                    & "ESMF_FieldCreate ICON2GETM xgrid field "//TRIM(FieldNamesFromICON(i))//"_2_"//TRIM(config_ESMF%vars_GETM_Import(j)))
   
                CALL ESMF_FieldGet(ICON2GETM_XGrid_Field(nVar) , fArrayPtr = FArrayPtr_1D , rc = localrc)
                CALL checkESMF(localrc , routine , &
                    & "ESMF_FieldGet Pointer "//TRIM(FieldNamesFromICON(i))//"_2_"//TRIM(config_ESMF%vars_GETM_Import(j)))

                FArrayPtr_1D = 0.0_8 

            END IF

        END DO

        IF (nVar > 0) THEN

            ALLOCATE(InternalStateData%ICON2GETM_Field(nVar))

            InternalStateData%ICON2GETM_Field = ICON2GETM_XGrid_Field(1:nVar)

        END IF

        DEALLOCATE(ICON2GETM_XGrid_Field , FieldFromState , FieldToState , ICON_Mesh , GETM_Grid)

    END IF


    ! GETM2ICON

    IF (nVar_2ICON > 0) THEN

        ALLOCATE(GETM2ICON_XGrid_Field(nVar_2ICON))

        ALLOCATE(FieldFromState(nVar_2ICON))
        ALLOCATE(FieldToState(nVar_GETM2))

        ALLOCATE(ICON_Mesh(nVar_2ICON))
        ALLOCATE(GETM_Grid(nVar_GETM2))  


        ! Special situation with sea surface temperature -> should be removed soon

        i = FINDLOC(config_ESMF%vars_ICON_Import , "t_seasfc")

        IF (i > 0) THEN
           
            CALL ESMF_StateGet(fromICON , "t_seasfc" , field = FieldFromState(1) , rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_StateGet from ICON t_seasfc")

            CALL Realize(fromICON , FieldFromState(1) , "t_seasfc" , ICON_Mesh(1))
 
        END IF


        DO i = 1 , nVar_GETM2

            CALL ESMF_StateGet(fromGETM , TRIM(FieldNamesFromGETM(i)) , field = FieldFromState(i) , rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_StateGet from GETM "//TRIM(FieldNamesFromGETM(i)))

            CALL Realize(fromGETM , FieldFromState(i) , TRIM(FieldNamesFromGETM(i)) , GETM_Grid(i))

        END DO

        DO i = 1 , nVar_2ICON
                       
            CALL ESMF_StateGet(  toICON , TRIM(config_ESMF%vars_ICON_Import(i)) , field = FieldToState(i) , rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_StateGet   to ICON "//TRIM(config_ESMF%vars_ICON_Import(i)))

            CALL Realize(  toICON , FieldToState(i) , TRIM(config_ESMF%vars_ICON_Import(i)) , ICON_Mesh(i))

        END DO


CALL ESMF_FieldGet(FieldToState(1) , localDeCount = localDeCount , rc = localrc)
CALL checkESMF(localrc , routine , "ESMF_FieldGet ICON mesh fraction "//TRIM(config_ESMF%vars_ICON_Import(1)))

IF (localDeCount > 0) THEN

    CALL ESMF_FieldGet(FieldToState(1) , fArrayPtr = FArrayPtr_1D , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_FieldGet pointer "//TRIM(config_ESMF%vars_ICON_Import(1)))

    ALLOCATE(InternalStateData%SSTFracICON(SIZE(FArrayPtr_1D)))

    InternalStateData%SSTFracICON = 0.0_8

END IF


        nVar = 0

        DO i = 1 , nVar_2ICON

            j = FINDLOC(VarConnection(i , nVar_ICON_Import + nVar_ICON_Export + nVar_GETM_Import + 1 : &
                                        & nVar_ICON_Import + nVar_ICON_Export + nVar_GETM_Import + nVar_GETM_Export) , 1)

            IF (VarConnection(i , nVar_ICON_Import + nVar_ICON_Export + nVar_GETM_Import + j) > 0) THEN

                ! create xgrid
                GETM2ICON_XGrid = ESMF_XGridCreate(sideAGrid = (/ GETM_Grid(j) /) , sideBMesh = (/ ICON_Mesh(i) /) , & 
                    & sideAMaskValues = (/ 0 , 2 , 3 /) , sideBMaskValues = (/ 0 /) , &
!                    & sideAMaskValues = (/ 0 /) , sideBMaskValues = (/ 0 /) , &
                    & name = "GETM2ICON_XGrid" , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_XGridCreate GETM2ICON")

                nVar = nVar + 1

                ! create field for xgrid
                GETM2ICON_XGrid_Field(nVar) = ESMF_FieldCreate(GETM2ICON_XGrid , ESMF_TYPEKIND_R8 , &
                    & name = TRIM(FieldNamesFromGETM(j))//"_2_"//TRIM(config_ESMF%vars_ICON_Import(i)) , rc = localrc)
                CALL checkESMF(localrc , routine , &
                    & "ESMF_FieldCreate GETM2ICON xgrid field "//TRIM(FieldNamesFromGETM(j))//"_2_"//TRIM(config_ESMF%vars_ICON_Import(i)))
            
                CALL ESMF_FieldGet(GETM2ICON_XGrid_Field(nVar) , fArrayPtr = FArrayPtr_1D , rc = localrc)
                CALL checkESMF(localrc , routine , &
                    & "ESMF_FieldGet Pointer "//TRIM(FieldNamesFromGETM(j))//"_2_"//TRIM(config_ESMF%vars_ICON_Import(i)))

                FArrayPtr_1D = 0.0_8
        
            END IF

        END DO

        IF (nVar > 0) THEN

            ALLOCATE(InternalStateData%GETM2ICON_Field(nVar))

            InternalStateData%GETM2ICON_Field = GETM2ICON_XGrid_Field(1:nVar)

        END IF
    
        DEALLOCATE(GETM2ICON_XGrid_Field , FieldFromState , FieldToState , ICON_Mesh , GETM_Grid)

    END IF


    InternalStateData%VarConnection = VarConnection
   
    InternalStateWrapperNew%wrapper => InternalStateData
    
    CALL ESMF_GridCompSetInternalState(Cpl_Comp , InternalStateWrapperNew , localrc)
    CALL checkESMF(localrc , routine , "ESMF_GridCompSetInternalState")


    IF (ALLOCATED(FieldNamesFromGETM)) THEN

        DEALLOCATE(FieldNamesFromGETM)

    END IF

    IF (ALLOCATED(FieldNamesFromICON)) THEN

        DEALLOCATE(FieldNamesFromICON)

    END IF

    DEALLOCATE(VarConnection)

END SUBROUTINE Cpl_Comp_Init_Phase_5


SUBROUTINE Realize_withMesh(State , FieldFromState , FieldName , ICON_Mesh)

    TYPE(ESMF_State), INTENT(INOUT) :: State
    TYPE(ESMF_Field)                :: FieldFromState

    CHARACTER(LEN=*), INTENT(IN)    :: FieldName

    TYPE(ESMF_Mesh), INTENT(OUT)    :: ICON_Mesh

    INTEGER                         :: localrc

    CHARACTER(LEN=ESMF_MAXSTR)      :: transferAction

    CHARACTER(LEN=33), PARAMETER    :: routine = "mo_esmf_cpl_comp:Realize_withMesh"


    ! realize fields to/from ICON

    CALL NUOPC_GetAttribute(FieldFromState , name = "TransferActionGeomObject" , value = transferAction , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_GetAttribute TransferActionGeomObject "//TRIM(FieldName))

!WRITE(0,*) routine , ": " , TRIM(FieldName) , ": TransferActionGeomObject: " , TRIM(transferAction)
        
!    IF (TRIM(transferAction) == "accept") THEN
    IF (TRIM(transferAction) == "provide") THEN

        ! query mesh from ICON
        CALL ESMF_FieldGet(FieldFromState , mesh = ICON_Mesh , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldGet ICON mesh "//TRIM(FieldName))

        CALL ESMF_FieldEmptyComplete(FieldFromState , typekind = ESMF_TYPEKIND_R8 , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldEmptyComplete "//TRIM(FieldName))

        CALL NUOPC_Realize(State , FieldFromState , rc = localrc)
        CALL checkESMF(localrc , routine , "NUOPC_Realize "//TRIM(FieldName))

    END IF
        
END SUBROUTINE Realize_withMesh


SUBROUTINE Realize_withGrid(State , FieldFromState , FieldName , GETM_Grid)

    TYPE(ESMF_State), INTENT(INOUT) :: State
    TYPE(ESMF_Field)                :: FieldFromState

    CHARACTER(LEN=*), INTENT(IN)    :: FieldName

    TYPE(ESMF_Grid), INTENT(OUT)    :: GETM_Grid

    INTEGER                         :: localrc

    CHARACTER(LEN=ESMF_MAXSTR)      :: transferAction

    INTEGER                         :: localDeCount
    REAL(ESMF_KIND_R8) , POINTER    :: FArrayPtr2D(:,:)

    CHARACTER(LEN=33), PARAMETER    :: routine = "mo_esmf_cpl_comp:Realize_withGrid"


    ! realize fields to/from GETM

    CALL NUOPC_GetAttribute(FieldFromState , name = "TransferActionGeomObject" , value = transferAction , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_GetAttribute TransferActionGeomObject "//TRIM(FieldName))

!WRITE(0,*) routine , ": " , TRIM(FieldName) , ": TransferActionGeomObject: " , TRIM(transferAction)
        
!    IF (TRIM(transferAction) == "accept") THEN
    IF (TRIM(transferAction) == "provide") THEN

        ! query grid from GETM
        CALL ESMF_FieldGet(FieldFromState , grid = GETM_Grid , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldGet GETM grid "//TRIM(FieldName))

        CALL ESMF_FieldEmptyComplete(FieldFromState , typekind = ESMF_TYPEKIND_R8 , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldEmptyComplete "//TRIM(FieldName))

        CALL NUOPC_Realize(State , FieldFromState , rc = localrc)
        CALL checkESMF(localrc , routine , "NUOPC_Realize "//TRIM(FieldName))

    END IF  

END SUBROUTINE Realize_withGrid


SUBROUTINE Cpl_Comp_Init_Phase_6(Cpl_Comp , ImportState , ExportState , ICONGETM_Clock , localrc)

    TYPE(ESMF_GridComp)                             :: Cpl_Comp
    TYPE(ESMF_State)                                :: ImportState , ExportState
    TYPE(ESMF_Clock)                                :: ICONGETM_Clock

    INTEGER, INTENT(OUT)                            :: localrc

    TYPE(ESMF_State)                                :: fromICON , toGETM , fromGETM , toICON

    INTEGER                                         :: nVar , nVar_ICON2 , nVar_GETM2

TYPE(ESMF_Field) :: FieldFromGETM , FieldToICON , dstFracField
TYPE(ESMF_Mesh) :: ICON_Mesh
TYPE(ESMF_XGrid) :: XGrid
TYPE(ESMF_RouteHandle) :: to_XGrid , XGrid_to
CHARACTER(LEN=128) :: FieldName
INTEGER :: localDeCount
REAL(8), POINTER, DIMENSION(:) :: FArrayPtr1D , FArrayPtr1DFrac
REAL(8), ALLOCATABLE, DIMENSION(:) :: temp

    TYPE(Cpl_InternalState)                         :: InternalStateWrapper, InternalStateWrapperNew
    TYPE(Cpl_InternalStateStruct) , POINTER         :: InternalStateData


    CHARACTER(LEN=38), PARAMETER                    :: routine = "mo_esmf_cpl_comp:Cpl_Comp_Init_Phase_6"
    

    CALL ESMF_LogWrite("Coupler: initialize: phase 6" , ESMF_LOGMSG_INFO , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_LogWrite Coupler initialize phase 6")

    ! set the internal clock to the parent clock
    ! NOTE (bauer@tropos.de): is required by IPDV03p6, since it is overwritten
    CALL NUOPC_CompSetClock(Cpl_Comp , ICONGETM_Clock , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompSetClock")


    CALL ESMF_GridCompGetInternalState(Cpl_Comp , InternalStateWrapper , localrc)
    CALL checkESMF(localrc , routine , "ESMF_GridCompGetInternalState")

    InternalStateData => InternalStateWrapper%wrapper 


    ! ICON2GETM

    IF (ALLOCATED(InternalStateData%ICON2GETM_Field)) THEN

        CALL ESMF_StateGet(ImportState , "ICON" , nestedState = fromICON , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet import nested state ICON")

        CALL ESMF_StateGet(ExportState , "GETM" , nestedState =   toGETM , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet export nested state GETM")
        
        CALL StoreRouteHandle(fromICON , "ICON" , toGETM , "GETM" , &
            & InternalStateData%ICON2GETM_Field , InternalStateData%nVar_ICON2 , nVar_2GETM , &
            & InternalStateData%VarConnection(nVar_ICON_Import + 1 : nVar_ICON_Import + nVar_ICON_Export , &
                                            & nVar_ICON_Import + nVar_ICON_Export + 1 : &
                                            & nVar_ICON_Import + nVar_ICON_Export + nVar_GETM_Import))

    END IF


    ! GETM2ICON

    IF (ALLOCATED(InternalStateData%GETM2ICON_Field)) THEN

        CALL ESMF_StateGet(ImportState , "GETM" , nestedState = fromGETM , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet import nested state GETM")

        CALL ESMF_StateGet(ExportState , "ICON" , nestedState =   toICON , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet export nested state ICON")
       
!        CALL StoreRouteHandle(fromGETM , "GETM" ,   toICON , "ICON" , &
!            & InternalStateStructPointer%GETM2ICON_Field , InternalStateData%nVar_GETM2 , nVar_2ICON , &
!            & TRANSPOSE(InternalStateStructPointer%VarConnection(1 : nVar_ICON_Import , &
!                           & nVar_ICON_Import + nVar_ICON_Export + nVar_GETM_Import + 1 : &
!                           & nVar_ICON_Import + nVar_ICON_Export + nVar_GETM_Import + nVar_GETM_Export)))

 
                CALL ESMF_StateGet(fromGETM , "sea_surface_temperature" , FieldFromGETM , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_StateGet from GETM field sea_surface_temperature")

                CALL ESMF_FieldGet(InternalStateData%GETM2ICON_Field(1) , &
                    & name = FieldName , xgrid = XGrid , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_FieldGet name and xgrid GETM2_XGrid_Field")

                CALL ESMF_FieldRegridStore(XGrid , FieldFromGETM , &
                    & InternalStateData%GETM2ICON_Field(1) , &
                    & routeHandle = to_XGrid , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_FieldRegridStore GETM2XGrid route handle "//TRIM(FieldName))

                CALL ESMF_RouteHandleSet(to_XGrid , name = "GETM2_"//TRIM(FieldName) , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_RouteHandleSet name GETM2_"//TRIM(FieldName)//"_RouteHandle")

                CALL ESMF_StateGet(  toICON , "t_seasfc" , FieldToICON , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_StateGet   to ICON field t_seasfc")

CALL ESMF_FieldGet(FieldToICON , localDeCount = localDeCount , mesh = ICON_Mesh , rc = localrc)
CALL checkESMF(localrc , routine , "ESMF_FieldGet localDeCount and mesh t_seasfc")

IF (localDeCount > 0) THEN

    CALL ESMF_FieldGet(FieldToICON , fArrayPtr = FArrayPtr1D , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_FieldGet pointer t_seasfc")

    ALLOCATE(temp(SIZE(FArrayPtr1D)))

    temp = FArrayPtr1D

END IF

dstFracField = ESMF_FieldCreate(ICON_Mesh , ESMF_TYPEKIND_R8 , meshloc = ESMF_MESHLOC_ELEMENT , &
    & name = "Frac_t_seasfc" , rc = localrc)
CALL checkESMF(localrc , routine , "ESMF_FieldCreate destination fraction t_seasfc")

                CALL ESMF_FieldRegridStore(XGrid , InternalStateData%GETM2ICON_Field(1) , &
                    & FieldToICON , &
& dstFracField = dstFracField , &
                    & routeHandle = XGrid_to , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_FieldRegridStore 2ICON route handle "//TRIM(FieldName))

    IF (localDeCount > 0) THEN

        CALL ESMF_FieldGet(dstFracField , fArrayPtr = FArrayPtr1DFrac , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldGet ICON mesh fraction t_seasfc")

        InternalStateData%SSTFracICON = FArrayPtr1DFrac

        FArrayPtr1D = temp

        DEALLOCATE(temp)

    END IF

CALL ESMF_FieldDestroy(dstFracField , rc = localrc)
CALL checkESMF(localrc , routine , "ESMF_FieldDestroy ICON mesh fraction t_seasfc")

                CALL ESMF_RouteHandleSet(XGrid_to , name = TRIM(FieldName)//"_2ICON" , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_RouteHandleSet name "//TRIM(FieldName)//"_2ICON_RouteHandle")

                CALL ESMF_StateAdd(fromGETM , routehandleList = (/ to_XGrid /) , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_StateAdd GETM2 route handles")

                CALL ESMF_StateAdd(  toICON , routehandleList = (/ XGrid_to /) , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_StateAdd 2ICON route handles")

    END IF
    
    InternalStateWrapperNew%wrapper => InternalStateData
    
    CALL ESMF_GridCompSetInternalState(Cpl_Comp , InternalStateWrapperNew , localrc)
    CALL checkESMF(localrc , routine , "ESMF_GridCompSetInternalState")

END SUBROUTINE Cpl_Comp_Init_Phase_6


SUBROUTINE StoreRouteHandle(fromState , from , toState , to , XGridField , nVar_From , nVar_To , VarConnection)

    TYPE(ESMF_State), INTENT(INOUT)                     :: fromState , toState
    
    CHARACTER(LEN=4), INTENT(IN)                        :: from , to

    TYPE(ESMF_Field), INTENT(INOUT), DIMENSION(:)       :: XGridField

    INTEGER, INTENT(IN)                                 :: nVar_From , nVar_To

    INTEGER, INTENT(IN), DIMENSION(:,:)                 :: VarConnection

    INTEGER                                             :: localrc
    INTEGER                                             :: i , j , nVar , ix(1) , itemCount

    CHARACTER(LEN=128), ALLOCATABLE, DIMENSION(:)       :: FieldNamesFrom , FieldNamesTo
    CHARACTER(LEN=128)                                  :: FieldName

    TYPE(ESMF_Field)                                    :: FieldFromState , FieldToState
    TYPE(ESMF_XGrid)                                    :: XGrid
    TYPE(ESMF_RouteHandle), ALLOCATABLE, DIMENSION(:)   :: to_XGrid , XGrid_to

    CHARACTER(LEN=33), PARAMETER                        :: routine = "mo_esmf_cpl_comp:StoreRouteHandle"


    CALL ESMF_StateGet(fromState , itemCount = itemCount , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_StateGet item count from "//from)

    ALLOCATE(FieldNamesFrom(itemCount))

    ALLOCATE(FieldNamesTo(nVar_To))
    ALLOCATE(to_XGrid(nVar_To)) 
    ALLOCATE(XGrid_to(nVar_To)) 

    CALL ESMF_StateGet(fromState , itemNameList = FieldNamesFrom , &
        & itemorderflag = ESMF_ITEMORDER_ADDORDER , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_StateGet field names from "//from)

    CALL ESMF_StateGet(  toState , itemNameList = FieldNamesTo   , &
        & itemorderflag = ESMF_ITEMORDER_ADDORDER , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_StateGet field names   to "//to)

    nVar = 0

    DO j = 1 , nVar_To

        i = FINDLOC(VarConnection(: , j) , 1)

        IF (VarConnection(i , j) > 0) THEN

            nVar = nVar + 1

            CALL ESMF_StateGet(fromState , TRIM(FieldNamesFrom(i)) , FieldFromState , rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_StateGet from "//from//" field "//Trim(FieldNamesFrom(i)))

            CALL ESMF_FieldGet(XGridField(nVar) , name = FieldName , xgrid = XGrid , rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_FieldGet name and xgrid "//from//"2_XGrid_Field")

            CALL ESMF_FieldRegridStore(XGrid , FieldFromState , XGridField(nVar) , &
                & routeHandle = to_XGrid(nVar) , rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_FieldRegridStore "//from//"2XGrid route handle "//TRIM(FieldName))

            CALL ESMF_RouteHandleSet(to_XGrid(nVar) , name = from//"2_"//TRIM(FieldName) , rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_RouteHandleSet name "//from//"2_"//TRIM(FieldName)//"_RouteHandle")
        
            CALL ESMF_StateGet(toState , TRIM(FieldNamesTo(j)) , FieldToState , rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_StateGet   to "//to//" field "//Trim(FieldNamesTo(j)))

            CALL ESMF_FieldRegridStore(XGrid , XGridField(nVar) , FieldToState , &
                & routeHandle = XGrid_to(nVar) , rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_FieldRegridStore 2"//to//" route handle "//TRIM(FieldName))

            CALL ESMF_RouteHandleSet(XGrid_to(nVar) , name = TRIM(FieldName)//"_2"//to , rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_RouteHandleSet name "//TRIM(FieldName)//"_2"//to//"_RouteHandle")

        END IF

    END DO

    CALL ESMF_StateAdd(fromState , routehandleList = to_XGrid(1:nVar) , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_StateAdd "//from//"2 route handles")
        
    CALL ESMF_StateAdd(  toState , routehandleList = XGrid_to(1:nVar) , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_StateAdd 2"//to//" route handles")

    DEALLOCATE(FieldNamesFrom , FieldNamesTo , to_XGrid , XGrid_to)

END SUBROUTINE StoreRouteHandle


SUBROUTINE Cpl_Comp_DataInit(Cpl_Comp , localrc)
    
    TYPE(ESMF_GridComp)             :: Cpl_Comp

    INTEGER, INTENT(OUT)            :: localrc

    CHARACTER(LEN=34), PARAMETER    :: routine = "mo_esmf_cpl_comp:Cpl_Comp_DataInit"


    ! indicate that data initialization is complete (breaking out of init-loop)
    CALL NUOPC_CompAttributeSet(Cpl_Comp , name = "InitializeDataComplete" , value = "true" , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompAttributeSet")
    
END SUBROUTINE Cpl_Comp_DataInit


SUBROUTINE Cpl_Comp_Run(Cpl_Comp , localrc)

    TYPE(ESMF_GridComp)                             :: Cpl_Comp

    INTEGER, INTENT(OUT)                            :: localrc
    
    TYPE(ESMF_State)                                :: ImportState , ExportState    
    TYPE(ESMF_State)                                :: fromICON , toGETM , fromGETM , toICON
    TYPE(ESMF_Clock)                                :: Cpl_Clock
    TYPE(ESMF_Time)                                 :: runtime

    INTEGER                                         :: nVar , nVar1 , nVar_ICON2 , nVar_GETM2

    INTEGER, ALLOCATABLE, DIMENSION(:,:)            :: VarConnection
    CHARACTER(LEN=128)                              :: FieldName , FieldName1
    CHARACTER(LEN=128), ALLOCATABLE, DIMENSION(:)   :: FieldNamesFromICON , FieldNamesFromGETM , FieldNamesTo

    TYPE(ESMF_Field)                                :: FieldFromState , FieldFromState2 , FieldToState
    TYPE(ESMF_RouteHandle)                          :: XGrid_to , to_XGrid

    REAL(8), POINTER, DIMENSION(:)                  :: SurfDownHeatFlux , GridScaleRainRate , GridScaleSnowRate , &
                                                     & GridScaleGraupelRate , GridScaleIceRate , GridScaleHailRate , &
                                                     & ConvRainRate , ConvSnowRate , MoistFluxSurf , LatentHeatFlux , &
                                                     & LongWaveFlux , SpecificHumiditySurf , PresMeanSeaLevel , SurfTemp

    REAL(8), POINTER, DIMENSION(:)                  :: FarrayPtr1D_ADD , FarrayPtr1D_From , FarrayPtr1D_To
REAL(8), POINTER, DIMENSION(:,:) :: FArrayPtr2D
    REAL(8), ALLOCATABLE, DIMENSION(:)              :: temp , temp2

    INTEGER                                         :: localDeCount
    INTEGER                                         :: i , i1 , i2 , j , ix(1)

    TYPE(Cpl_InternalState)                         :: InternalStateWrapper, InternalStateWrapperNew
    TYPE(Cpl_InternalStateStruct) , POINTER         :: InternalStateData

    CHARACTER(LEN=29), PARAMETER                    :: routine = "mo_esmf_cpl_comp:Cpl_Comp_Run"


    CALL ESMF_LogWrite("Coupler: interpolate" , ESMF_LOGMSG_INFO , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_LogWrite Coupler interpolate")

    CALL ESMF_GridCompGet(Cpl_Comp , clock = Cpl_Clock , importState = ImportState , &
        & exportState = ExportState , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_GridCompGet Cpl clock and states")
        
    CALL ESMF_ClockPrint(Cpl_Clock , options = "currTime" , preString = "------>Advancing Cpl from: " , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_ClockPrint Cpl currTime")

    
    CALL ESMF_GridCompGetInternalState(Cpl_Comp , InternalStateWrapper , localrc)
    CALL checkESMF(localrc , routine , "ESMF_GridCompGetInternalState")

    InternalStateData => InternalStateWrapper%wrapper 

    nVar_ICON2 = InternalStateData%nVar_ICON2
    nVar_GETM2 = InternalStateData%nVar_GETM2

    ALLOCATE(VarConnection(nVar_ICON_Import+nVar_ICON_Export+nVar_GETM_Import+nVar_GETM_Export , &
                         & nVar_ICON_Import+nVar_ICON_Export+nVar_GETM_Import+nVar_GETM_Export))

    VarConnection = InternalStateData%VarConnection

    
    ! special situation with sea surface temperature -> should be removed soon

    i = FINDLOC(config_ESMF%vars_ICON_Import , "t_seasfc")

    IF (i > 0) i = 1

    IF (nVar_ICON2 + i > 0) THEN

        CALL ESMF_StateGet(ImportState , "ICON" , nestedState = fromICON , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet import nested state ICON")

        ! Fields and RouteHandles
        ALLOCATE(FieldNamesFromICON(nVar_ICON2 + nVar_2GETM + i))

        CALL ESMF_StateGet(fromICON , itemNameList = FieldNamesFromICON , &
            & itemorderflag = ESMF_ITEMORDER_ADDORDER , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet field names from ICON")

    END IF


    IF (nVar_GETM2 > 0) THEN

        CALL ESMF_StateGet(ImportState , "GETM" , nestedState = fromGETM , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet import nested state GETM")

        ! Fields and RouteHandles
        ALLOCATE(FieldNamesFromGETM(nVar_GETM2 + nVar_2ICON))

        CALL ESMF_StateGet(fromGETM , itemNameList = FieldNamesFromGETM , &
            & itemorderflag = ESMF_ITEMORDER_ADDORDER , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet field names from GETM")

    END IF


    ! GETM2ICON

    IF (ALLOCATED(InternalStateData%GETM2ICON_Field)) THEN

        CALL ESMF_StateGet(ExportState , "ICON" , nestedState =   toICON , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet export nested state ICON")

        nVar = 0

        DO i = 1 , nVar_2ICON

            j = FINDLOC(VarConnection(i , nVar_ICON_Import + nVar_ICON_Export + nVar_GETM_Import + 1 : &
                                        & nVar_ICON_Import + nVar_ICON_Export + nVar_GETM_Import + nVar_GETM_Export) , 1)

            IF (VarConnection(i , nVar_ICON_Import + nVar_ICON_Export + nVar_GETM_Import + j) > 0) THEN

                nVar = nVar + 1

                CALL ESMF_StateGet(fromGETM , TRIM(FieldNamesFromGETM(j)) , FieldFromState , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_StateGet field from GETM "//TRIM(FieldNamesFromGETM(j)))

                CALL ESMF_FieldGet(InternalStateData%GETM2ICON_Field(nVar) , name = FieldName , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_FieldGet name "//TRIM(FieldName))

                CALL ESMF_StateGet(fromGETM , "GETM2_"//TRIM(FieldName) , routehandle = to_XGrid , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_StateGet route handle GETM2_"//TRIM(FieldName))

                CALL ESMF_StateGet(  toICON , TRIM(config_ESMF%vars_ICON_import(i)) , FieldToState , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_StateGet field   to ICON "//TRIM(config_ESMF%vars_ICON_import(i)))

                CALL ESMF_StateGet(  toICON , TRIM(FieldName)//"_2ICON" , routehandle = XGrid_to , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_StateGet route handle "//TRIM(FieldName)//"_2ICON")


CALL ESMF_FieldGet(FieldToState , localDeCount = localDeCount , rc = localrc)
CALL checkESMF(localrc , routine , "ESMF_FieldGet "//TRIM(config_ESMF%vars_ICON_import(i)))

IF (localDeCount > 0) THEN

    CALL ESMF_StateGet(fromICON , TRIM(config_ESMF%vars_ICON_import(i)) , FieldFromState2 , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_StateGet field from ICON "//TRIM(config_ESMF%vars_ICON_import(i)))

    CALL ESMF_FieldGet(FieldFromState2 , fArrayPtr = FarrayPtr1D_From , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_FieldGet from ICON "//TRIM(config_ESMF%vars_ICON_import(i))//" pointer")

    CALL ESMF_FieldGet(FieldToState , fArrayPtr = FarrayPtr1D_To , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_FieldGet   to ICON "//TRIM(config_ESMF%vars_ICON_import(i))//" pointer")

    FarrayPtr1D_To = FarrayPtr1D_From

    ALLOCATE(temp(SIZE(FarrayPtr1D_To)))

    temp = FarrayPtr1D_To * (1.0_8 - InternalStateData%SSTFracICON)

END IF

                CALL ESMF_FieldRegrid(FieldFromState , InternalStateData%GETM2ICON_Field(nVar) , &
                    & routeHandle = to_XGrid , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_FieldRegrid GETM to xgrid "//TRIM(FieldName))

                CALL ESMF_FieldRegrid(InternalStateData%GETM2ICON_Field(nVar) , FieldToState , &
                    & routeHandle = XGrid_to , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_FieldRegrid "//TRIM(FieldName)//" to "//TRIM(config_ESMF%vars_ICON_import(i)))


IF (localDeCount > 0) THEN

!    CALL ESMF_FieldGet(FieldToState , fArrayPtr = FarrayPtr1D_To , rc = localrc)
!    CALL checkESMF(localrc , routine , "ESMF_FieldGet   to ICON "//TRIM(config_ESMF%vars_ICON_import(i))//" pointer")

    FarrayPtr1D_To = temp + FarrayPtr1D_To
    FarrayPtr1D_From = FarrayPtr1D_To

    DEALLOCATE(temp)

!WRITE(0,*) routine , ": t_seasfc: " , MINVAL(FarrayPtr1D_To) , MAXVAL(FarrayPtr1D_To)

END IF
 
            END IF

        END DO

    END IF


    ! ICON2GETM

    IF (ALLOCATED(InternalStateData%ICON2GETM_Field)) THEN

        CALL ESMF_StateGet(ExportState , "GETM" , nestedState =   toGETM , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet export nested state GETM")

        nVar = 0

        DO j = 1 , nVar_2GETM

            ! pres_msl , u_10m , v_10m , t_2m , rh_2m , qv_s , td_2m , 
            ! clct , umfl_s , vmfl_s , sodifd_s , shfl_s/lhfl_s , qhfl_s/pres_msl/qv_s/t_s
            
            i = FINDLOC(VarConnection(nVar_ICON_Import + 1 : nVar_ICON_Import + nVar_ICON_Export , &
                                    & nVar_ICON_Import + nVar_ICON_Export + j) , 1)
            i1 = -1

            IF (VarConnection(nVar_ICON_Import + i , nVar_ICON_Import + nVar_ICON_Export + j) > 0) THEN

                nVar = nVar + 1

                CALL ESMF_StateGet(fromICON , TRIM(FieldNamesFromICON(i)) , FieldFromState , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_StateGet field from ICON "//TRIM(FieldNamesFromICON(i)))

                CALL ESMF_FieldGet(InternalStateData%ICON2GETM_Field(nVar) , name = FieldName , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_FieldGet name "//TRIM(FieldName))

                CALL ESMF_StateGet(fromICON , "ICON2_"//TRIM(FieldName) , routehandle = to_XGrid , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_StateGet route handle ICON2_"//TRIM(FieldName))

                CALL ESMF_StateGet(  toGETM , TRIM(config_ESMF%vars_GETM_Import(j)) , FieldToState , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_StateGet field   to GETM "//TRIM(config_ESMF%vars_GETM_Import(j)))

                CALL ESMF_StateGet(  toGETM , TRIM(FieldName)//"_2GETM" , routehandle = XGrid_to , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_StateGet route handle "//TRIM(FieldName)//"_2GETM")

                CALL ESMF_FieldGet(FieldFromState , localDeCount = localDeCount , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_FieldGet "//TRIM(FieldNamesFromICON(i)))

                IF (localDeCount > 0) THEN

                    SELECT CASE (TRIM(config_ESMF%vars_GETM_Import(j)))

                        CASE ("surface_downward_heat_flux") ! SurfDownHeatFlux

                            ! load sensible heat flux (shfl_s) as SurfDownHeatFlux
                            CALL ESMF_FieldGet(FieldFromState , farrayPtr = SurfDownHeatFlux , rc = localrc)
                            CALL checkESMF(localrc , routine , "ESMF_FieldGet pointer "//TRIM(FieldNamesFromICON(i)))

                            ! load latent heat flux (lhfl_s) as LatentHeatFlux
                            i1 = FINDLOC(VarConnection(nVar_ICON_Import + 1 : nVar_ICON_Import + nVar_ICON_Export , &
                                                     & nVar_ICON_Import + nVar_ICON_Export + j) , 2)
                
                            CALL ESMF_StateGet(fromICON , TRIM(FieldNamesFromICON(i1)) , FieldFromState2 , rc = localrc)
                            CALL checkESMF(localrc , routine , "ESMF_StateGet field from ICON "//TRIM(FieldNamesFromICON(i1)))

                            CALL ESMF_FieldGet(FieldFromState2 , farrayPtr = LatentHeatFlux , rc = localrc)
                            CALL checkESMF(localrc , routine , "ESMF_FieldGet pointer "//TRIM(FieldNamesFromICON(i1)))

                            ! load longwave net flux at surface (thb_s) as LongWaveFlux
                            i1 = FINDLOC(VarConnection(nVar_ICON_Import + 1 : nVar_ICON_Import + nVar_ICON_Export , &
                                                     & nVar_ICON_Import + nVar_ICON_Export + j) , 3)
                
                            CALL ESMF_StateGet(fromICON , TRIM(FieldNamesFromICON(i1)) , FieldFromState2 , rc = localrc)
                            CALL checkESMF(localrc , routine , "ESMF_StateGet field from ICON "//TRIM(FieldNamesFromICON(i1)))

                            CALL ESMF_FieldGet(FieldFromState2 , farrayPtr = LongWaveFlux , rc = localrc)
                            CALL checkESMF(localrc , routine , "ESMF_FieldGet pointer "//TRIM(FieldNamesFromICON(i1)))

                            SurfDownHeatFlux = SurfDownHeatFlux + LatentHeatFlux + LongWaveFlux

                        CASE ("precipitation") ! GridScaleRainRate

                            CALL ESMF_FieldGet(FieldFromState , farrayPtr = GridScaleRainRate , rc = localrc)
                            CALL checkESMF(localrc , routine , "ESMF_FieldGet pointer "//TRIM(FieldNamesFromICON(i)))

                            ! load gridscale snow rate (snow_gsp_rate) as GridScaleSnowRate
                            i1 = FINDLOC(VarConnection(nVar_ICON_Import + 1 : nVar_ICON_Import + nVar_ICON_Export , &
                                                     & nVar_ICON_Import + nVar_ICON_Export + j) , 2)

                            CALL ESMF_StateGet(fromICON , TRIM(FieldNamesFromICON(i1)) , FieldFromState2 , rc = localrc)
                            CALL checkESMF(localrc , routine , "ESMF_StateGet field from ICON "//TRIM(FieldNamesFromICON(i1)))

                            CALL ESMF_FieldGet(FieldFromState2 , farrayPtr = GridScaleSnowRate , rc = localrc)
                            CALL checkESMF(localrc , routine , "ESMF_FieldGet pointer "//TRIM(FieldNamesFromICON(i1)))

                            ! load gridscale graupel rate (graupel_gsp_rate) as GridScaleGraupelRate and add it to GridScaleRainRate
                            IF (FINDLOC(FieldNamesFromICON , "graupel_gsp_rate") > 0) THEN

                                i1 = FINDLOC(VarConnection(nVar_ICON_Import + 1 : nVar_ICON_Import + nVar_ICON_Export , &
                                                         & nVar_ICON_Import + nVar_ICON_Export + j) , 3)

                                CALL checkESMF(localrc , routine , "NUOPC_IsConnected from ICON "//TRIM(FieldNamesFromICON(i1)))

                                CALL ESMF_StateGet(fromICON , TRIM(FieldNamesFromICON(i1)) , FieldFromState2 , rc = localrc)
                                CALL checkESMF(localrc , routine , "ESMF_StateGet field from ICON "//TRIM(FieldNamesFromICON(i1)))

                                CALL ESMF_FieldGet(FieldFromState2 , farrayPtr = GridScaleGraupelRate , rc = localrc)
                                CALL checkESMF(localrc , routine , "ESMF_FieldGet pointer "//TRIM(FieldNamesFromICON(i1)))

                                GridScaleRainRate = GridScaleRainRate + GridScaleGraupelRate

                            END IF

                            ! load gridscale ice rate (ice_gsp_rate) as GridScaleIceRate and add it to GridScaleRainRate
                            IF (FINDLOC(FieldNamesFromICON , "ice_gsp_rate") > 0) THEN
                
                                i1 = FINDLOC(VarConnection(nVar_ICON_Import + 1 : nVar_ICON_Import + nVar_ICON_Export , &
                                                         & nVar_ICON_Import + nVar_ICON_Export + j) , 4)

                                CALL checkESMF(localrc , routine , "NUOPC_IsConnected from ICON "//TRIM(FieldNamesFromICON(i1)))

                                CALL ESMF_StateGet(fromICON , TRIM(FieldNamesFromICON(i1)) , FieldFromState2 , rc = localrc)
                                CALL checkESMF(localrc , routine , "ESMF_StateGet field from ICON "//TRIM(FieldNamesFromICON(i1)))

                                CALL ESMF_FieldGet(FieldFromState2 , farrayPtr = GridScaleIceRate , rc = localrc)
                                CALL checkESMF(localrc , routine , "ESMF_FieldGet pointer "//TRIM(FieldNamesFromICON(i1)))

                                GridScaleRainRate = GridScaleRainRate + GridScaleIceRate

                            END IF

                            ! load gridscale hail rate (hail_gsp_rate) as GridScaleHailRate and add it to GridScaleRainRate
                            IF (FINDLOC(FieldNamesFromICON , "hail_gsp_rate") > 0) THEN
                
                                i1 = FINDLOC(VarConnection(nVar_ICON_Import + 1 : nVar_ICON_Import + nVar_ICON_Export , &
                                                         & nVar_ICON_Import + nVar_ICON_Export + j) , 5)
                
                                CALL checkESMF(localrc , routine , "NUOPC_IsConnected from ICON "//TRIM(FieldNamesFromICON(i1)))

                                CALL ESMF_StateGet(fromICON , TRIM(FieldNamesFromICON(i1)) , FieldFromState2 , rc = localrc)
                                CALL checkESMF(localrc , routine , "ESMF_StateGet field from ICON "//TRIM(FieldNamesFromICON(i1)))

                                CALL ESMF_FieldGet(FieldFromState2 , farrayPtr = GridScaleHailRate , rc = localrc)
                                CALL checkESMF(localrc , routine , "ESMF_FieldGet pointer "//TRIM(FieldNamesFromICON(i1)))

                                GridScaleRainRate = GridScaleRainRate + GridScaleHailRate

                            END IF

                            ! load convective rain rate (rain_con_rate) as ConvRainRate
                            i1 = FINDLOC(VarConnection(nVar_ICON_Import + 1 : nVar_ICON_Import + nVar_ICON_Export , &
                                                     & nVar_ICON_Import + nVar_ICON_Export + j) , 6)
                
                            CALL ESMF_StateGet(fromICON , TRIM(FieldNamesFromICON(i1)) , FieldFromState2 , rc = localrc)
                            CALL checkESMF(localrc , routine , "ESMF_StateGet field from ICON "//TRIM(FieldNamesFromICON(i1)))

                            CALL ESMF_FieldGet(FieldFromState2 , farrayPtr = ConvRainRate , rc = localrc)
                            CALL checkESMF(localrc , routine , "ESMF_FieldGet pointer "//TRIM(FieldNamesFromICON(i1)))

                            ! load convective snow rate (snow_con_rate) as ConvSnowRate
                            i1 = FINDLOC(VarConnection(nVar_ICON_Import + 1 : nVar_ICON_Import + nVar_ICON_Export , &
                                                     & nVar_ICON_Import + nVar_ICON_Export + j) , 7)
                
                            CALL ESMF_StateGet(fromICON , TRIM(FieldNamesFromICON(i1)) , FieldFromState2 , rc = localrc)
                            CALL checkESMF(localrc , routine , "ESMF_StateGet field from ICON "//TRIM(FieldNamesFromICON(i1)))

                            CALL ESMF_FieldGet(FieldFromState2 , farrayPtr = ConvSnowRate , rc = localrc)
                            CALL checkESMF(localrc , routine , "ESMF_FieldGet pointer "//TRIM(FieldNamesFromICON(i1)))


                            GridScaleRainRate = GridScaleRainRate + GridScaleSnowRate + ConvRainRate + ConvSnowRate

                            ! precipitation: reference density correction of ocean
                            GridScaleRainRate = GridScaleRainRate / rhoWater

                        CASE ("evaporation") ! MoistFluxSurf

                            ! load moisture flux at surface (qhfl_s) as MoistFluxSurf
                            CALL ESMF_FieldGet(FieldFromState , farrayPtr = MoistFluxSurf , rc = localrc)
                            CALL checkESMF(localrc , routine , "ESMF_FieldGet pointer "//TRIM(FieldNamesFromICON(i)))

                            ! evaporation: reference density of ocean
                            MoistFluxSurf = MoistFluxSurf / rhoWater

                    END SELECT

                END IF

                CALL ESMF_FieldRegrid(FieldFromState , InternalStateData%ICON2GETM_Field(nVar) , &
                    & routeHandle = to_XGrid , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_FieldRegrid ICON to xgrid "//TRIM(FieldName))

                CALL ESMF_FieldRegrid(InternalStateData%ICON2GETM_Field(nVar) , FieldToState , &
                    & routeHandle = XGrid_to , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_FieldRegrid "//TRIM(FieldName)//" to "//TRIM(config_ESMF%vars_GETM_import(j)))

            END IF

!CALL ESMF_FieldGet(FieldToState , localDeCount = localDeCount , rc = localrc)
!CALL checkESMF(localrc , routine , "ESMF_FieldGet 2GETM localDeCount")
!
!IF (localDeCount > 0) THEN
!
!    CALL ESMF_FieldGet(FieldToState , fArrayPtr = FArrayPtr2D , rc = localrc)
!    CALL checkESMF(localrc , routine , "ESMF_FieldGet GETM pointer "//TRIM(config_ESMF%vars_GETM_Import(j)))
!
!    WRITE(0,*) TRIM(config_ESMF%vars_GETM_Import(j)) , ": " , MINVAL(FArrayPtr2D) , MAXVAL(FArrayPtr2D)
!
!END IF

        END DO

    END IF

    CALL ESMF_ClockPrint(Cpl_Clock , options = "currTime" , preString = "----------------> model time step to: " , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_ClockPrint Cpl current time + step")

END SUBROUTINE Cpl_Comp_Run


SUBROUTINE Cpl_Comp_Finalize(Cpl_Comp , localrc)

    TYPE(ESMF_GridComp)             :: Cpl_Comp

    INTEGER, INTENT(OUT)            :: localrc
    
    TYPE(ESMF_State)                :: ImportState , ExportState
    TYPE(ESMF_State)                :: fromICON , toGETM , fromGETM , toICON
    TYPE(ESMF_XGrid)                :: ICON2GETM_XGrid , GETM2ICON_XGrid
    TYPE(ESMF_RouteHandle)          :: ICON2XGrid , XGrid2GETM , GETM2XGrid , XGrid2ICON

    TYPE(Cpl_InternalState)                 :: InternalStateWrapper
    TYPE(Cpl_InternalStateStruct) , POINTER :: InternalStateData

    CHARACTER(LEN=34), PARAMETER    :: routine = "mo_esmf_cpl_comp:Cpl_Comp_Finalize"


    CALL ESMF_LogWrite("Coupler: finalize" , ESMF_LOGMSG_INFO , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_LogWrite Coupler finalize")

    CALL ESMF_GridCompGet(Cpl_Comp , importState = ImportState , exportState = exportState , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_GridCompGet Cpl states")

    ! Get Internal State
   
    CALL ESMF_GridCompGetInternalState(Cpl_Comp , InternalStateWrapper , localrc)
    CALL checkESMF(localrc , routine , "ESMF_GridCompGetInternalState")

    InternalStateData => InternalStateWrapper%wrapper 

    IF (InternalStateData%nVar_ICON2 > 0) THEN   

        CALL ESMF_StateGet(ImportState , "ICON" , nestedState = fromICON , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet import nested state ICON")

        CALL ESMF_StateGet(ExportState , "GETM" , nestedState =   toGETM , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet export nested state GETM")

    END IF

    IF (InternalStateData%nVar_GETM2 > 0) THEN
    
        CALL ESMF_StateGet(ImportState , "GETM" , nestedState = fromGETM , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet import nested state GETM")
    
        CALL ESMF_StateGet(ExportState , "ICON" , nestedState =   toICON , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet export nested state ICON") 

    END IF
    
    IF (ALLOCATED(InternalStateData%ICON2GETM_Field)) THEN

        CALL FinalizeNestedStates(fromICON , "ICON" , toGETM , "GETM" , InternalStateData%ICON2GETM_Field)

    END IF


    IF (ALLOCATED(InternalStateData%GETM2ICON_Field)) THEN

        CALL FinalizeNestedStates(fromGETM , "GETM" , toICON , "ICON" , InternalStateData%GETM2ICON_Field)

    END IF

    IF (InternalStateData%nVar_ICON2 > 0) THEN
    
        CALL ESMF_StateDestroy(fromICON , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateDestroy from ICON")

        CALL ESMF_StateDestroy(  toGETM , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateDestroy   to GETM")

    END IF    

    IF (InternalStateData%nVar_GETM2 > 0) THEN
    
        CALL ESMF_StateDestroy(fromGETM , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateDestroy from GETM")
    
        CALL ESMF_StateDestroy(  toICON , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateDestroy   to ICON")

    END IF

    DEALLOCATE(InternalStateData%VarConnection)
    DEALLOCATE(InternalStateData)

END SUBROUTINE Cpl_Comp_Finalize


SUBROUTINE FinalizeNestedStates(fromState , from , toState , to , XGridField)

    TYPE(ESMF_State), INTENT(INOUT)                 :: fromState
    
    CHARACTER(LEN=4), INTENT(IN)                    :: from

    TYPE(ESMF_State), INTENT(INOUT)                 :: toState

    CHARACTER(LEN=4), INTENT(IN)                    :: to

    TYPE(ESMF_Field), ALLOCATABLE, DIMENSION(:)     :: XGridField

    INTEGER                                         :: localrc
    INTEGER                                         :: i

    CHARACTER(LEN=128)                              :: FieldName

    TYPE(ESMF_RouteHandle)                          :: routeHandle
    TYPE(ESMF_XGrid)                                :: XGrid


    CHARACTER(LEN=37), PARAMETER                    :: routine = "mo_esmf_cpl_comp:FinalizeNestedStates"    


    CALL FinalizeFieldsInState(fromState , from)
    CALL FinalizeFieldsInState(  toState , to)

    DO i = 1 , SIZE(XGridField)

        CALL ESMF_FieldGet(XGridField(i) , name = FieldName , xgrid = XGrid , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldGet name and xgrid "//TRIM(FieldName))

        CALL ESMF_XGridDestroy(XGrid , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_XGridDestroy xgrid "//TRIM(FieldName))

        CALL ESMF_FieldDestroy(XGridField(i) , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldDestroy "//from//" to "//to//" xgrid field "//TRIM(FieldName))

        CALL ESMF_StateGet(fromState , from//"2_"//TRIM(FieldName) , routehandle = routeHandle , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet route handle "//from//"2_"//TRIM(FieldName))

        CALL ESMF_FieldRegridRelease(routeHandle , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldRegridRelease route handle "//from//"2_"//TRIM(FieldName))

        CALL ESMF_StateGet(  toState , TRIM(FieldName)//"_2"//to , routehandle = routeHandle , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_StateGet route handle "//TRIM(FieldName)//"_2"//to)

        CALL ESMF_FieldRegridRelease(routeHandle , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_FieldRegridRelease route handle "//TRIM(FieldName)//"_2"//to)

    END DO
        
END SUBROUTINE FinalizeNestedStates


SUBROUTINE FinalizeFieldsInState(State , Model)

    TYPE(ESMF_State), INTENT(INOUT)                         :: State
    
    CHARACTER(LEN=4), INTENT(IN)                            :: Model

    CHARACTER(LEN=128), ALLOCATABLE, DIMENSION(:)           :: itemNameList

    TYPE(ESMF_StateItem_Flag), ALLOCATABLE, DIMENSION(:)    :: itemTypeList

    TYPE(ESMF_Field)                                        :: FieldFromState
    TYPE(ESMF_Grid)                                         :: GETM_Grid
    TYPE(ESMF_Mesh)                                         :: ICON_Mesh
    
    INTEGER                                                 :: i , itemCount
    INTEGER                                                 :: localrc


    CHARACTER(LEN=38), PARAMETER                            :: routine = "mo_esmf_cpl_comp:FinalizeFieldsInState"


    CALL ESMF_StateGet(State , itemCount = itemCount , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_StateGet item count from "//Model)

    ALLOCATE(itemNameList(itemCount))
    ALLOCATE(itemTypeList(itemCount))

    CALL ESMF_StateGet(State , itemNameList = itemNameList , itemTypeList = itemTypeList , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_StateGet item names and types from "//Model)

    DO i = 1 , itemCount

        IF (itemTypeList(i) == ESMF_STATEITEM_FIELD) THEN

            CALL ESMF_StateGet(State , TRIM(itemNameList(i)) , field = FieldFromState , rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_StateGet field "//TRIM(itemNameList(i)))

            IF (Model == "ICON") THEN

                CALL ESMF_FieldGet(FieldFromState , mesh = ICON_Mesh , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_FieldGet ICON mesh "//TRIM(itemNameList(i)))

                CALL ESMF_MeshDestroy(ICON_Mesh , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_GridDestroy GETM grid")

            ELSEIF (Model == "GETM") THEN

                CALL ESMF_FieldGet(FieldFromState , grid = GETM_Grid , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_FieldGet GETM grid "//TRIM(itemNameList(i)))

                CALL ESMF_GridDestroy(GETM_Grid , rc = localrc)
                CALL checkESMF(localrc , routine , "ESMF_GridDestroy GETM grid")

            ELSE

                CALL checkESMF(-1 , routine , "something went wrong, no mesh/grid")

            END IF

            CALL ESMF_FieldDestroy(FieldFromState , rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_FieldDestroy "//Model//" field "//TRIM(itemNameList(i)))

        END IF

    END DO

    DEALLOCATE(itemNameList , itemTypeList)
    
END SUBROUTINE FinalizeFieldsInState


SUBROUTINE defineConnections2GETM(Var_ICON2 , nVar_ICON2)

    CHARACTER(LEN=128), INTENT(OUT) :: Var_ICON2(28)

    INTEGER, INTENT(OUT)            :: nVar_ICON2

    INTEGER                         :: j

    LOGICAL                         :: pres_msl_added , u_10m_added , v_10m_added , t_s_added , t_2m_added , td_2m_added , &
                                     & umfl_s_added , vmfl_s_added , rain_gsp_rate_added , snow_gsp_rate_added , &
                                     & graupel_gsp_rate_added , ice_gsp_rate_added , hail_gsp_rate_added , rain_con_rate_added , &
                                     & snow_con_rate_added , sob_s_added , thb_s_added , sodifd_s_added , shfl_s_added , &
                                     & lhfl_s_added , qv_s_added , rh_2m_added , qhfl_s_added , clct_added

    CHARACTER(LEN=39), PARAMETER    :: routine = "mo_esmf_cpl_comp:defineConnections2GETM"


    pres_msl_added          = .FALSE.
    u_10m_added             = .FALSE.
    v_10m_added             = .FALSE.
    t_s_added               = .FALSE.
    t_2m_added              = .FALSE.
    td_2m_added             = .FALSE.
    umfl_s_added            = .FALSE.
    vmfl_s_added            = .FALSE.
    rain_gsp_rate_added     = .FALSE.
    snow_gsp_rate_added     = .FALSE.
    graupel_gsp_rate_added  = .FALSE.
    ice_gsp_rate_added      = .FALSE.
    hail_gsp_rate_added     = .FALSE.
    rain_con_rate_added     = .FALSE.
    snow_con_rate_added     = .FALSE.
    sob_s_added             = .FALSE.
    thb_s_added             = .FALSE.
    sodifd_s_added          = .FALSE.
    shfl_s_added            = .FALSE.
    lhfl_s_added            = .FALSE.
    qv_s_added              = .FALSE.
    rh_2m_added             = .FALSE.
    qhfl_s_added            = .FALSE.
    clct_added              = .FALSE.


    nVar_ICON2 = 0

    DO j = 1 , nVar_2GETM

        nVar_ICON2 = nVar_ICON2 + 1

        SELECT CASE (TRIM(config_ESMF%vars_GETM_Import(j)))

            CASE ("air_pressure_at_sea_level")

                IF (.NOT. pres_msl_added) THEN

                    Var_ICON2(nVar_ICON2) = "pres_msl"
                    pres_msl_added = .TRUE.

                ELSE

                    nVar_ICON2 = nVar_ICON2 - 1

                END IF

            CASE ("wind_x_velocity_at_10m")

                IF (.NOT. u_10m_added) THEN

                    Var_ICON2(nVar_ICON2) = "u_10m"
                    u_10m_added = .TRUE.

                ELSE

                    nVar_ICON2 = nVar_ICON2 - 1

                END IF

            CASE ("wind_y_velocity_at_10m")

                IF (.NOT. v_10m_added) THEN

                    Var_ICON2(nVar_ICON2) = "v_10m"
                    v_10m_added = .TRUE.

                ELSE

                    nVar_ICON2 = nVar_ICON2 - 1

                END IF

            CASE ("air_temperature_at_2m")

                IF (.NOT. t_2m_added) THEN

                    Var_ICON2(nVar_ICON2) = "t_2m"
                    t_2m_added = .TRUE.

                ELSE

                    nVar_ICON2 = nVar_ICON2 - 1

                END IF

            CASE ("dew_point_temperature")

                IF (.NOT. td_2m_added) THEN

                    Var_ICON2(nVar_ICON2) = "td_2m"
                    td_2m_added = .TRUE.

                ELSE

                    nVar_ICON2 = nVar_ICON2 - 1

                END IF

            CASE ("surface_downward_eastward_stress")

                IF (.NOT. umfl_s_added) THEN

                    Var_ICON2(nVar_ICON2) = "umfl_s"
                    umfl_s_added = .TRUE.

                ELSE

                    nVar_ICON2 = nVar_ICON2 - 1

                END IF

            CASE ("surface_downward_northward_stress")

                IF (.NOT. vmfl_s_added) THEN

                    Var_ICON2(nVar_ICON2) = "vmfl_s"
                    vmfl_s_added = .TRUE.

                ELSE

                    nVar_ICON2 = nVar_ICON2 - 1

                END IF

            CASE ("precipitation")

                IF (.NOT. rain_gsp_rate_added) THEN

                    Var_ICON2(nVar_ICON2) = "rain_gsp_rate"
                    rain_gsp_rate_added = .TRUE.

                ELSE

                    nVar_ICON2 = nVar_ICON2 - 1

                END IF

                IF (.NOT. snow_gsp_rate_added) THEN

                    nVar_ICON2 = nVar_ICON2 + 1
                    Var_ICON2(nVar_ICON2) = "snow_gsp_rate"
                    snow_gsp_rate_added = .TRUE.

                END IF

                IF (.NOT. graupel_gsp_rate_added) THEN

                    nVar_ICON2 = nVar_ICON2 + 1
                    Var_ICON2(nVar_ICON2) = "graupel_gsp_rate"
                    graupel_gsp_rate_added = .TRUE.

                END IF

                IF (.NOT. ice_gsp_rate_added) THEN

                    nVar_ICON2 = nVar_ICON2 + 1
                    Var_ICON2(nVar_ICON2) = "ice_gsp_rate"
                    ice_gsp_rate_added = .TRUE.

                END IF

                IF (.NOT. hail_gsp_rate_added) THEN

                    nVar_ICON2 = nVar_ICON2 + 1
                    Var_ICON2(nVar_ICON2) = "hail_gsp_rate"
                    hail_gsp_rate_added = .TRUE.

                END IF

                IF (.NOT. rain_con_rate_added) THEN

                    nVar_ICON2 = nVar_ICON2 + 1
                    Var_ICON2(nVar_ICON2) = "rain_con_rate"
                    rain_con_rate_added = .TRUE.

                END IF

                IF (.NOT. snow_con_rate_added) THEN

                    nVar_ICON2 = nVar_ICON2 + 1
                    Var_ICON2(nVar_ICON2) = "snow_con_rate"
                    snow_con_rate_added = .TRUE.

                END IF

            CASE ("surface_downwelling_shortwave_flux")

                IF (.NOT. sob_s_added) THEN

                    Var_ICON2(nVar_ICON2) = "sob_s"
                    sob_s_added = .TRUE.

                ELSE

                    nVar_ICON2 = nVar_ICON2 - 1

                END IF

            CASE ("surface_downward_heat_flux")

                IF (.NOT. shfl_s_added) THEN

                    Var_ICON2(nVar_ICON2) = "shfl_s"
                    shfl_s_added = .TRUE.

                ELSE

                    nVar_ICON2 = nVar_ICON2 - 1

                END IF

                IF (.NOT. lhfl_s_added) THEN

                    nVar_ICON2 = nVar_ICON2 + 1
                    Var_ICON2(nVar_ICON2) = "lhfl_s"
                    lhfl_s_added = .TRUE.

                END IF

                IF (.NOT. thb_s_added) THEN

                    nVar_ICON2 = nVar_ICON2 + 1
                    Var_ICON2(nVar_ICON2) = "thb_s"
                    thb_s_added = .TRUE.

                END IF

            CASE ("evaporation")

                IF (.NOT. qhfl_s_added) THEN

                    Var_ICON2(nVar_ICON2) = "qhfl_s"
                    qhfl_s_added = .TRUE.

                ELSE

                    nVar_ICON2 = nVar_ICON2 - 1

                END IF

            CASE ("relative_humidity")

                IF (.NOT. rh_2m_added) THEN

                    Var_ICON2(nVar_ICON2) = "rh_2m"
                    rh_2m_added = .TRUE.

                ELSE

                    nVar_ICON2 = nVar_ICON2 - 1

                END IF

            CASE ("specific_humidity")

                IF (.NOT. qv_s_added) THEN

                    Var_ICON2(nVar_ICON2) = "qv_s"
                    qv_s_added = .TRUE.

                ELSE

                    nVar_ICON2 = nVar_ICON2 - 1

                END IF

            CASE ("total_cloud_cover")

                IF (.NOT. clct_added) THEN

                    Var_ICON2(nVar_ICON2) = "clct"
                    clct_added = .TRUE.

                ELSE

                    nVar_ICON2 = nVar_ICON2 - 1

                END IF

            CASE DEFAULT

                CALL checkESMF(-1 , routine , TRIM(config_ESMF%vars_GETM_Import(j))//" not a recognised field of data")

        END SELECT

    END DO

END SUBROUTINE defineConnections2GETM


SUBROUTINE defineConnections2ICON(Var_GETM2 , nVar_GETM2)

    CHARACTER(LEN=128), INTENT(OUT) :: Var_GETM2(1)

    INTEGER, INTENT(OUT)            :: nVar_GETM2

    INTEGER                         :: i

    CHARACTER(LEN=39), PARAMETER    :: routine = "mo_esmf_cpl_comp:defineConnections2ICON"


    nVar_GETM2 = 0

    DO i = 1 , nVar_2ICON

        nVar_GETM2 = nVar_GETM2 + 1

        SELECT CASE (TRIM(config_ESMF%vars_ICON_Import(i)))

            CASE ("t_seasfc")

                Var_GETM2(nVar_GETM2) = "sea_surface_temperature"

            CASE DEFAULT

                CALL checkESMF(-1 , routine , TRIM(config_ESMF%vars_ICON_Import(i))//" not a recognised field of data")

        END SELECT

    END DO


END SUBROUTINE defineConnections2ICON


SUBROUTINE setConnections2GETM(VarConnection , nVar_2GETM , FieldNamesFromICON , FieldNamesFromGETM)

    INTEGER, ALLOCATABLE, DIMENSION(:,:), INTENT(INOUT)         :: VarConnection

    INTEGER, INTENT(IN)                                         :: nVar_2GETM

    CHARACTER(LEN=128), ALLOCATABLE, DIMENSION(:), INTENT(IN)   :: FieldNamesFromGETM , FieldNamesFromICON

    CHARACTER(LEN=128)                                          :: FieldName
    INTEGER                                                     :: i , j

    CHARACTER(LEN=36), PARAMETER                                :: routine = "mo_esmf_cpl_comp:setConnections2GETM"


    DO j = 1 , nVar_2GETM

        SELECT CASE (TRIM(config_ESMF%vars_GETM_Import(j)))

            CASE ("air_pressure_at_sea_level")

                FieldName = "pres_msl"

            CASE ("wind_x_velocity_at_10m")

                FieldName = "u_10m"

            CASE ("wind_y_velocity_at_10m")

                FieldName = "v_10m"

            CASE ("air_temperature_at_2m")

                FieldName = "t_2m"

            CASE ("dew_point_temperature")

                FieldName = "td_2m"

            CASE ("surface_downward_eastward_stress")

                FieldName = "umfl_s"

            CASE ("surface_downward_northward_stress")

                FieldName = "vmfl_s"

            CASE ("precipitation")

                FieldName = "rain_gsp_rate"

                i = FINDLOC(FieldNamesFromICON , "snow_gsp_rate")
                VarConnection(nVar_ICON_Import + i , nVar_ICON_Import + nVar_ICON_Export + j) = 2

                i = FINDLOC(FieldNamesFromICON , "graupel_gsp_rate")
                VarConnection(nVar_ICON_Import + i , nVar_ICON_Import + nVar_ICON_Export + j) = 3

                i = FINDLOC(FieldNamesFromICON , "ice_gsp_rate")
                VarConnection(nVar_ICON_Import + i , nVar_ICON_Import + nVar_ICON_Export + j) = 4

                i = FINDLOC(FieldNamesFromICON , "hail_gsp_rate")
                VarConnection(nVar_ICON_Import + i , nVar_ICON_Import + nVar_ICON_Export + j) = 5

                i = FINDLOC(FieldNamesFromICON , "rain_con_rate")
                VarConnection(nVar_ICON_Import + i , nVar_ICON_Import + nVar_ICON_Export + j) = 6

                i = FINDLOC(FieldNamesFromICON , "snow_con_rate")
                VarConnection(nVar_ICON_Import + i , nVar_ICON_Import + nVar_ICON_Export + j) = 7

            CASE ("surface_downwelling_shortwave_flux")

                FieldName = "sob_s"

            CASE ("surface_downward_heat_flux")

                FieldName = "shfl_s"

                i = FINDLOC(FieldNamesFromICON , "lhfl_s")
                VarConnection(nVar_ICON_Import + i , nVar_ICON_Import + nVar_ICON_Export + j) = 2

                i = FINDLOC(FieldNamesFromICON , "thb_s")
                VarConnection(nVar_ICON_Import + i , nVar_ICON_Import + nVar_ICON_Export + j) = 3

            CASE ("evaporation")

                FieldName = "qhfl_s"

            CASE ("relative_humidity")

                FieldName = "rh_2m"

            CASE ("specific_humidity")

                FieldName = "qv_s"

            CASE ("total_cloud_cover")

                FieldName = "clct"

        END SELECT

        i = FINDLOC(FieldNamesFromICON , TRIM(FieldName))
        VarConnection(nVar_ICON_Import + i , nVar_ICON_Import + nVar_ICON_Export + j) = 1

    END DO


END SUBROUTINE setConnections2GETM


SUBROUTINE setConnections2ICON(VarConnection , nVar_2ICON , FieldNamesFromGETM)

    INTEGER, ALLOCATABLE, DIMENSION(:,:), INTENT(INOUT)         :: VarConnection

    INTEGER, INTENT(IN)                                         :: nVar_2ICON

    CHARACTER(LEN=128), ALLOCATABLE, DIMENSION(:), INTENT(IN)   :: FieldNamesFromGETM

    CHARACTER(LEN=128)                                          :: FieldName

    INTEGER                                                     :: i , j

    CHARACTER(LEN=36), PARAMETER                                :: routine = "mo_esmf_cpl_comp:setConnections2ICON"


    DO i = 1 , nVar_2ICON

        SELECT CASE (TRIM(config_ESMF%vars_ICON_Import(i)))

            CASE ("t_seasfc")

                FieldName = "sea_surface_temperature"

        END SELECT

        j = FINDLOC(FieldNamesFromGETM , TRIM(FieldName))
        VarConnection(i , nVar_ICON_Import + nVar_ICON_Export + nVar_GETM_Import + j) = 1

    END DO


END SUBROUTINE setConnections2ICON


SUBROUTINE checkConnectionStatus(State , model , stateType , nestedState , nVar_ICON2)

    TYPE(ESMF_State), INTENT(IN)        :: State

    CHARACTER(LEN=4), INTENT(IN)        :: model
    CHARACTER(LEN=6), INTENT(IN)        :: stateType

    TYPE(ESMF_State), INTENT(OUT)       :: nestedState

    INTEGER, OPTIONAL, INTENT(INOUT)    :: nVar_ICON2

    INTEGER                             :: i
    INTEGER                             :: localrc

    CHARACTER(LEN=4)                    :: ToFrom

    CHARACTER(ESMF_MAXSTR), POINTER     :: ConnectedList(:) , StandardNameList(:)

    CHARACTER(LEN=38), PARAMETER        :: routine = "mo_esmf_cpl_comp:checkConnectionStatus"


    IF (stateType .EQ. "import") THEN

        ToFrom = "from"

    ELSE IF (stateType .EQ. "export") THEN

        ToFrom = "  to"

    END IF

    CALL ESMF_StateGet(State , model , nestedState = nestedState , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_StateGet "//stateType//" nested state "//model)

    IF (.NOT. NUOPC_IsConnected(nestedState , rc = localrc)) THEN

        CALL checkESMF(localrc , routine , "NUOPC_IsConnected "//ToFrom//" "//model)

        IF (model .EQ. "ICON" .AND. ToFrom .EQ. "from") THEN

            NULLIFY(ConnectedList)
            NULLIFY(StandardNameList)

            CALL NUOPC_GetStateMemberLists(nestedState , StandardNameList , ConnectedList , rc = localrc)
            CALL checkESMF(localrc , routine , "NUOPC_GetStateMemberLists "//ToFrom//" "//model)

            DO i = 1 , SIZE(ConnectedList , DIM = 1)

                IF (TRIM(ConnectedList(i)) .EQ. "false") THEN

                    SELECT CASE (TRIM(StandardNameList(i)))

                        CASE ("graupel_gsp_rate" , "hail_gsp_rate" , "ice_gsp_rate")

                            nVar_ICON2 = nVar_ICON2 - 1
                            CALL NUOPC_Realize(nestedState , TRIM(StandardNameList(i)) , &
                                              & removeNotConnected = .true. , rc = localrc)
                            CALL checkESMF(localrc , routine , "NUOPC_Realize "//TRIM(StandardNameList(i)))

                        CASE DEFAULT

                            CALL checkESMF(-1 , routine , "NUOPC_IsConnected from ICON "//TRIM(StandardNameList(i))//" not connected")

                    END SELECT

                END IF

            END DO

        ELSE

            CALL checkESMF(-1 , routine , "NUOPC_IsConnected "//ToFrom//" "//model//" not all fields connected")

        END IF

    ELSE 

        CALL checkESMF(localrc , routine , "NUOPC_IsConnected "//ToFrom//" "//model)

    END IF

END SUBROUTINE checkConnectionStatus


END MODULE mo_esmf_cpl_comp

