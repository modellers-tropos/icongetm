MODULE mo_ICONGETM

    USE ESMF
    USE NUOPC
    USE NUOPC_Driver,                 ICONGETM_Comp_SetSerivces            => SetServices , &
                                    & ICONGETM_Comp_Label_SetModelServices => label_SetModelServices , &
                                    & ICONGETM_Comp_Label_SetRunSequence   => label_SetRunSequence , &
                                    & ICONGETM_Comp_Label_ModifyCplLists   => label_ModifyCplLists

    USE NUOPC_Connector,        ONLY: Coupler_SetServices => SetServices

    USE mo_esmf_icon_comp,      ONLY: ICON_SetServices    => SetServices
    USE mo_nuopc_getm_comp,     ONLY: GETM_SetServices    => GETM_CompSetServices
    USE mo_esmf_cpl_comp,       ONLY: Cpl_SetServices     => SetServices
!    USE mo_esmf_coupler_comp,   ONLY: Coupler_SetServices => SetServices

    USE mo_esmf_timemanagement, ONLY: SetCalendarClock
    USE mo_esmf_var,            ONLY: config_ESMF
    USE mo_esmf_utilities,      ONLY: checkESMF

    IMPLICIT NONE

    PUBLIC :: SetServices

    PRIVATE :: Set_ICONGETM_Services

CONTAINS


SUBROUTINE SetServices(ICONGETM_Comp , localrc)

    TYPE(ESMF_GridComp)             :: ICONGETM_Comp

    INTEGER, INTENT(OUT)            :: localrc

    CHARACTER(LEN=23), PARAMETER    :: routine = "mo_ICONGETM:SetServices"


    CALL NUOPC_CompDerive(ICONGETM_Comp , ICONGETM_Comp_SetSerivces , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompDerive")

    CALL NUOPC_CompSpecialize(ICONGETM_Comp , specLabel = ICONGETM_Comp_Label_SetModelServices , &
        & specRoutine = Set_ICONGETM_Services , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompSpecialize ICONGETM services")

    CALL NUOPC_CompSpecialize(ICONGETM_Comp , specLabel = ICONGETM_Comp_Label_SetRunSequence , &
        & specRoutine = Set_ICONGETM_Sequence , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompSpecialize ICONGETM run sequence")
  
    CALL NUOPC_CompSpecialize(ICONGETM_Comp , specLabel = ICONGETM_Comp_Label_ModifyCplLists, &
        & specRoutine = Modify_ICONGETM_CplLists , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompSpecialize ICONGETM modify CplLists")

END SUBROUTINE SetServices


SUBROUTINE Set_ICONGETM_Services(ICONGETM_Comp , localrc)

    TYPE(ESMF_GridComp)             :: ICONGETM_Comp

    INTEGER, INTENT(OUT)            :: localrc

    TYPE(ESMF_GridComp)             :: ICON_Comp , GETM_Comp , XGRID_Comp
    TYPE(ESMF_CplComp)              :: Connector

    INTEGER                         :: ICONGETM_nProc
    INTEGER                         :: i
    
    TYPE(ESMF_Calendar)             :: ICONGETM_Calendar
    TYPE(ESMF_Clock)                :: ICONGETM_Clock

    CHARACTER(LEN=33), PARAMETER    :: routine = "mo_ICONGETM:Set_ICONGETM_Services"


    ! get number of processes for ICONGETM
    CALL ESMF_GridCompGet(ICONGETM_Comp , petCount = ICONGETM_nProc , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_GridCompGet number of processes")

    IF (ICONGETM_nProc /= config_ESMF%nProc_ICON + config_ESMF%nProc_GETM) THEN

        CALL checkESMF(-1 , routine , "Number of processes for components is not consistent with overall number of processes!!!")

    END IF


    ! create ICON GridComp, set ICON services and add ICON GridComp to ICONGETM_Comp (NUOPC model)
    CALL NUOPC_DriverAddComp(ICONGETM_Comp , "ICON" , ICON_SetServices , &
        & petList = (/ (i , i = 0 , config_ESMF%nProc_ICON - 1) /) , &
        & comp = ICON_Comp , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_DriverAddComp ICON")

    CALL NUOPC_CompAttributeSet(ICON_Comp , name = "Verbosity" , value = TRIM(config_ESMF%verbosity) , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompAttributeSet ICON verbosity")


    ! create GETM GridComp, set GETM services and add GETM GridComp to ICONGETM_Comp (NUOPC model)
    CALL NUOPC_DriverAddComp(ICONGETM_Comp , "GETM" , GETM_SetServices , &
        & petList = (/ (i , i = config_ESMF%nProc_ICON , config_ESMF%nProc_ICON + config_ESMF%nProc_GETM - 1) /) , &
        & comp = GETM_Comp , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_DriverAddComp GETM")

    CALL NUOPC_CompAttributeSet(GETM_Comp , name = "Verbosity" , value = TRIM(config_ESMF%verbosity) , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompAttributeSet GETM verbosity")


    ! create Coupler CplComp (air-sea interface), set Coupler services and add Coupler CplComp to ICONGETM_Comp (NUOPC mediator)
    CALL NUOPC_DriverAddComp(ICONGETM_Comp , "Coupler" , Cpl_SetServices , &
        & petList = (/ (i , i = 0 , ICONGETM_nProc - 1) /) , comp = XGRID_Comp , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_DriverAddComp Coupler")

    CALL NUOPC_CompAttributeSet(XGRID_Comp , name = "Verbosity" , value = TRIM(config_ESMF%verbosity) , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompAttributeSet Coupler verbosity")


    ! create Connector for ICON-TO-Coupler-GETM
    CALL NUOPC_DriverAddComp(ICONGETM_Comp , srcCompLabel = "ICON" , dstCompLabel = "Coupler" , &
        & compSetServicesRoutine = Coupler_SetServices , comp = Connector , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_DriverAddComp ICON-TO-Coupler")
    
    CALL NUOPC_CompAttributeSet(Connector , name = "Verbosity" , value = TRIM(config_ESMF%verbosity) , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompAttributeSet ICON-TO-Coupler verbosity")

    CALL NUOPC_DriverAddComp(ICONGETM_Comp , srcCompLabel = "Coupler" , dstCompLabel = "GETM" , &
        & compSetServicesRoutine = Coupler_SetServices , comp = Connector , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_DriverAddComp Coupler-TO-GETM")
    
    CALL NUOPC_CompAttributeSet(Connector , name = "Verbosity" , value = TRIM(config_ESMF%verbosity) , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompAttributeSet Coupler-TO-GETM verbosity")


    ! create Connector for GETM-TO-Coupler-ICON
    CALL NUOPC_DriverAddComp(ICONGETM_Comp , srcCompLabel = "GETM" , dstCompLabel = "Coupler" , &
        & compSetServicesRoutine = Coupler_SetServices , comp = Connector , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_DriverAddComp GETM-TO-Coupler")
    
    CALL NUOPC_CompAttributeSet(Connector , name = "Verbosity" , value = TRIM(config_ESMF%verbosity) , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompAttributeSet GETM-TO-Coupler verbosity")

   CALL NUOPC_DriverAddComp(ICONGETM_Comp , srcCompLabel = "Coupler" , dstCompLabel = "ICON" , &
        & compSetServicesRoutine = Coupler_SetServices , comp = Connector , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_DriverAddComp Coupler-TO-ICON")

    CALL NUOPC_CompAttributeSet(Connector , name = "Verbosity" , value = TRIM(config_ESMF%verbosity) , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_CompAttributeSet Coupler-TO-ICON verbosity")


!    ! create Connector for ICON-TO-GETM
!    CALL NUOPC_DriverAddComp(ICONGETM_Comp , srcCompLabel = "ICON" , dstCompLabel = "GETM" , &
!        & compSetServicesRoutine = Coupler_SetServices , comp = Connector , rc = localrc)
!    CALL checkESMF(localrc , routine , "NUOPC_DriverAddComp ICON-TO-GETM")
!    
!    CALL NUOPC_CompAttributeSet(Connector , name = "Verbosity" , value = TRIM(config_ESMF%verbosity) , rc = localrc)
!    CALL checkESMF(localrc , routine , "NUOPC_CompAttributeSet ICON-TO-GETM verbosity")
!
!    ! create Connector for GETM-TO-ICON
!    CALL NUOPC_DriverAddComp(ICONGETM_Comp , srcCompLabel = "GETM" , dstCompLabel = "ICON" , &
!        & compSetServicesRoutine = Coupler_SetServices , comp = Connector , rc = localrc)
!    CALL checkESMF(localrc , routine , "NUOPC_DriverAddComp GETM-TO-ICON")
!    
!    CALL NUOPC_CompAttributeSet(Connector , name = "Verbosity" , value = TRIM(config_ESMF%verbosity) , rc = localrc)
!    CALL checkESMF(localrc , routine , "NUOPC_CompAttributeSet GETM-TO-ICON verbosity")


    ! set ICONGETM clock
    CALL SetCalendarClock(ICONGETM_Calendar , ICONGETM_Clock)

    CALL ESMF_GridCompSet(ICONGETM_Comp , clock = ICONGETM_Clock , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_GridCompSet ICONGETM clock")

END SUBROUTINE Set_ICONGETM_Services


SUBROUTINE Set_ICONGETM_Sequence(ICONGETM_Comp , localrc)

    TYPE(ESMF_GridComp)  :: ICONGETM_Comp

    INTEGER, INTENT(OUT) :: localrc

    TYPE(NUOPC_FreeFormat)          :: runSequenceFreeFormat

    CHARACTER(LEN=33), PARAMETER    :: routine = "mo_ICONGETM:Set_ICONGETM_Sequence"
    

    ! set up free format run sequence
    runSequenceFreeFormat = NUOPC_FreeFormatCreate(stringList = (/ &
        & " @*                 " , &
        & "   ICON -> Coupler  " , &
        & "   GETM -> Coupler  " , &
        & "   Coupler          " , &
        & "   Coupler -> ICON  " , &
        & "   Coupler -> GETM  " , &
        & "   ICON             " , &
        & "   GETM             " , &
        & " @                  " /) , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_FreeFormatCreate")
    
    ! ingest FreeFormat run sequence
    CALL NUOPC_DriverIngestRunSequence(ICONGETM_Comp , runSequenceFreeFormat , &
        & autoAddConnectors = .TRUE. , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_DriverIngestRunSequence")

    ! clean-up
    CALL NUOPC_FreeFormatDestroy(runSequenceFreeFormat , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_FreeFormatDestroy")

!    CALL NUOPC_DriverNewRunSequence(ICONGETM_Comp , slotCount = 1 , rc = localrc)
!    CALL checkESMF(localrc , routine , "NUOPC_DriverNewRunSequence")
!
!    CALL NUOPC_DriverAddRunElement(ICONGETM_Comp , slot = 1 , srcCompLabel = "ICON" , dstCompLabel = "GETM" , rc = localrc)
!    CALL checkESMF(localrc , routine , "NUOPC_DriverAddRunElement ICON2GETM")
!
!    CALL NUOPC_DriverAddRunElement(ICONGETM_Comp , slot = 1 , srcCompLabel = "GETM" , dstCompLabel = "ICON" , rc = localrc)
!    CALL checkESMF(localrc , routine , "NUOPC_DriverAddRunElement GETM2ICON")
!    
!    CALL NUOPC_DriverAddRunElement(ICONGETM_Comp , slot = 1 , compLabel = "ICON" , rc = localrc)
!    CALL checkESMF(localrc , routine , "NUOPC_DriverAddRunElement ICON")
!
!    CALL NUOPC_DriverAddRunElement(ICONGETM_Comp , slot = 1 , compLabel = "GETM" , rc = localrc)
!    CALL checkESMF(localrc , routine , "NUOPC_DriverAddRunElement GETM")

END SUBROUTINE Set_ICONGETM_Sequence


SUBROUTINE Modify_ICONGETM_CplLists(ICONGETM_Comp , localrc)

    TYPE(ESMF_GridComp)  :: ICONGETM_Comp

    INTEGER, INTENT(OUT) :: localrc

    TYPE(ESMF_CplComp), POINTER     :: ConnectorList(:)

    INTEGER                         :: i, j, CplListSize
    
    CHARACTER(LEN=160), ALLOCATABLE :: CplList(:)

    CHARACTER(LEN=37), PARAMETER    :: routine = "mo_ICONGETM:Modify_ICONGETM_CplLists"


    NULLIFY(ConnectorList)

    CALL NUOPC_DriverGetComp(ICONGETM_Comp , compList = ConnectorList, rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_DriverGetComp connector list")

    DO i = 1 , SIZE(ConnectorList)

        ! query the cplList for connector i
        
        CALL NUOPC_CompAttributeGet(ConnectorList(i) , name = "CplList" , itemCount = CplListSize , rc = localrc)
        CALL checkESMF(localrc , routine , "NUOPC_CompAttributeGet CplListSize")

        IF (CplListSize > 0) THEN

            ALLOCATE(CplList(CplListSize))
            
            CALL NUOPC_CompAttributeGet(ConnectorList(i) , name = "CplList" , valueList = CplList , rc = localrc)
            CALL checkESMF(localrc , routine , "NUOPC_CompAttributeGet CplList")

            ! go through all of the entries in the cplList
        
            DO j = 1 , CplListSize
          
                CplList(j) = TRIM(CplList(j))//":REMAPMETHOD=redist"
        
            END DO
            
            ! store the modified cplList in CplList attribute of connector i
        
            CALL NUOPC_CompAttributeSet(ConnectorList(i) , name = "CplList" , valueList = CplList , rc = localrc)
            CALL checkESMF(localrc , routine , "NUOPC_CompAttributeSet CplList")
        
            DEALLOCATE(CplList)

        END IF

    END DO

    DEALLOCATE(ConnectorList)

END SUBROUTINE Modify_ICONGETM_CplLists


END MODULE mo_ICONGETM
