MODULE mo_netcdf


USE NetCDF


IMPLICIT NONE

PRIVATE :: PUTVAR_INT
PRIVATE :: PUTVAR_REAL

PRIVATE :: CHECK

PUBLIC :: openNetCDF, createNetCDF, closeNetCDF
PUBLIC :: GETDIM, PUTDIM
PUBLIC :: PUTVAR, LOAD_VAR

INTERFACE PUTVAR

    MODULE PROCEDURE PUTVAR_INT, PUTVAR_REAL

END INTERFACE



CONTAINS


SUBROUTINE openNetCDF(fileName , fileID)


! declaration of variables

    ! input

    CHARACTER(LEN = *), INTENT(IN) :: fileName

    ! output

    INTEGER, INTENT(OUT) :: fileID

    ! additional

    INTEGER :: statusNF90


! main code - open NetCDF-file


    statusNF90 = NF90_OPEN(fileName , NF90_NOWRITE , fileID)
    CALL CHECK(statusNF90)


END SUBROUTINE openNetCDF


SUBROUTINE createNetCDF(fileName , fileID)


! declaration of variables

    ! input

    CHARACTER(LEN = *), INTENT(IN) :: fileName

    ! output

    INTEGER, INTENT(OUT) :: fileID

    ! additional

    INTEGER :: statusNF90


! main code - create NetCDF-file

    statusNF90 = NF90_CREATE(fileName , NF90_CLOBBER , fileID)
    CALL CHECK(statusNF90)

    statusNF90 = NF90_ENDDEF(fileID)
    CALL CHECK(statusNF90)


END SUBROUTINE createNetCDF


SUBROUTINE closeNetCDF(fileID)


! declaration of variables

    ! input

    INTEGER, INTENT(IN) :: fileID

    ! additional

    INTEGER :: statusNF90


! main code - close NetCDF-file


    statusNF90 = NF90_CLOSE(fileID)
    CALL CHECK(statusNF90)


END SUBROUTINE closeNetCDF


SUBROUTINE GETDIM(fileID , dimName , dim)

! declaration of variables

    ! input

    INTEGER , INTENT(IN) :: fileID
    CHARACTER(LEN = *) , INTENT(IN) :: dimName

    ! output

    INTEGER, INTENT(OUT) :: dim

    ! additional

    INTEGER :: dimID
    INTEGER :: statusNF90


! main code - load dimension from file

    statusNF90 = NF90_INQ_DIMID(fileID , dimName , dimID)
    CALL CHECK(statusNF90)

    statusNF90 = NF90_INQUIRE_DIMENSION(fileID , dimID , len = dim)
    CALL CHECK(statusNF90)


END SUBROUTINE GETDIM



SUBROUTINE PUTDIM(fileID , dimName , dim)

! declaration of variable

    ! input

    INTEGER, INTENT(IN) :: fileID
    INTEGER, INTENT(IN) :: dim
    CHARACTER(LEN = *), INTENT(IN) :: dimName

    ! additional

    INTEGER :: statusNF90
    INTEGER :: dimID


! main code - write dimension to file

    statusNF90 = NF90_REDEF(fileID)
    CALL CHECK(statusNF90)

    statusNF90 = NF90_DEF_DIM(fileID , dimName , dim , dimID)
    CALL CHECK(statusNF90)

    statusNF90 = NF90_ENDDEF(fileID)
    CALL CHECK(statusNF90)


END SUBROUTINE PUTDIM



SUBROUTINE PUTVAR_REAL(fileID , varName , dimName , var)


! declaration of variables

    ! input

    INTEGER, INTENT(IN) :: fileID
    CHARACTER(LEN = *) , INTENT(IN) :: varName , dimName(2)
    REAL(8), DIMENSION(: , :) , ALLOCATABLE :: var

    ! additional

    INTEGER :: statusNF90
    INTEGER :: varID

    INTEGER , DIMENSION(:) , ALLOCATABLE :: dimID


! main code - write real-variable to file

    IF (dimName(2) /= "NaN") THEN

        ALLOCATE(dimID(2))

        statusNF90 = NF90_INQ_DIMID(fileID , dimName(2) , dimID(1))
        CALL CHECK(statusNF90)

        statusNF90 = NF90_INQ_DIMID(fileID , dimName(1) , dimID(2))
        CALL CHECK(statusNF90)

        var = TRANSPOSE(var)

    ELSE

        ALLOCATE(dimID(1))

        statusNF90 = NF90_INQ_DIMID(fileID , dimName(1) , dimID(1))
        CALL CHECK(statusNF90)

    END IF


    statusNF90 = NF90_REDEF(fileID)
    CALL CHECK(statusNF90)

    statusNF90 = NF90_DEF_VAR(fileID , varName , NF90_DOUBLE , dimID , varID)
    CALL CHECK(statusNF90)

    statusNF90 = NF90_ENDDEF(fileID)
    CALL CHECK(statusNF90)

    statusNF90 = NF90_PUT_VAR(fileID , varID , var)
    CALL CHECK(statusNF90)


    DEALLOCATE(dimID)


END SUBROUTINE PUTVAR_REAL



SUBROUTINE PUTVAR_INT(fileID , varName , dimName , var)


! declaration of variables

    ! input

    INTEGER, INTENT(IN) :: fileID
    CHARACTER(LEN = *) , INTENT(IN) :: varName , dimName(2)
    INTEGER, DIMENSION(: , :) , ALLOCATABLE :: var

    ! additional

    INTEGER :: statusNF90
    INTEGER :: varID

    INTEGER , DIMENSION(:) , ALLOCATABLE :: dimID


! main code - write integer-variable to file

    IF (dimName(2) /= "NaN") THEN

        ALLOCATE(dimID(2))

        statusNF90 = NF90_INQ_DIMID(fileID , dimName(2) , dimID(1))
        CALL CHECK(statusNF90)

        statusNF90 = NF90_INQ_DIMID(fileID , dimName(1) , dimID(2))
        CALL CHECK(statusNF90)

        var = TRANSPOSE(var)

    ELSE

        ALLOCATE(dimID(1))

        statusNF90 = NF90_INQ_DIMID(fileID , dimName(1) , dimID(1))
        CALL CHECK(statusNF90)

    END IF


    statusNF90 = NF90_REDEF(fileID)
    CALL CHECK(statusNF90)

    statusNF90 = NF90_DEF_VAR(fileID , varName , NF90_INT , dimID , varID)
    CALL CHECK(statusNF90)

    statusNF90 = NF90_ENDDEF(fileID)
    CALL CHECK(statusNF90)

    statusNF90 = NF90_PUT_VAR(fileID , varID , var)
    CALL CHECK(statusNF90)


    DEALLOCATE(dimID)


END SUBROUTINE PUTVAR_INT



SUBROUTINE LOAD_VAR(fileID , varName , dimName , var)

! declaration of variables

    ! input

    INTEGER, INTENT(IN) :: fileID
    CHARACTER(Len = *), INTENT(IN) :: varName
    CHARACTER(Len = *), DIMENSION(2), INTENT(IN) :: dimName

    ! output

    REAL, DIMENSION(: , :) , ALLOCATABLE, INTENT(OUT) :: var

    ! additional

    INTEGER :: statusNF90
    INTEGER :: varID
    INTEGER, DIMENSION(2) :: dim , dimID
    REAL, DIMENSION(:) , ALLOCATABLE :: var1D
    REAL, DIMENSION(: , :) , ALLOCATABLE :: var2D


! main code - load variable from file

    statusNF90 = NF90_INQ_DIMID(fileID , dimName(1) , dimID(1))
    CALL CHECK(statusNF90)

    statusNF90 = NF90_INQUIRE_DIMENSION(fileID , dimID(1) , len = dim(1))
    CALL CHECK(statusNF90)


    IF (dimName(2) /= "NaN") THEN

        statusNF90 = NF90_INQ_DIMID(fileID , dimName(2) , dimID(2))
        CALL CHECK(statusNF90)

        statusNF90 = NF90_INQUIRE_DIMENSION(fileID , dimID(2) , len = dim(2))
        CALL CHECK(statusNF90)

    ELSE

        dim(2) = 1

    END IF


    statusNF90 = NF90_INQ_VARID(fileID , varName , varID)
    CALL CHECK(statusNF90)


    IF (dim(2) > 1) THEN

        ALLOCATE(var2D(dim(2) , dim(1)))

        statusNF90 = NF90_GET_VAR(fileID , varID , var2D)
        CALL CHECK(statusNF90)

        ALLOCATE(var(dim(1) , dim(2)))
        var = TRANSPOSE(var2D)

    ELSE

        ALLOCATE(var1D(dim(1)))

        statusNF90 = NF90_GET_VAR(fileID , varID , var1D)
        CALL CHECK(statusNF90)

        ALLOCATE(var(dim(1) , 1))
        var(: , 1) = var1D

    END IF


END SUBROUTINE LOAD_VAR



SUBROUTINE CHECK(statusNF90)


! declaration of variables

    ! input

    INTEGER, INTENT(IN) :: statusNF90


! main code - check status of NetCDF - function


    IF (statusNF90 /= NF90_NOERR) THEN

        WRITE(*,*) trim(NF90_STRERROR(statusNF90))
        STOP  "Stopped"

    END IF



END SUBROUTINE CHECK


END MODULE mo_netcdf
