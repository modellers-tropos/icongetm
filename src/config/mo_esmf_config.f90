MODULE mo_esmf_config

    USE ESMF
    USE NUOPC

    USE mo_esmf_utilities,      ONLY: checkESMF
    USE mo_esmf_var,            ONLY: config_ESMF , nVar_2GETM , nVar_2ICON

    IMPLICIT NONE

    PUBLIC :: configure_COUPLER , destruct_COUPLER
    PUBLIC :: defineFieldDictionary

    PRIVATE :: configure_TIME , configure_ExchangeVariables
    PRIVATE :: destruct_TIME , destruct_ExchangeVariables

CONTAINS


SUBROUTINE configure_COUPLER()

    TYPE(ESMF_Config) :: configCPL
    
    INTEGER :: localrc

    INTEGER :: nStart , nStep , nEnd
    INTEGER :: nVar(2)

    CHARACTER(LEN=128), DIMENSION(:), ALLOCATABLE :: varlist

    INTEGER :: i

    LOGICAL :: tf(2)

    CHARACTER(LEN=32), PARAMETER :: routine = "mo_esmf_config:configure_COUPLER"


    ! create ESMF_Config for controlling of COUPLER
    configCPL = ESMF_ConfigCreate(rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_ConfigCreate")

    ! so far only: ICON to/from GETM is allowed 
    CALL ESMF_ConfigLoadFile(configCPL , "ICONGETM.rc" , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_ConfigLoadFile")

    ! verbosity level: "off", "low", "high", "max"
    CALL ESMF_ConfigGetAttribute(configCPL , config_ESMF%verbosity , label = "verbosity:" , default = "off" , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_ConfigGetAttribute verbosity")

    ! get number of processes for each component
    CALL ESMF_ConfigGetAttribute(configCPL , config_ESMF%nProc_ICON , label = "nProc_ICON:" , default = 1 , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_ConfigGetAttribute nProc_ICON")

    CALL ESMF_ConfigGetAttribute(configCPL , config_ESMF%nProc_GETM , label = "nProc_GETM:" , default = 1 , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_ConfigGetAttribute nProc_GETM")

    ! halo elements are not exchanged, i.e. not included in coupler if set to false
    CALL ESMF_ConfigGetAttribute(configCPL , config_ESMF%lowned , label = "lowned:" , default = .true. , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_ConfigGetAttribute lowend")
    
    ! setting for time controlling
    nStart = ESMF_ConfigGetLen(configCPL , label = "startTime:" , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_ConfigGetLen startTime")

    nStep = ESMF_ConfigGetLen(configCPL , label = "timestep:" , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_ConfigGetLen timestep")

    nEnd = ESMF_ConfigGetLen(configCPL , label = "endTime:" , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_ConfigGetLen endTime")

    CALL configure_TIME(nStart , nStep , nEnd)
    
    CALL ESMF_ConfigFindLabel(configCPL , label = "startTime:" , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_ConfigFindLabel startTime")

    DO i = 1 , nStart
        
        CALL ESMF_ConfigGetAttribute(configCPL , config_ESMF%startTime(i) , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_ConfigGetAttribute startTime")

    END DO

    CALL ESMF_ConfigFindLabel(configCPL , label = "timestep:" , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_ConfigFindLabel timestep")

    DO i = 1 , nStep
        
        CALL ESMF_ConfigGetAttribute(configCPL , config_ESMF%timeStep(i) , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_ConfigGetAttribute timestep")

    END DO

    CALL ESMF_ConfigFindLabel(configCPL , label = "endTime:" , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_ConfigFindLabel endTime")

    DO i = 1 , nEnd
        
        CALL ESMF_ConfigGetAttribute(configCPL , config_ESMF%endTime(i) , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_ConfigGetAttribute endTime")

    END DO


    ! setting number of elements of lists of variables for data exchange
    CALL ESMF_ConfigFindLabel(configCPL , label = "vars_GETM_Import:" , isPresent = tf(1) , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_ConfigFindLabel vars_GETM_Import")

    IF (tf(1)) THEN

        nVar_2GETM = ESMF_ConfigGetLen(configCPL , label = "vars_GETM_Import:" , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_ConfigGetLen vars_GETM_Import")

    END IF

    CALL ESMF_ConfigFindLabel(configCPL , label = "vars_ICON_Import:" , isPresent = tf(2) , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_ConfigFindLabel vars_ICON_Import")

    IF (tf(2)) THEN

        nVar_2ICON = ESMF_ConfigGetLen(configCPL , label = "vars_ICON_Import:" , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_ConfigGetLen vars_ICON_Import")

    END IF


    ! allocating of space for names of variables for data exchange
    CALL configure_ExchangeVariables()


    ! get variable names for ICON2GETM
    IF (tf(1)) THEN

        CALL ESMF_ConfigFindLabel(configCPL , label = "vars_GETM_Import:" , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_ConfigFindLabel vars_GETM_Import")

        DO i = 1 , nVar_2GETM

            CALL ESMF_ConfigGetAttribute(configCPL , config_ESMF%vars_GETM_Import(i) , rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_ConfigGetAttribute vars_GETM_Import")

        END DO
    
    END IF


    ! get variable names for GETM2ICON
    IF (tf(2)) THEN

        CALL ESMF_ConfigFindLabel(configCPL , label = "vars_ICON_Import:" , rc = localrc)
        CALL checkESMF(localrc , routine , "ESMF_ConfigFindLabel vars_ICON_Import")

        DO i = 1 , nVar_2ICON

            CALL ESMF_ConfigGetAttribute(configCPL , config_ESMF%vars_ICON_Import(i) , rc = localrc)
            CALL checkESMF(localrc , routine , "ESMF_ConfigGetAttribute vars_ICON_Import")

        END DO

    END IF

    ! destroy ESMF_Config, since not required anymore
    CALL ESMF_ConfigDestroy(configCPL , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_ConfigDestroy")

END SUBROUTINE configure_COUPLER


SUBROUTINE configure_ExchangeVariables()

    ALLOCATE(config_ESMF%vars_GETM_Import(nVar_2GETM))
    ALLOCATE(config_ESMF%vars_ICON_Import(nVar_2ICON))

END SUBROUTINE configure_ExchangeVariables


SUBROUTINE configure_TIME(nStart , nStep , nEnd)

    INTEGER, INTENT(IN) :: nStart , nStep , nEnd


    ALLOCATE(config_ESMF%startTime(nStart))
    ALLOCATE(config_ESMF%timeStep(nStep))
    ALLOCATE(config_ESMF%endTime(nEnd))

END SUBROUTINE configure_TIME


SUBROUTINE destruct_COUPLER()

    CALL destruct_ExchangeVariables()
    CALL destruct_TIME()

END SUBROUTINE destruct_COUPLER


SUBROUTINE destruct_ExchangeVariables()

    IF (ALLOCATED(config_ESMF%vars_GETM_Import)) THEN

        DEALLOCATE(config_ESMF%vars_GETM_Import)

    END IF

    IF (ALLOCATED(config_ESMF%vars_ICON_Import)) THEN

        DEALLOCATE(config_ESMF%vars_ICON_Import)

    END IF

END SUBROUTINE destruct_ExchangeVariables


SUBROUTINE destruct_TIME()

    IF (ALLOCATED(config_ESMF%startTime)) THEN

        DEALLOCATE(config_ESMF%startTime)

    END IF

    IF (ALLOCATED(config_ESMF%timeStep)) THEN

        DEALLOCATE(config_ESMF%timeStep)

    END IF

    IF (ALLOCATED(config_ESMF%endTime)) THEN

        DEALLOCATE(config_ESMF%endTime)

    END IF

END SUBROUTINE destruct_TIME


SUBROUTINE defineFieldDictionary()

    TYPE(NUOPC_FreeFormat)          :: freeFormat    

    INTEGER                         :: localrc

    CHARACTER(LEN=36), PARAMETER    :: routine = "mo_esmf_config:defineFieldDictionary"


    ! pressure

! ???????????
        ! add: pres_msl = mean sea level pressure = air_pressure_at_sea_level
        CALL addSynField("pres_msl" , "Pa" , longName = "mean sea level pressure" , cfName = "air_pressure_at_sea_level")
    
    ! wind

        ! add: u_10m = zonal wind in 10m = wind_x_velocity_at_10m
        CALL addSynField("u_10m" , "m s-1" , longName = "zonal wind in 10m" , cfName = "wind_x_velocity_at_10m")

        ! add: v_10m = meridional wind in 10m = wind_y_velocity_at_10m
        CALL addSynField("v_10m" , "m s-1" , longName = "meridional wind in 10m" , cfName = "wind_y_velocity_at_10m")
    
    ! temperature

        ! add: t_2m = temperature in 2m = air_temperature_at_2m
        CALL addSynField("t_2m" , "K" , longName = "temperature in 2m" , cfName = "air_temperature_at_2m")
    
        ! add: td_2m = dew-point in 2m = dew_point_temperature
        CALL addSynField("td_2m" , "K" , longName = "dew-point in 2m" , cfName = "dew_point_temperature")

        ! add: t_seasfc = sea surface temperature = sea_surface_temperature
        CALL addSynField("t_seasfc" , "K" , longName = "sea surface temperature" , cfName = "sea_surface_temperature")

        ! add: t_s = temperature of ground surface 
        CALL addSynField("t_s" , "K" , longName = "temperature of ground surface")

    ! momentum

        ! add: umfl_s = u-momentum flux at the surface = surface_downward_eastward_stress (ICON: downward)
        CALL addSynField("umfl_s" , "Pa" , longName = "u-momentum flux at the surface" , cfName = "surface_downward_eastward_stress")

        ! add: vmfl_s = v-momentum flux at the surface = surface_downward_northward_stress (ICON: downward)
        CALL addSynField("vmfl_s" , "Pa" , longName = "v-momentum flux at the surface" , cfName = "surface_downward_northward_stress")

    ! precipitation

        ! add: rain_gsp_rate = gridscale rain rate
        CALL addSynField("rain_gsp_rate" , "kg m-2 s-1" , longName = "gridscale rain rate")

        ! add: snow_gsp_rate = gridscale snow rate
        CALL addSynField("snow_gsp_rate" , "kg m-2 s-1" , longName = "gridscale snow rate")

        ! add: graupel_gsp_rate = gridscale graupel rate
        CALL addSynField("graupel_gsp_rate" , "kg m-2 s-1" , longName = "gridscale graupel rate")

        ! add: ice_gsp_rate = gridscale ice rate
        CALL addSynField("ice_gsp_rate" , "kg m-2 s-1" , longName = "gridscale ice rate")

        ! add: hail_gsp_rate = gridscale hail rate
        CALL addSynField("hail_gsp_rate" , "kg m-2 s-1" , longName = "gridscale hail rate")

        ! add: rain_con_rate = convective rain rate
        CALL addSynField("rain_con_rate" , "kg m-2 s-1" , longName = "convective rain rate")

        ! add: snow_con_rate = convective snow rate
        CALL addSynField("snow_con_rate" , "kg m-2 s-1" , longName = "convective snow rate")

        ! add: precipitation
        CALL addSynField("precipitation" , "m s-1")

    ! radiation

        ! add: sob_s = shortwave net flux at surface (ICON: net without diffusive part) = surface_downwelling_shortwave_flux
        CALL addSynField("sob_s" , "W m-2" , longName = "shortwave net flux at surface" , &
            & cfName = "surface_downwelling_shortwave_flux")

        ! add: thb_s = longwave net flux at surface
        CALL addSynField("thb_s" , "W m-2" , longName = "longwave net flux at surface")

        ! add: sodifd_s = shortwave diffuse downward flux at surface
        CALL addSynField("sodifd_s" , "W m-2" , longName = "shortwave diffuse downward flux at surface")

    ! heat

        ! add: shfl_s = surface sensible heat flux (ICON: downward)
        CALL addSynField("shfl_s" , "W m-2" , longName = "surface sensible heat flux")

        ! add: lhfl_s = surface latent heat flux (ICON: downward)
        CALL addSynField("lhfl_s" , "W m-2" , longName = "surface latent heat flux")

        ! add: surface_downward_heat_flux = surface_downward_heat_flux_in_air
        CALL addSynField("surface_downward_heat_flux" , "W m-2" , cfName = "surface_downward_heat_flux_in_air")

    ! humidity

        ! add: qv_s = specific humidity at the surface = specific_humidity 
        CALL addSynField("qv_s" , "1" , longName = "specific humidity at the surface" , &
            & cfName = "specific_humidity")

        ! add: rh_2m = relative humidity in 2m = relative_humidity
        CALL addSynField("rh_2m" , "1e-2" , longName = "relative humidity in 2m" , &
            & cfName = "relative_humidity")

    ! moisture / evaporation / cloud cover

        ! add: evaporation
        CALL addSynField("evaporation" , "m s-1")

        ! add: qhfl_s = surface moisture flux = surface_downward_water_flux (ICON: downward)
        CALL addSynField("qhfl_s" , "kg m-2 s-1" , longName = "surface moisture flux" , cfName = "surface_downward_water_flux")

        ! add: clct = total cloud cover = total_cloud_cover
        CALL addSynField("clct" , "1" , longName = "total cloud cover" , cfName = "total_cloud_cover")



    CALL NUOPC_FieldDictionaryEgest(freeFormat, rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_FieldDictionaryEgest")

    CALL NUOPC_FreeFormatLog(freeFormat, rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_FreeFormatLog")

END SUBROUTINE defineFieldDictionary


SUBROUTINE addSynField(shortName , units , kwe , cfName , longName)

    CHARACTER(LEN=*), INTENT(IN)            :: shortName , units
    CHARACTER(LEN=*), INTENT(IN), OPTIONAL  :: cfName , longName
    
    LOGICAL, INTENT(IN), OPTIONAL           :: kwe

    INTEGER                                 :: localrc

    CHARACTER(LEN=128)                      :: StandardNames(2)

    CHARACTER(LEN=36), PARAMETER            :: routine = "mo_esmf_config:addSynField"


    CALL NUOPC_FieldDictionaryAddEntry(TRIM(shortName) , TRIM(units) , rc = localrc)
    CALL checkESMF(localrc , routine , "NUOPC_FieldDictionaryAddEntry "//TRIM(shortName)//" "//TRIM(units))

    IF (PRESENT(cfName)) THEN

        IF ((TRIM(cfName) /= "air_pressure_at_sea_level") .AND. &
            & (TRIM(cfName) /= "magnitude_of_surface_downward_stress") .AND. &
            & (TRIM(cfName) /= "precipitation_flux") .AND. &
            & (TRIM(cfName) /= "sea_surface_height_above_sea_level") .AND. &
            & (TRIM(cfName) /= "sea_surface_salinity") .AND. &
            & (TRIM(cfName) /= "sea_surface_temperature") .AND. &
            & (TRIM(cfName) /= "surface_downward_eastward_stress") .AND. &
            & (TRIM(cfName) /= "surface_downward_heat_flux_in_air") .AND. &
            & (TRIM(cfName) /= "surface_downward_northward_stress") .AND. &
            & (TRIM(cfName) /= "surface_downward_water_flux") .AND. &
            & (TRIM(cfName) /= "surface_eastward_sea_water_velocity") .AND. &
            & (TRIM(cfName) /= "surface_net_downward_longwave_flux") .AND. &
            & (TRIM(cfName) /= "surface_net_downward_shortwave_flux") .AND. &
            & (TRIM(cfName) /= "surface_northward_sea_water_velocity")) THEN

            CALL NUOPC_FieldDictionaryAddEntry(TRIM(cfName) , TRIM(units) , rc = localrc)
            CALL checkESMF(localrc , routine , "NUOPC_FieldDictionaryAddEntry "//TRIM(cfName)//" "//TRIM(units))

        END IF

        StandardNames(1) = TRIM(shortName) 
        StandardNames(2) = TRIM(cfName) 

        CALL NUOPC_FieldDictionarySetSyno(StandardNames , rc = localrc)
        CALL checkESMF(localrc , routine , "NUOPC_FieldDictionarySetSyno "//TRIM(shortName)//" "//TRIM(cfName))

    END IF

    IF (PRESENT(longName)) THEN
    
        CALL NUOPC_FieldDictionaryAddEntry(TRIM(longName) , TRIM(units) , rc = localrc)
        CALL checkESMF(localrc , routine , "NUOPC_FieldDictionaryAddEntry "//TRIM(longName)//" "//TRIM(units))

        StandardNames(1) = TRIM(shortName) 
        StandardNames(2) = TRIM(longName) 

        CALL NUOPC_FieldDictionarySetSyno(StandardNames , rc = localrc)
        CALL checkESMF(localrc , routine , "NUOPC_FieldDictionarySetSyno "//TRIM(shortName)//" "//TRIM(longName))

    END IF

    IF (PRESENT(longName) .AND. PRESENT(cfName)) THEN

        StandardNames(1) = TRIM(longName) 
        StandardNames(2) = TRIM(cfName) 

        CALL NUOPC_FieldDictionarySetSyno(StandardNames , rc = localrc)
        CALL checkESMF(localrc , routine , "NUOPC_FieldDictionarySetSyno "//TRIM(longName)//" "//TRIM(cfName))
    
    END IF

END SUBROUTINE


END MODULE mo_esmf_config
