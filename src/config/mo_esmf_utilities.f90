MODULE mo_esmf_utilities

    USE ESMF

    USE mo_esmf_var,    ONLY: config_ESMF

    IMPLICIT NONE

    PUBLIC :: checkESMF
    PUBLIC :: FINDLOC

    PRIVATE :: FINDLOC_CHAR , FINDLOC_INT

    INTEGER, PARAMETER :: nerr = 0

    INTERFACE FINDLOC
        MODULE PROCEDURE FINDLOC_CHAR
        MODULE PROCEDURE FINDLOC_INT
    END INTERFACE FINDLOC       

CONTAINS


SUBROUTINE checkESMF(checkrc , routine , functionName)

    INTEGER, INTENT(IN) :: checkrc

    INTEGER :: localrc
    
    CHARACTER(LEN=*), INTENT(IN) :: routine , functionName

    
    IF (checkrc /= ESMF_SUCCESS) THEN

        IF (checkrc < 0) THEN

            WRITE(nerr , '(a,a,a,a,I3.3)') routine , ":" , TRIM(functionName) , " error: " , checkrc

        ELSE

            WRITE(nerr , '(a,a,a,a,I3.3)') routine , ":" , TRIM(functionName) , " returned with error code: " , checkrc
    
        END IF

        CALL ESMF_Finalize(endflag = ESMF_END_ABORT , rc = localrc)
 
        IF (localrc /= ESMF_SUCCESS) THEN

            WRITE(nerr , '(a,a,a,a,I3.3)') routine , ":" , TRIM("ESMF_Finalize") , " returned with error code: " , localrc
        
        END IF

    ELSE IF (TRIM(config_ESMF%verbosity) /= "off") THEN

        CALL ESMF_LogWrite(TRIM(routine)//TRIM(functionName) , ESMF_LOGMSG_TRACE , rc = localrc)

    END IF

END SUBROUTINE checkESMF


INTEGER FUNCTION FINDLOC_INT(list , element)

    INTEGER, INTENT(IN), DIMENSION(:)  :: list
    INTEGER, INTENT(IN)                :: element

    INTEGER                                     :: i


    FINDLOC_INT = 0

    DO i = 1 , SIZE(list)

        IF (list(i) == element) THEN

            FINDLOC_INT = i
            EXIT

        END IF

    END DO

END FUNCTION FINDLOC_INT


INTEGER FUNCTION FINDLOC_CHAR(list , element)

    CHARACTER(LEN=*), INTENT(IN), DIMENSION(:)  :: list
    CHARACTER(LEN=*), INTENT(IN)                :: element

    INTEGER                                     :: i


    FINDLOC_CHAR = 0

    DO i = 1 , SIZE(list)

        IF (TRIM(list(i)) .EQ. TRIM(element)) THEN

            FINDLOC_CHAR = i
            EXIT

        END IF

    END DO

END FUNCTION FINDLOC_CHAR


END MODULE mo_esmf_utilities
