MODULE mo_esmf_timemanagement

    USE ESMF

    USE mo_esmf_utilities,      ONLY: checkESMF
    USE mo_esmf_var,            ONLY: config_ESMF

    IMPLICIT NONE

    PUBLIC :: SetCalendarClock

CONTAINS


SUBROUTINE SetCalendarClock(Comp_Calendar , Comp_Clock)

    TYPE(ESMF_Calendar), INTENT(OUT)    :: Comp_Calendar
    TYPE(ESMF_Clock),    INTENT(OUT)    :: Comp_Clock

    TYPE(ESMF_Time)                     :: startTime , stopTime
    TYPE(ESMF_TimeInterval)             :: timeStep

    INTEGER                             :: localrc

    INTEGER                             :: y , mm , d , h , m , s

    CHARACTER(LEN=39), PARAMETER        :: routine = "mo_esmf_timemanagement:SetCalendarClock"


    ! create calender
    Comp_Calendar = ESMF_CalendarCreate(ESMF_CALKIND_GREGORIAN, name = "PROLEBTIC_GREGORIAN" , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_CalendarCreate")


    ! initialize start time
    CALL storeTimeInVar(config_ESMF%startTime , y , mm , d , h , m , s)

    CALL ESMF_TimeSet(startTime , yy = y , mm = mm , dd = d , h = h , m = m , s = s , calendar = Comp_Calendar, rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_TimeSet (start time)")


    ! initialize time interval
    CALL storeTimeInVar(config_ESMF%timeStep , y , mm , d , h , m , s)

    CALL ESMF_TimeIntervalSet(timeStep , yy = y , mm = mm , d = d , h = h , m = m , s = s , rc = localrc) 
    CALL checkESMF(localrc , routine , "ESMF_TimeIntervalSet")


    ! initialize stop time
    CALL storeTimeInVar(config_ESMF%endTime , y , mm , d , h , m , s)
    
    CALL ESMF_TimeSet(stopTime , yy = y , mm = mm , dd = d , h = h , m = m , s = s , calendar = Comp_Calendar, rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_TimeSet (stop time)")


    ! initialize the clock with the above values
    Comp_Clock = ESMF_ClockCreate(timeStep , startTime , stopTime = stopTime, name = "Clock_ICONGETM" , rc = localrc)
    CALL checkESMF(localrc , routine , "ESMF_ClockCreate")

END SUBROUTINE SetCalendarClock


SUBROUTINE storeTimeInVar(time , y , mm , d , h , m , s)

    INTEGER, INTENT(IN), ALLOCATABLE, DIMENSION(:)  :: time

    INTEGER, INTENT(OUT)                            ::  y , mm , d , h , m , s

    INTEGER                                         :: nTime

    CHARACTER(LEN=37), PARAMETER                    :: routine = "mo_esmf_timemanagement:storeTimeInVar"


     y = 0
    mm = 0
     d = 0
     h = 0
     m = 0
     s = 0

    nTime = SIZE(time)

    IF (nTime >= 1) THEN

        s = time(nTime)
        nTime = nTime - 1

        IF (nTime >= 1) THEN

            m = time(nTime)
            nTime = nTime - 1

        END IF

        IF (nTime >= 1) THEN

            h = time(nTime)
            nTime = nTime - 1

        END IF

        IF (nTime >= 1) THEN

            d = time(nTime)
            nTime = nTime - 1

        END IF

        IF (nTime >= 1) THEN

            mm = time(nTime)
            nTime = nTime - 1

        END IF

        IF (nTime >= 1) THEN

            y = time(nTime)
            nTime = nTime - 1

        END IF

    ELSE

        CALL checkESMF(-1 , routine , "time not correct given")

    END IF       

END SUBROUTINE storeTimeInVar


END MODULE mo_esmf_timemanagement
